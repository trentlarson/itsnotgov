#!/usr/bin/env python

# The MIT License (MIT)
#
# Copyright (c) 2018 Sunaina Pai
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


"""Make static website/blog with Python."""


import codecs
import os
import itertools
import shutil
import re
import glob
import sys
import json
import datetime
import string
from markdown import markdown
from markdown.extensions.toc import TocExtension
import jinja2

BUILD_PATH = '_site_build'
SERVE_PATH = '_site'

def fread(filename):
    """Read file and close the file."""
    with codecs.open(filename, 'r', encoding='utf-8') as f:
        return f.read()


def fwrite_html(url_path, text):
    """Write content to file and close the file."""

    # if it's an abs path, path.join will ignore the first parameter for whatever reason.
    assert not os.path.isabs(url_path), url_path + ' is not a relative path'

    file_path = os.path.join(BUILD_PATH, url_path)
    basedir = os.path.dirname(file_path)
    if not os.path.isdir(basedir):
        os.makedirs(basedir)

    with codecs.open(file_path, 'w', encoding='utf-8') as f:
        f.write(text)


def log(msg, *args):
    """Log message with specified arguments."""
    sys.stderr.write(msg.format(*args) + '\n')


def truncate(text, words=25):
    """Remove tags and truncate text to the specified number of words."""
    return ' '.join(re.sub('(?s)<.*?>', ' ', text).split()[:words])


def read_headers(text):
    """Parse headers in text and yield (key, value, end-index) tuples."""
    assert isinstance(text, unicode)
    for match in re.finditer('\s*<!--\s*(.+?)\s*:\s*(.+?)\s*-->\s*|.+', text):
        if not match.group(1):
            break
        yield match.group(1), match.group(2), match.end()


def rfc_2822_format(date_str):
    """Convert yyyy-mm-dd date string to RFC 2822 format date string."""
    d = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    return d.strftime('%a, %d %b %Y %H:%M:%S +0000')


def read_content(filename):
    """Read content and metadata from file into a dictionary."""
    # Read file content.
    text = fread(filename)

    # Read metadata and save it in a dictionary.
    date_slug = os.path.basename(filename).split('.')[0]
    match = re.search('^(?:(\d\d\d\d-\d\d-\d\d)-)?(.+)$', date_slug)
    content = {
        'date': match.group(1) or '1970-01-01',
        'slug': match.group(2),
        # TODO - a better place for global variables like this
        'source_link': 'https://gitlab.com/orblivion/itsnotgov',
    }

    # Read headers.
    end = 0
    for key, val, end in read_headers(text):
        content[key] = val

    # Separate content from headers.
    text = text[end:]

    # Convert Markdown content to HTML.
    if filename.endswith(('.md', '.mkd', '.mkdn', '.mdown', '.markdown')):
        text = markdown(text, extensions=[TocExtension(title='Contents', baselevel=2)])

    # Update the dictionary with content and RFC 2822 date.
    content.update({
        'content': text,
        'rfc_2822_date': rfc_2822_format(content['date']),
    })

    return content


def render_common(template_text, **params):
    """Replace placeholders in template with values from params."""
    return jinja2.Environment(
      loader=jinja2.FileSystemLoader('layout')
    ).from_string(template_text).render(params)

def render_path(path_template, **params):
    path = render_common(path_template, **params)
    assert set(path) < set(string.letters + string.digits + '_-/.'), path
    return path

def render(template, **params):
    assert isinstance(template, unicode), type(template)
    return render_common(template, **params)

def peek_values(src_path, dst, **params):
    # TODO - would be nice to integrate this with make_pages below
    content = read_content(src_path)
    page_params = dict(params, **content)
    dst_path = render_path(dst, **page_params)
    page_params['link_path'] = os.path.dirname(dst_path)
    del page_params['content']
    return page_params

def make_pages(src, dst, layout, params, required_params=None):
    """Generate pages from page content."""
    items = []
    required_params = required_params or []

    for src_path in glob.glob(src):
        log('Reading {} ...', src_path)
        content = read_content(src_path)

        page_params = dict(params, **content)
        dst_path = render_path(dst, **page_params)

        # TODO Should this go in `page_params` instead of `content`? This whole thing is confusing.
        content['link_path'] = os.path.dirname(dst_path)

        # Populate placeholders in content if content-rendering is enabled.
        if page_params.get('render') == 'yes':
            rendered_content = render(page_params['content'], **page_params)
            page_params['content'] = rendered_content
            content['content'] = rendered_content

        missing_params = set(required_params) - set(content)
        if missing_params:
            raise ValueError("missing params for", src, missing_params)

        items.append(content)

        output = render(layout, **page_params)

        log('Rendering {} => {} ...', src_path, dst_path)

        fwrite_html(dst_path, output)

    return sorted(items, key=lambda x: x['date'], reverse=True)

def make_list(posts, src, dst, list_layout, item_layout, params, required_params=None):
    """Generate list page."""
    items = []
    required_params = required_params or []

    for post in posts:
        # TODO - style stubs differently and only make them work on bottom level stories
        #if post.get('stub'):
        #    continue
        item_params = dict(params, **post)
        if post['content']:
            item_params['summary'] = item_params.get('summary', truncate(post['content']) + "&nbsp...")
        else:
            item_params['summary'] = ''
        item = render(item_layout, **item_params)
        items.append(item)

    log('Rendering list:')
    [list_obj] = make_pages(
        src, dst,
        list_layout,
        dict(params, item_list=''.join(items)),
        required_params
    )
    return dict(list_obj,
        list_items=posts, # TODO oy. naming. just do a normal jinja list later.
    )

def make_redirects(items):
    for item in items:
        if item.has_key('redirect'):
            # make it a relative path because bah
            assert item['redirect'][0] == '/', item['redirect']
            redirect_from = item['redirect'][1:]

            make_pages(
                'content/stories/_index.md', # doesn't matter! we don't use this anyway. just needed some valid file path.
                os.path.join(redirect_from, 'index.html'),
                fread('layout/redirect.html'),
                {'redirect_to': item['link_path']}
            )

def make_stories(**params):
    subcategory_item_layout = story_item_layout = fread('layout/item.html')

    home_page_layout = fread('layout/home.html')
    category_list_layout = fread('layout/category.html')
    subcategory_list_layout = fread('layout/subcategory.html')
    story_page_layout = fread('layout/story.html')
    categories = []
    for category_path in glob.glob('content/stories/[!_]*'):
        category_index_input_path = os.path.join(category_path, '_index.md')
        category_index_output_path = '{{ category_slug }}/index.html'
        category_params = dict(params, category_slug=os.path.basename(category_path))
        category_values = peek_values(category_index_input_path, category_index_output_path, **category_params)
        subcategories = []
        subcategories_glob = glob.glob(os.path.join(category_path, '[!_]*'))
        if not subcategories_glob:
            continue
        for subcategory_path in subcategories_glob:
            subcategory_index_input_path = os.path.join(subcategory_path, '_index.md')
            subcategory_index_output_path = '{{ category.category_slug }}/{{ subcategory_slug }}/index.html'
            subcategory_params = dict(params, category=category_values, subcategory_slug=os.path.basename(subcategory_path))
            subcategory_values = peek_values(subcategory_index_input_path, subcategory_index_output_path, **subcategory_params)

            stories = make_pages(
                os.path.join(subcategory_path, '[!_]*'),
                '{{ subcategory.category.category_slug }}/{{ subcategory.subcategory_slug }}/{{ slug }}/index.html',
                story_page_layout,
                dict(params, subcategory=subcategory_values),
                ['title'],
            )
            if not stories:
                continue
            subcategories.append(make_list(
                stories,
                subcategory_index_input_path,
                subcategory_index_output_path,
                subcategory_list_layout,
                story_item_layout,
                subcategory_params,
            ))
        categories.append(make_list(
            subcategories,
            os.path.join(category_path, '_index.md'),
            category_index_output_path,
            category_list_layout,
            subcategory_item_layout,
            category_params,
            ['icon'],
        ))
    home = make_pages(
        'content/_index.md',
        'index.html',
        home_page_layout,
        dict(params, categories=categories),
    )[0]
    home['categories'] = categories
    return home

def main():
    # Create a new BUILD_PATH directory from scratch.
    if os.path.isdir(BUILD_PATH):
        shutil.rmtree(BUILD_PATH)
    shutil.copytree('static', BUILD_PATH)

    # Default parameters.
    params = {
        'base_path': '',
        'author': 'Admin',
        'site_url': 'http://localhost:8000',
        'current_year': datetime.datetime.now().year
    }

    # If params.json exists, load it.
    if os.path.isfile('params.json'):
        params.update(json.loads(fread('params.json')))

    # Load layouts.
    direct_layout = fread('layout/direct.html')
    page_layout = fread('layout/page.html')
    #feed_xml = fread('layout/feed.xml')
    #item_xml = fread('layout/item.xml')

    # Create site html pages. (won't contain redirects)
    make_pages('content/[!_]*.html', '{{ slug }}/index.html',
               page_layout, params)

    # Create site md pages (may contain redirects)
    root = make_pages('content/[!_]*.md', '{{ slug }}/index.html',
               page_layout, params)

    # Create rss link (faked for now, copied from previous sate of former dynamic version of the site.
    # Just so it won't break people following this URL.). Named file index.html so that the path /rss/ works
    make_pages('content/rss-holdover.xml', 'rss/index.html',
               direct_layout, params)

    # Make the 404 page
    make_pages('content/_404.md', '404.html',
               page_layout, params)

    # Create special pages
    special = make_pages('content/help/[!_]*.md',
                        'help/{{ slug }}/index.html',
                        page_layout, params)

    home_page = make_stories(**params)
    categories = home_page['categories']
    subcategories = list(itertools.chain(*[c['list_items'] for c in categories]))
    stories = list(itertools.chain(*[s['list_items'] for s in subcategories]))

    make_redirects([home_page])
    make_redirects(categories)
    make_redirects(subcategories)
    make_redirects(stories)
    make_redirects(special)
    make_redirects(root)

    # Assuming all is well, do an "atomic" build of the site.
    # (If there was an error before it got this far, the old version of the site stays intact)
    if os.path.isdir(SERVE_PATH):
        shutil.rmtree(SERVE_PATH)
    shutil.move(BUILD_PATH, SERVE_PATH)

    # Create RSS feeds.
    #make_list(blog_posts, 'blog/rss.xml',
    #          feed_xml, item_xml, blog='blog', title='Blog', params)

# Test parameter to be set temporarily by unit tests.
_test = None


if __name__ == '__main__':
    main()
