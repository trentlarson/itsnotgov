Source code for a statically generated website called [I Can't Believe It's Not Government](https://itsnotgov.org).

![logo](https://itsnotgov.org/images/logo.png)

# Credit

Thanks to [Makesite](https://github.com/sunainapai/makesite/) for a simple static content generator that looked decent out of the box, and which I could easily contort for my own needs.

# HowTo

To contribute text, just drop a markdown file in the appropriate directory. You may as well edit and preview it right on gitlab.

To contribute anything else, you can build it locally. Look here: [Makesite](https://github.com/sunainapai/makesite/), except you want to install all the stuff from `requirements.txt` instead of just `CommonMark`. (I know these instructions suck, improvements will come later).

# Licenses:

* Everything in the `content/` Directory: [Creative Commons Zero](LICENSE_CONTENT.md)
* Font (Roboto): [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html), from Google. See [here](https://fonts.google.com/attribution).
* Everything Else: [MIT](LICENSE_CODE.md)
