<!-- title: Story Idea Dump -->

<!-- redirect: /w/s/story_idea_dump -->

This is a dumping ground stories that we have come across which may inspire a Story entry on this site. Not a whole lot of thought has gone into what is added on this page, so some investigation is in order.

If you want to help out the site, this would be a great place to start. Find a story that you'd like to take the time to investigate, take a quick look at our [submission criteria](/help/story-submission-criteria) to see what qualifies as a good story, our [writing guidelines](/help/story-writing-guidelines) to see how we'd like it done, and create an article!

For technical expediency (based on how I've set up this site) this page is editable only by admins, but hopefully that will change soon. For now, if you have an article you've heard about, and don't have the time to write an article, email it to info@itsnotgov.org or tweet it at [@itsnotgov](https://twitter.com/itsnotgov)

# Ideas

##  General places to fish ideas:

* https://en.wikipedia.org/wiki/List_of_privatizations_by_country
* http://www.notbeinggoverned.com/anarchy-never-been-tried-part-v-anarchy-in-the-usa/

## Societies with anarchist elements

Try to glean specific government sources from this if we can.

[Christiania](https://en.wikipedia.org/wiki/Christiania)

Anarchist Catalonia (caveat leftist property arrangements, but worth mentioning if they actually enforced it in a decentralized way, and accomplished something interesting in the process)

Ireland http://www.badquaker.com/archives/554 (go to original sources! Bad Quaker is one of us.)

## Privately owned public spaces (evaluate pros and cons):

### National Land Trust

### NYC
* https://news.ycombinator.com/item?id=14847894
* http://www.nyc.gov/pops
* https://nycopendata.socrata.com/Housing-Development/Privately-Owned-Public-Spaces/fum3-ejky
* http://www.nytimes.com/2011/10/20/opinion/zuccotti-park-and-the-private-plaza-problem.html

### (Lack of) zoning Houston, TX

Apparently in Houston, zoning laws are very lax or nonexistent, and it works out. There may be other things in its place, though. Will require some careful research to see if it actually is of interest to us.

## Fire Protection

National Fire Protection Association - Writing fire codes which are adopted by local municipalities. Need to figure out how it's funded, who the constituents of this organisation are (ie, if it's an association of government fire departments, it may not be very interesting for this site)
* https://en.wikipedia.org/wiki/National_Fire_Protection_Association

2010 Russian Wildfires
* https://en.wikipedia.org/wiki/2010_Russian_wildfires#Volunteer_efforts
* Apparently despite gov't? I dunno. Listen to Reply All: Friends and Blasphemers. 15 min in or something.

## Law

### David Friedman stuff:

http://www.daviddfriedman.com/Academic/Course_Pages/legal_systems_very_different_08/legal_systems_v_diff.htm

Remember: Don't use David Friedman as a source. He is biased toward our side! We should go to his sources and draw our own conclusions.

### List of private arbitrators:

* http://www.internationalarbitrationlaw.com/arbitral-institutions/icc/
    * International Court of Arbitration:
        * http://www.iccwbo.org/about-icc/organization/dispute-resolution-services/icc-international-court-of-arbitration/
    * https://en.wikipedia.org/wiki/International_Court_of_Arbitration
    * London Court of International Arbitration (caveat: chartered by a city precinct government)
    * https://en.wikipedia.org/wiki/London_Court_of_International_Arbitration

Modria (a product, not sure if it has a track record yet) https://www.tylertech.com/solutions-products/modria
    * Modria was bought out by Tyler Tech, who shut down the e-commerce part of it, and now it handles court cases.

## Police

A Reddit discussion about alternatives to the Police: http://www.reddit.com/r/Bad_Cop_No_Donut/comments/2p1bkd/successful_alternatives_to_the_police/

Englewood, Chicago mothers patrol to stop gun violence: http://www.dnainfo.com/chicago/20150728/englewood/no-shootings-since-army-of-moms-set-up-on-s-side-blocks-but-help-needed#

## Regulation

### Food Safety

I have a start here: https://itsnotgov.org/w/c/Regulation/FoodSafety/food_certification_organizations/

#### The international patchwork

A big network of food safety organizations that I don't quite understand, but who relate to the food safety standards of grocery stores such as Costco. I think the way it works is that each store has their own standards, which makes it a bit of a challenge for vendors. Eventually larger standards boards developed, creating their own standards, which Costco and the like accept alternately to their own standards. There was a whole rabbit hole I followed before I decided it was going to require more time than I had at the time.

* Global Food Safety Initiative
    * http://www.mygfsi.com/
    * https://en.wikipedia.org/wiki/Global_Food_Safety_Initiative
* Cert ID http://www.cert-id.com/
* International Food Safety and Quality Network
    * http://www.ifsqn.com/about_ifsqn.html
* Guelph Food Technology Centre http://www.gftc.ca/default.aspx

#### Organic certification

Mofga.org Apparently legally not organic, but I think it may actually be better. USDA hijacked the Organic label. Other certifications used to exist as well.

### Other

Underwriters Laboratories: http://ul.com/

## Uncategorized

Replacing the prison system: http://www.delanceystreetfoundation.org/circle.php

Replacing involuntary environmental controls by making the process voluntary (mostly at least) with Conservation easements: http://www.nature.org/ 

Rolling Stone's 6 ideas for a cop-free world: http://www.rollingstone.com/politics/news/policing-is-a-dirty-job-but-nobodys-gotta-do-it-6-ideas-for-a-cop-free-world-20141216

6 things you learn in a town prepared for apocalypse: http://www.cracked.com/personal-experiences-1540-6-things-you-learn-in-town-prepared-apocalypse.html

Detroit Bus Company - http://www.popularmechanics.com/cars/alternative-fuel/biofuels/a-private-bus-company-debuts-in-detroit-8657656

American Mail and Letter Company - https://en.wikipedia.org/wiki/American_Letter_Mail_Company

National Auto Trail - https://en.m.wikipedia.org/wiki/National_Auto_Trail Requires investigation into exactly who orchestrated it, funded it, and how land was acquired.

TED talk about private schools for the poor https://www.youtube.com/watch?v=gzv4nBoXoZc&feature=youtu.be

Surgery Center of Oklahoma - http://www.surgerycenterok.com/ Relatively inexpensive surgery, prices listed on its website (imagine that). I think they work around the system in some way. Perhaps they just don't take insurance?

Bitcoin (of course) - but need to find some sort of "success". Did it help a country in economic turmoil? Maybe Dash in Venezuela.

Free Banking in Britain: http://www.amazon.com/Free-Banking-Britain-Theory-Experienc/dp/0255363753

Insurance companies safety testing cars: http://en.wikipedia.org/wiki/Insurance_Institute_for_Highway_Safety (the currently existing article is about them contributing to the regulation of traffic)

Waste Management: https://twitter.com/BobMurphyEcon/status/878292483947540480

Alternate power grid? http://www.politico.com/magazine/story/2017/06/15/how-a-street-in-brooklyn-is-changing-the-energy-grid-215268 Could be BS. Need to figure out whose lines they're using. If they're using the existing lines, who cares.

Free hospital in Athens, Greece built by anarchists

* https://twitter.com/IGD_News/status/882390909001678848
* https://www.unicornriot.ninja/2017/greece-adye-exarcheias-free-self-organized-healthcare-clinic/
