<!-- title: Story Submission Criteria - Working notes / In Progress -->

<!-- redirect: /w/s/StorySubmissionCriteriaInProgress -->

These are the working notes for the [Story Submission Critera](/help/story-submission-criteria) article, in order to keep them from cluttering the Story Submission Critera article itself.

# General Notes

The government helps them not at all, or "less than" people expect.

Government shouldn't be providing significant incentive for what they're doing, financially or otherwise. Identify another incentive for this being created.

It doesn't have to be something esoteric, it can be something people already know about. It could just be there to remind people. You could also provide a different twist on something (though see the article writing guidelines, putting spin walks a fine line).

# Thoughts on our bias

We walk a fine line, given that we want to be factual, but we are also acting with an agenda.

Do we accept "counterexamples"? IE, cases where the market failed? I would argue no, with the following justification: We are not here to demonstrate what usually happens. In a world with government involvement, it's clear that government is what will happen. We're here to show what *can* happen despite people's expectations. To show some business models that are proven viable. The question of whether they'd be better in a world without government involvement in that sector, compared to how government performs now, is another long discussion.

That said, in each submission, we have to be completely forthcoming about the circumstances, if we are to really demonstrate the viability. Did the government help? Did they get lucky somehow? See the [Story Writing Guidelines](/help/story-writing-guidelines) for more on that.

However, showing more is better. If we show more to further our point, maybe we should show counterexamples? But any bozo can fail at a business. Hmm.

How about this: As people with an agenda to teach, we believe that the facts are likely on our side. As such, we're not afraid of the facts, but we reach a different conclusion from others with access to the same facts. What we should do is highlight, in a dense form, those facts which we believe most people are not sufficiently considering.