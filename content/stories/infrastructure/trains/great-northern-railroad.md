<!-- title: Great Northern Railroad (U.S.) -->

<!-- redirect: /w/c/Infrastructure/Trains/GreatNorthernRailroad -->

*This article is a stub*

The Great Northern Railroad was a railroad line built in the United States in the mid-1800s. It was founded by James Jerome Hill. When it was completed, it stretched from St. Paul, Minnesota to Seattle, Washington.

It received [no government funding][gnrhs_history]. It also received no direct government land grants, though the company did purchase one 600 piece of railroad track from a company who was granted their land from the government.

[gnrhs_history]: http://www.gnrhs.org/gn_history.htm
[wikipedia]: https://en.wikipedia.org/wiki/Great_Northern_Railway_(U.S.)

# Sources

## References

* [History][gnrhs_history] from the Great Northern Railroad Historical Society

## Other Resources

* [Wikipedia][wikipedia]
* [Blog post](http://socialdemocracy21stcentury.blogspot.com/2011/07/government-intervention-james-j-hill.html) giving examples of the railroad's creator accepting certain government benefits.