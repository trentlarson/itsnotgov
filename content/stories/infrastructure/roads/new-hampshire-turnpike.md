<!-- title: First New Hampshire Turnpike -->

<!-- redirect: /w/c/Infrastructure/Roads/NewHampshireTurnpike -->

The [New Hampshire Turnpike System][nhdot_turnpikes] is, like most major roads in the United States today, operated by a government organization. However the first New Hampshire Turnpike was created by a [private corporation][nhpr_story]. A [commemorative marker][commemorative_marker] gives a brief account of its history, including its origins with a private corporation.

# Results

## Length

It ran 36 miles, starting near the town of Durham and reaching Concord, today the capital of New Hampshire. The turnpike connected to a bridge across the Little Bay which ultimately connected travelers to the coastal city of Portsmouth.

## Funding

A company was formed around the road project, and according to the Town of Nottingham website, shares were [sold to the public][nottingham_website] in order to finance it. From there it became a [toll road][nottingham_website] ("[turnpike](http://en.wikipedia.org/wiki/Turnpike)" being a reference to the mechanism to block travelers who had not yet paid).

However [according to the Concord Insider][concord_insider_article], it was originated by a government land survey, and initially planned to be a public (non-toll) road, financed by the towns through which it passed. This article gives many details, and it is as of yet unclear how all these facts are reconciled.

# Sources:

# References:

* [New Hampshire Public Radio story][nhpr_story]
* Modern [New Hampshire Turnpike system][nhdot_turnpikes]
* [Concord Insider article][concord_insider_article] which describes it as a government project.
* Picture of [Commemorative marker][commemorative_marker]
* Nottingham NH [town government website][nottingham_website]

# Other Resources

* [Amherst Citizen article][amherst_citizen_article] about the second New Hampshire turnpike, which some reference to the first.

[nhpr_story]: http://nhpr.org/post/marking-history-first-new-hampshire-turnpike-northwood
[nhdot_turnpikes]: http://www.nh.gov/dot/org/operations/turnpikes/
[nottingham_website]: http://www.nottingham-nh.gov/Pages/NottinghamNH_Historic/townhistory
[concord_insider_article]: http://www.theconcordinsider.com/article/new-hampshires-turnpike
[amherst_citizen_article]: http://www.amherstcitizen.com/files/Archives/VOLUME_19/V19-N09/PDF_files/AC08.pdf
[commemorative_marker]: http://www.waymarking.com/gallery/image.aspx?f=1&guid=4c23d725-d651-4350-a857-eb4b03e95e71