<!-- title: Fruit smugglers improve road near Russia-Belarus border -->

<!-- redirect: /w/c/Infrastructure/Roads/fruit_smugglers_improve_road_near_russia_belaru -->

*This article is a stub. You can help out by filling out useful details. See our writing guidelines.*

Fruit smugglers have [improved a road][bbc_article] between the Russia-Belarus border.

Presumably they received no government assistance, being part of the black market. Thus, this is evidence that businesses have a profit motive to invest in infrastructure.

# Sources

## References
* [BBC Article][bbc_article]

## Other Resources
* [Moscow Times Article](https://themoscowtimes.com/news/traffickers-repair-russian-road-used-for-smuggling-55065)

[bbc_article]: http://www.bbc.com/news/blogs-news-from-elsewhere-37166353