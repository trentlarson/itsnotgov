<!-- title: Santa Cruz Vigilante Pothole Team -->
<!-- summary: A group of citizens in Santa Cruz, California get tired of waiting for the government to fix potholes after heavy rainfall. -->

![Pothole](/images/stories/infrastructure/roads/santa-cruz-vigilante-pothole-team/pothole.jpg)

The [Santa Cruz Vigilante Pothole Team][santacruzsentinel] is a group of citizens in who make small road repairs in Santa Cruz, California in defiance of the Santa Cruz County government. It was [created in response][fox] to Santa Cruz County government claiming that it would take over a year to repair holes created by some heavy rains. The group has roughly [three dozen people][santacruzsentinel] and is lead by a retired software company CEO.  They are [self-taught][santacruzsentinel], they wear safety vests, they use radios to communicate, and they direct traffic. The government's main objection [appears to be][fox] an issue of legal liability.

<iframe width="560" height="315" src="https://www.youtube.com/embed/7kZPY4lgYmM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Funding

They [raised their initial $3,800][santacruzsentinel] for materials in 24 hours by asking the community, after which the group stopped asking for donations (at least for the time being). They use [their own][fox] pickup trucks to perform work.

# Government Involvement

As of 2017, they have been [seeking an arrangement][fox] with the government to make them legally sanctioned, similarly to volunteer fire fighters, though they continued their work regardless.

The group [does not see][santacruzsentinel] themselves as a replacement for government road services. Rather, they [see their efforts][fox] as a way to allow the government to focus their efforts on larger road repairs caused by the rains.

# Sources

## References

[fox]: https://www.foxnews.com/auto/vigilante-pothole-team-brings-relief-to-california-drivers-but-draws-ire-of-county-officials (Fox News - Vigilante pothole team brings relief to California drivers but draws ire of county officials)
* Fox News - [Vigilante pothole team brings relief to California drivers but draws ire of county officials][fox]
[santacruzsentinel]: https://www.santacruzsentinel.com/2017/03/17/santa-cruz-mountain-vigilantes-tackle-plague-of-potholes/ (Santa Cruz Sentinel - Santa Cruz Mountain ‘vigilantes’ tackle plague of potholes)
* Santa Cruz Sentinel - [Santa Cruz Mountain ‘vigilantes’ tackle plague of potholes][santacruzsentinel]
