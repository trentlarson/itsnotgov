<!-- title: Dashrath Manjhi - Carving a path through the hills -->

<!-- redirect: /w/c/Infrastructure/Roads/dashrath_manjhi_carving_a_traveling_path_through_t -->

Dashrath Manjhi is a man in India who, from 1960 to 1982, is purported to have almost [single-handedly carved][india_today] a 360 foot long, 25 foot deep path through a hill near his village using only hand tools. Toward the end of the project, some of his fellow villagers [pitched in][oneindia].

After his path was completed, his village had [much faster access][india_today] to the nearest hospital. He acted in honor of his wife, who died due to [lack of medical care][india_today] in 1959. Dashrath Manjhi [died in 2007][india_today] at the age of 73.

# Government Involvement

During the project, the leader of the village apparently did not support his efforts, and even [filed a complaint][oneindia] against him with the prime minister of India, Indira Gandhi. Later, when other villagers joined him, local officials [tried to stop them][oneindia].

However at a certain point, after media attention, the government apparently [finished the job][oneindia], including building a road. It's not clear exactly how much work was left by that point.

# Sources

## References

* India Today [article][india_today]
* Oneindia.com [article][oneindia]

## Other Sources

* Wikipedia [article][wikipedia]
* Hoax or Fact [article][hoax_or_fact]
    * Note: Not sure if this site is as reputable as Snopes, but it was referenced by Wikipedia as of this writing

[wikipedia]: https://en.wikipedia.org/wiki/Dashrath_Manjhi
[india_today]: https://www.indiatoday.in/education-today/gk-current-affairs/story/dashrath-manjhi-282520-2015-07-15
[oneindia]: https://www.oneindia.com/feature/dashrath-manjhi-the-mountain-man-the-inspiring-untold-story-of-an-unsung-hero-1841887.html
[hoax_or_fact]: https://www.hoaxorfact.com/Inspirational/man-in-india-carved-360-feet-mountain-tunnel-in-memory-of-his-wife-s-death-facts-analysis.html
