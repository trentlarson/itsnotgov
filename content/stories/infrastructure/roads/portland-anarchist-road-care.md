<!-- title: Portland Anarchist Road Care -->

<!-- redirect: /w/c/Infrastructure/Roads/portland_anarchist_road_care -->

<!-- left-friendly: yes -->

Portland Anarchist Road Care is a group of anarchists who engage in [direct action][direct_action_wikipedia] by fixing potholes in streets in Portland, Oregon.

It is a very small scale group, but serves as a potential model for maintenance of public resources. As of April, 2018, the group has not posted on their Facebook page since December, 2017.

# Incentives

The group is opposed by the city government, which presumably means they get no government funding or other support. Dylan Rivera, a spokesman for the Portland Bureau of Transportation, [said][citylab_article]:

> It’s not safe or legal for people to fill potholes on streets that are maintained by the city, They run the risk of being injured in a traffic crash.

The group's motivation is [ideological][citylab_article]:

> We aren’t asking permission, because these are our streets. They belong to the people of Portland, and the people of Portland will fix them.

# Results

Most of the results here are found from the group's own Facebook page and news articles interviewing neighbors, government spokespeople, and the group themselves. More objective sources on results would be preferable.

## Number of holes patched

So far this is a very small scale operation. As of March 2017, the group only [claimed to have][huffpo_article] fixed five potholes, while the city government [had fixed 900][weather_network_article].

## Popular Support

The group received at least some popular support. Resident [Tom Satchell said][kgw_article]:

> They don’t need to be masked... they’re doing a great job.

Resident [Eric Hollstein said][kgw_article]:

> I appreciate the effort

And

> Great job trying to be a helpful citizen, but not necessarily successful at all times maybe

## Safety

Spokesman [Dylan Rivera warns][citylab_article]:

> They also run the risk of being held personally liable if someone were to be injured by the pothole they attempted to fix.

However the group [checks back][carbuzz_article] on to make sure their old repairs hold up.

Another concern is that the group uses a so-called "cold patch" method, which [they acknowledge][weather_network_article] does not last as long as other methods:

> We are working within a harm reduction model right now. We don't have the money or equipment [to] do more permanent repairs, but we are doing what we can in the mean time. We would love your advice though. Are there any in between solutions of cold patch and hot asphalt? Are there any ways to make cold patches stronger?

# Sources

## Reference:
* [Citylab Article][citylab_article]
* [KGW8 News Article][kgw_article]
* [Carbuzz Article][carbuzz_article]
* [Weather Network Article][weather_network_article]
* [Huffington Post Article][huffpo_article]

## Other Sources:
* [Wikipedia Article][wikipedia_article]
* [Oregon Live Article][oregon_live_article]

## Further Reading:
* [Facebook Page][facebook_page]
    * [A post][fb_post_feb_28_2017] from Feb 28, 2017, with photographs of a repair job.

## See Also:

* [Open Source Streets][indianapolis_story], a similar (unmasked) group in Indianapolis

[direct_action_wikipedia]: https://en.wikipedia.org/wiki/Direct_action
[citylab_article]: https://www.citylab.com/equity/2017/03/portland-anarchists-want-to-fix-your-streets-potholes/519588/
[facebook_page]: https://www.facebook.com/PortlandAnarchistRoadCare/
[wikipedia_article]: https://en.wikipedia.org/wiki/Portland_Anarchist_Road_Care
[oregon_live_article]: http://www.oregonlive.com/commuting/index.ssf/2017/03/why_portland_anarchists_are_pa.html
[kgw_article]: http://www.kgw.com/article/news/local/anarchist-group-repairing-portland-potholes/422421601
[indianapolis_story]: http://www.kiiitv.com/article/news/nation-world/they-were-tired-of-waiting-for-the-city-to-fill-potholes-so-theyre-doing-it-themselves/507-536435099
[fb_post_feb_28_2017]: https://www.facebook.com/PortlandAnarchistRoadCare/posts/641845379333160
[carbuzz_article]: https://carbuzz.com/news/anarchists-are-fixing-potholes-in-portland
[weather_network_article]: https://www.theweathernetwork.com/us/news/articles/pothole-vigilantes-seek-to-smooth-out-portlands-roads/80352/
[huffpo_article]: https://www.huffingtonpost.com/entry/anarchists-fixing-potholes-portland_us_58caaa7be4b0ec9d29d9575b
