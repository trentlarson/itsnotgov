<!-- title: Mike Will Build The Roads! A small private toll road is built in England. -->

<!-- redirect: /w/c/Infrastructure/Roads/MikeWillBuildTheRoads -->

In August of 2014, a [365 meter][telegraph_story] toll road was built between Bath and Bristol England, by the town of Kelston, to circumvent a segment of the heavily traveled A431 Kelston Road, which was temporary closed due to a landslide. It was financed and operated by then [62 year old][telegraph_story] local businessman Mike Watts, with help from his wife, to create an alternative to the 16km (10 mile) official detour.

Watts privately funded the project, even putting [his house][telegraph_story] up for collateral, and built the road through a local farmer's field. Many drivers chose to use this small road to avoid the very lengthy official detour.

# Incentives

From the very beginning, Mike was somewhat at odds with the Bath and North East Somerset Council, because he began operating the road [before asking for official permission][bbc_toll_road_created]. Over the course of the road's operation, the council [questioned the road's legitimacy][bbc_ridiculous_objections] and required Mike to meet regulations that he [considered burdensome][bbc_ridiculous_objections]. Thus, he built and operated the road without, and arguably inspite of, government incentives. What remains to determine is whether the government required some of the favorable policies he had (see Results below).

The possible incentives which arguably remain are profit, noteriety, desire to use the road himself, and desire to help his community. Profit incentive may also be unlikely however, since he often spoke of what it would take to [break even][bbc_needs_more_vehicles].

# Results

The results in this story are mixed. Mike and his wife did not end up breaking even, though he claims to [still be happy][telegraph_story] with his decision to build the road, and the community seems to be grateful to him for it. He offered certain amenities that one would expect from a public access road. However, the Council named some complaints about potential damage done by the road.

## Financial

Mike's initial investment was for [£150,000][bbc_ridiculous_objections]. In the end, Mike and his wife ended up losing roughly [£10,000 to £15,000][bbc_toll_road_closed]. However, it was in large part because the Council repaired the A431 Kelston Road [more than a month ahead of schedule][guardian_article]. Mike also claims that he [spent £25,000][bbc_ridiculous_objections] meeting the Council's regulations.

## Amenities

The road was [free to use][guardian_article] by emergency vehicles. He also [offered a discount][oldfield_school_discount] to parents of local Oldfield School students, who frequently needed to pass the A431 obstruction. However that discount was [discontinued][oldfield_school_discount] about a month later when the toll road's financial outlook worsened.

## Damage

According to the Council mentioned that there was a possibility of [damage to an archaeological][archaeology] site:

> The toll road goes through an area of medieval strip lynchets and field boundary earthworks and will have damaged the archaeology. Ordinarily a development proposed in an area of known archaeological interest would be required to carry out archaeological investigations before going ahead so that any archaeological remains are recorded, but as the development went ahead without consent this didn’t happen. Archaeological investigations will need to be carried out during the reinstatement of the land in order to understand what was there and the damage caused.

However, Watts [objected][archaeology] that the Council took a long time to raise this objection:

> There have been objections on the website regarding archaeology – where have they dug that up from at this late stage?

# Sources

## References

### BBC

* [Story][bbc_toll_road_created] about the toll road being created.
* [Story][bbc_toll_road_created] about the toll road closing.
* [Mike][bbc_ridiculous_objections] commenting on "ridiculous objections" of the Council.
* [Mike explains][bbc_needs_more_vehicles] that more vehicles need to use the toll road for it to break even.
* [Oldfield School][oldfield_school_discount] announces discount, and discontinuation of discount, for parents dropping off their children (Sept 8 2014, Oct 3rd 2014 respectively).
* [Bath Chronicle story][archaeology] which mentions objections about archaeological damage.

### Other

* [Guardian Article][guardian_article]
* [Telegraph News Story][telegraph_story]

## Other Resources

* [Sky News Story][sky_news_story]
* [Wikipedia article](http://en.wikipedia.org/wiki/Kelston_toll_road)
* [Youtube video](https://www.youtube.com/watch?v=bMuNZM1qY7g) wherein Mike describes his plans for the road.
* [Youtube video](https://www.youtube.com/watch?v=E-m6vc7lsmE) feature of the toll road in operation, by Tom Scott, including interview with Mike.
* BBC - [Mike predicts][bbc_will_recoup_costs], on Oct 8 2014, that the toll road will recoup its costs.
* [Various statements][council_statements] by the Bath & North East Somerset Council, with regard to the A431 Kelston Road as well as the toll road.

[sky_news_story]: http://news.sky.com/story/1312884/fed-up-driver-builds-own-toll-road-in-somerset
[telegraph_story]: http://www.telegraph.co.uk/news/uknews/road-and-rail-transport/11008835/Entrepreneur-builds-private-toll-road-after-landslip.html
[bbc_toll_road_created]: http://www.bbc.com/news/uk-england-somerset-28610943
[bbc_toll_road_closed]: http://www.bbc.com/news/uk-england-somerset-30078393
[bbc_ridiculous_objections]: http://www.bbc.com/news/uk-england-somerset-29984426
[archaeology]: http://www.bathchronicle.co.uk/Race-time-Kelston-Toll-Road-boss-Mike-Watts-break/story-23837045-detail/story.html
[bbc_will_recoup_costs]: http://www.bbc.com/news/uk-england-somerset-29537608
[guardian_article]: http://www.theguardian.com/world/shortcuts/2014/dec/17/long-journey-down-very-short-private-toll-road?CMP=share_btn_tw
[bbc_needs_more_vehicles]: http://www.bbc.com/news/uk-england-somerset-29009420
[oldfield_school_discount]: http://www.oldfieldschool.com/news/
[council_statements]: http://www.bathnes.gov.uk/services/streets-and-highway-maintenance/roadworks/major-transport-schemes/a431-kelston-road-council