<!-- title: Paving For Pizza (Domino's) -->
<!-- summary: Domino's offers grants to governments of select cities around the United States to repair potholes. -->

In 2018, Domino's Pizza created a program called [Paving For Pizza][site_home] to fund road repair, particularly to [fix potholes][sun_sentinel], in select towns. This is [done on the pretense][sun_sentinel] of wanting to avoid pizzas getting shaken up during delivery. In Milford, Delaware, the repairs were [conducted by workers][citylab] supplied by the city government. However, the city agreed to [paint a Domino's logo][citylab] with the phrase "Oh Yes We Did" over the repairs with a stencil supplied by Domino's. (These may also be true for other participating towns.)

# Incentives

Given the logos on the street, this is likely done for the sake of notoriety and advertising. Perhaps there is truth to the pretense of a less bumpy ride and a more reliable pizza delivery, however the benefit would be limited to the relatively few cities they service. CityLab [offers an opinion][citylab] of this program as a common form of publicity stunt.

# Results

The [program was launched][sun_sentinel] in June 2018. As of January, 2019, it [had fixed potholes][sun_sentinel] in 15 states.

In 2018, for example, they [repaired 40 potholes][cbs_milford] across 10 different roads in Milford, Delaware.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ddZxGAcZ06I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As of January, 2019, Miami-Dade [had been selected][sun_sentinel] for the next grant.

Another thing worth noting is that the contract for Athens, Georgia (and perhaps others) [stipulated a deadline][citylab] for the repair of its potholes.

## Government Involvement

Though Domino's is funding the paving projects, the city governments are still (at least in some cases) the ones [conducting the paving][citylab]. (This may seem to go without saying, but it is [not always the case][itsnotgov_portland_anarchist_road_care] with unconventional pothole repairs). Additionally, the pothole repair contracts (again, at least in some cases) required the government to [stencil their logo][citylab] over the potholes, and [promise not to portray][citylab] them negatively.

# Sources

## References

[site_home]: https://www.pavingforpizza.com/
* [Paving For Pizza][site_home] - official website

[sun_sentinel]: https://www.sun-sentinel.com/entertainment/restaurants-and-bars/fl-ne-dominos-pizza-paving-roads-miami-dade-20190128-story.html#
* [Domino's is paying to fix South Florida roads so your pizza will be delivered unharmed][sun_sentinel] - South Florida Sun Sentinel

* CBS Philly
[cbs_milford]: https://philadelphia.cbslocal.com/2018/06/11/dominos-pizza-milford-delaware/
    * [Domino’s Pizza Repaves Potholes In Milford, Delaware][cbs_milford] - news article
[youtube_cbs_milford]: https://www.youtube.com/watch?v=ddZxGAcZ06I
    * [Domino's Pizza Paying To Have Roads Paved In The United States][youtube_cbs_milford] - on YouTube

[citylab]: https://www.citylab.com/transportation/2018/06/dominos-pizza-is-fixing-potholes-now-and-thats-fine/562829/
* [Why Domino’s Pizza Is Fixing Potholes Now][citylab] - CityLab

## Other Sources

[snopes]: https://www.snopes.com/fact-check/dominos-pizza-paving/
* [Snopes rating: True][snopes]

## Further Reading

[fee]: https://fee.org/articles/without-government-who-will-build-the-roads-domino-s-pizza-apparently/
* [Without Government, Who Will Build the Roads? Domino’s Pizza, Apparently][fee] - Foundation for Economic Education

## See Also

[itsnotgov_portland_anarchist_road_care]: /infrastructure/roads/portland-anarchist-road-care/
* [Portland Anarchist Road Care][itsnotgov_portland_anarchist_road_care]
