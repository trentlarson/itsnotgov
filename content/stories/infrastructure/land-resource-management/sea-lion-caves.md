<!-- title: Sea Lion Caves (Oregon) -->

<!-- redirect: /w/c/Infrastructure/land_resource_management/sea_lion_caves -->

[Sea Lion Caves][site_home] is a [privately owned][site_about] animal sanctuary near Florence, Oregon, which was [opened to the public][site_cave] in 1932. It has been a breeding ground for sea lions [for generations][oregon_live_caves_article]. Rather than keeping the animals in captivity, the operators of the cave simply [protect the area][site_faq] for animals to come and go as they choose. Since its early years, it has served as a tourist attraction. Visitors can make their way down from the highway above and take a look at the animals inside the cave.

# Results

## Success

### Funding

The property containing the caves was [purchased in 1927][site_cave] by R.E. Clanton from William Cox. Clanton's intention was to start a business. J.G. Houghton and J.E. Jacobson later joined, and the three [risked everything][site_cave] they had to build a safe access route from the highway above down to the caves. In 1961, an [elevator][oregon_encyclopedia_article] costing $180,000 was added.

Today, the caves turn a profit, getting its money from (perhaps among other sources) [admission fees][site_faq] and its [gift shop][oregon_encyclopedia_article]. It was even [commended][oregon_encyclopedia_article] by the governor of Oregon for its financial success. By 1981 [it had a gross income][oregon_encyclopedia_article] of 1 million and [attracted 200,000][oregon_encyclopedia_article] visitors annually.

### Size and scope

The Sea Lion Caves contain 2 acres of cave floor.

## Question of Sea Lion Overpopulation

It is interesting to note that there is as of 2016, the National Oceanic and Atmospheric Administration (NOAA) [created an exception][oregon_live_noaa_article] to its rules against the killing of sea lions due to, as some have claimed, their adverse effect on fish populations.

## Government Involvement

### Adversity

In the early days, the caves provided sanctuary to animals arguably in spite of the government's attitude toward preservation. The federal [Marine Mammal Protection Act][mmpa_wikipedia_article] had not yet passed. Until 1950, there was even a [bounty paid][oregon_encyclopedia_article] by the state of Oregon on sea lions. Each hunter could obtain an annual total of $10,000.

In 1977, the government of Oregon [tried and failed][oregon_encyclopedia_article] to take control of the caves.

### Help

#### Property Issues

This being a discussion of the potential of private organizations performing land resource management without the government, a few words on government involvement in property rights management should be added.

First, it should be noted that there is dependency here on the government to assert property rights. Secondly, it should be noted that while the property containing the caves was purchased by R.E. Clanton from William Cox in 1927, Cox himself [initially purchased it][site_cave] from the government in 1887. However, at least it can be said here that the land was sold to the owners of the sanctuary at market rates, and that the sanctuary was subsequently financially sustainable.

The above issues, of course, will apply to the vast majority of such stories.

#### Other

Though R.E. Clanton had purchased the land in 1927, J.G. Houghton and J.E. Jacobson only joined the business after it was clear that US Route 101 (presumably a government project) would give potential visitors easy access to their location.

In 1972, the Nixon administration passed the [Marine Mammal Protection Act][mmpa_wikipedia_article]. This act has prohibited, with few exceptions, the hunting of marine mammals.

The caves are currently part of the [Oregon Marine Reserves][site_about], which includes some legal protections. (The extent of its relationship with the Sea Lion Caves is unclear)

# Sources

## References

### Main Site
* [Home][site_home]
* [About][site_about]
* [F.A.Q][site_faq]
* [History and cave details][site_cave]

### Oregon Live articles

* [About the caves][oregon_live_caves_article]
* [About NOAA][oregon_live_noaa_article] granting exceptions to sea lion hunting prohibitions

### Other

* [Oregon Encyclopedia article][oregon_encyclopedia_article]

## Other Sources

* [Sea Lion Caves][caves_wikipedia_article] Wikipedia article
* [Marine Mammal Protection Act][mmpa_wikipedia_article] Wikipedia article



[site_home]: http://www.sealioncaves.com
[site_about]: http://www.sealioncaves.com/about.php
[site_cave]: http://www.sealioncaves.com/cave.php
[site_faq]: http://www.sealioncaves.com/FAQ.php

[oregon_encyclopedia_article]: https://oregonencyclopedia.org/articles/sea_lion_caves/
[oregon_live_caves_article]: http://www.oregonlive.com/travel/index.ssf/2017/08/sea_lion_caves_still_wild_afte.html
[oregon_live_noaa_article]: http://www.oregonlive.com/pacific-northwest-news/index.ssf/2016/07/noaa_authorizes_oregon_to_cont.html

[mmpa_wikipedia_article]: https://en.wikipedia.org/wiki/Marine_Mammal_Protection_Act
[caves_wikipedia_article]: https://en.wikipedia.org/wiki/Sea_Lion_Caves