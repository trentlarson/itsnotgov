<!-- title: Cheetah Conservation Fund - Dogs helping save Cheetahs from extinction (Namibia) -->

The [Cheetah Conservation Fund][site_home] is a non-profit organization, formed in 1990, with [several programs][site_conservation] to promote the conservation of cheetahs in the African nation of Namibia. One of their earliest programs was to [give guard dogs to farmers][africa_geographic] to protect their livestock from preying cheetahs, thereby reducing the farmers' need to kill them. The Cheetah Conservation Fund [were recognized][usc_tyler] for this and other projects as a 2010 recipient of the Tyler Environmental Prize.

The dog placement program [started in 1994][africa_geographic]. The dog breeds, Anatolian Shepherd and Kangal, were [chosen for their history][africa_geographic] of guarding livestock in Turkey, which has a similar environment to Namibia. There is evidence that guard dogs [eliminate livestock losses][science_daily] in the vast majority of farms, reducing incentives for farmers to kill cheetahs on their land.

The program is [part of a larger approach][mongabay] of developing relationships with farmers, helping them with their predator problem, and teaching them to coexist with cheetahs. Dr. Laurie Marker, founder of Cheetah Conservation Fund, [claimed in a 2008 interview][mongabay],

> Because CCF tries to help farmers who have a predator problem, we are trusted by most locals. We don't wag our finger at people who have killed cheetahs; on the contrary, we try to help them and make them our friends so they will think twice before killing another cheetah. It's worked extremely well. Whenever we drive around in our cheetah bus or other CCF vehicles, people wave to us like we're celebrities.

# Results

## Cheetahs saved

By 2015, the guard dog program had [placed more][africa_geographic] than 650 dogs. A University of Kent study published in 2013 [provided evidence][science_daily] that the use of guard dogs was effective in protecting livestock, and ultimately the incidence of the killing of cheetahs.

## Funding

According to [charity navigator][charity_navigator], Cheetah Conservation Fund received $0 in government grants in 2009. The word "government" is not present in their [2016 audit report][site_financials_2016].

## Government Involvement

While their guard dog placement program appears to be an independent project, some of their other projects involve varying degrees of partnerships or other interactions with the government.

For example, they have a program called [Bushblok][site_conservation] to transform encroaching bush in Namibia into a biomass fuel, clearing Cheetah habitat in the process. It was researched [in partnership with][site_conservation] the United States Agency for International Development. In 2012 they [received support][clinton_bushblok] from the Clinton Global Initiative to expand this program. The Clinton Foundation has [received donations][clinton_donors] from governments.

They have also [partnered with government agencies][site_conservation] in efforts to curb illegal Cheetah trade.

# Sources

## References

* Cheetah Conservation Fund website
[site_home]: https://cheetah.org
    * [Home page][site_home]
[site_conservation]: https://cheetah.org/what-we-do/conservation/
    * [Overview of conservation programs][site_conservation]
[site_financials_2016]: https://cheetah.org/site/wp-content/uploads/2017/10/Cheetah-2016-audit-report.pdf
    * [Financial Statements And Independent Auditor’s Report][site_financials_2016]

[charity_navigator]: https://web.archive.org/web/20111106044912/https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=6617
* [Charity Navigator page][charity_navigator], Fiscal Year 2009

[africa_geographic]: https://magazine.africageographic.com/weekly/issue-75/saving-cheetahs-a-need-for-speed/
* [2015 article on Africa Geographic][africa_geographic] about the Cheetah Conservation Fund
    * Note that this article was written by the founder, so should not be used as a source of validation of their programs
[clinton_bushblok]: https://www.clintonfoundation.org/clinton-global-initiative/commitments/cheetah-conservation-fund-bushblok-project
* [Clinton Foundation overview][clinton_bushblok] of 2012 partnership with Cheetah Conservation Fund
[clinton_donors]: https://www.clintonfoundation.org/contributors
* [Clinton Foundation donors page][clinton_donors]

[usc_tyler]: https://pressroom.usc.edu/2010-tyler-environmental-prize-winners-announced/
* [Tyler Environmental Prize 2010 Winners][usc_tyler] at University of Southern California

[science_daily]: https://www.sciencedaily.com/releases/2013/11/131126102311.htm
* [Guard dogs reduce killing of threatened species][science_daily] - 2013 study published on Science Daily

[mongabay]: https://news.mongabay.com/2008/10/cheetah-population-stabilizes-in-namibia-with-support-from-farmers/
* [Cheetah population stabilizes in Namibia with support from farmers][mongabay] - 2008 article on Mongabay

## Other Sources

[fastcompany]: https://www.fastcompany.com/1681363/saving-cheetahs-by-making-them-friends-with-farmers
* [Fast Company article][fastcompany] about Cheetah Conservation Fund
[wikipedia]: https://en.wikipedia.org/wiki/Cheetah_Conservation_Fund
* [Wikipedia article][wikipedia] about Cheetah Conservation Fund

## See Also

[youtube_bushblok]: https://www.youtube.com/watch?v=CuShsDlRvk8
* [Dr. Laurie Marker speaking about Clinton Global Initiative commitment: Bushblok Project][youtube_bushblok]
[youtube_letterman]: https://www.youtube.com/watch?v=ol96VD4M1q8
* [Cheetah and Anatolian Shepherd dog][youtube_letterman] on the Tonight Show with David Letterman

[itsnotgov_american_prairie_reserve]: /infrastructure/land-resource-management/american-prairie-reserve
* [American Prairie Reserve and Wild Sky Beef][itsnotgov_american_prairie_reserve] - Beef ranchers that preserve wildlife no their land in Montana
