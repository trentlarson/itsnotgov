<!-- title: Hawk Mountain (Pennsylvania) -->

Hawk Mountain is a [member supported][site_about] bird sanctuary near Kempton, Pennsylvania. It was started by [Rosalie Edge][biography], a suffragette and conservationist. At the time, the state of Pennsylvania was [paying a bounty of $5][biography] for each goshawk shot down. With [financial help][biography] from her zoologist friend William Gibbs Van Name she [leased and eventually purchased][biography] 1,400 acres of land, officially creating the sanctuary. It was [2,500 acres][mcall] as of 2018. [Thousands of hawks][mcall] are seen there annually.

# Government Involvement

Hawk Mountain receives some government grants. According to their financial statements, the amount of revenue that falls under the category of "grant" (which may include some non-government grants) is relatively small. In FYE 2013, for example, they reported [$96,203][site_audit_13_14] in grants compared to [$401,960][site_audit_13_14] in membership dues and [$229,769][site_audit_13_14] in admissions charges.

However, in late 2013, [they were slated to receive][site_large_grant] $550,000 in grants from the state of Pennsylvania, which does not seem to be reflected in their [FYE 2014 financial statement][site_audit_13_14]. So the details are unclear. Perhaps it falls under another category within the financial statements.

# Sources

## References

* Official Website:
    * ["Who We Are"][site_about]
    * [Financial audit, FY 2013-2014][site_audit_13_14]
    * [Story about receiving $550,000 in grants][site_large_grant] from the Pennsylvania Department of Conservation and Natural Resources
* [Article][mcall] in The Morning Call (local newspaper in Pennsylvania)
* [Biography.com][biography] article about Rosalie Edge

## Other Sources

* Hawk Mountain financial details:
    * [Finances overview][site_finances]
    * [FY 2012-2013 audit][site_audit_12_13]
    * [FY 2014-2015 audit][site_audit_14_15]
    * [FY 2015-2016 audit][site_audit_15_16]

* [Wikipedia article][wikipedia]

[site_about]: http://www.hawkmountain.org/who-we-are/who-we-are/page.aspx?id=388
[site_finances]: https://www.hawkmountain.org/who-we-are/finances/page.aspx?id=4645
[site_audit_12_13]: https://www.hawkmountain.org/data/streamfile.aspx?id=NzcwODI%3d&name=Audit+2012+2013.pdf
[site_audit_13_14]: https://www.hawkmountain.org/data/streamfile.aspx?id=Nzc5MTE%3d&name=Audit+FY+2013+2014.pdf
[site_audit_14_15]: https://www.hawkmountain.org/data/streamfile.aspx?id=Nzg0NDM%3d&name=Audit+FY+2014-15.pdf
[site_audit_15_16]: https://www.hawkmountain.org/data/streamfile.aspx?id=NzkxMjY%3d&name=Audit+2015-16.pdf
[site_large_grant]: https://www.hawkmountain.org/who-we-are/news/sanctuary-receives-three-grants-totaling-550-000/page.aspx?id=4517

[biography]: https://www.biography.com/people/rosalie-edge-072715
[mcall]: https://www.mcall.com/sports/outdoors/mc-outdoors-hawk-mountain-20161003-story.html

[wikipedia]: https://en.wikipedia.org/wiki/Hawk_Mountain_Sanctuary
