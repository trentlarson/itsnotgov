<!-- title: Matatu (Private Bus) - Informal Transit system in Nairobi -->

<!-- redirect: /w/c/Infrastructure/public_transit/matatu_private_bus_informal_transit_system_in_nair -->

A [Matatu][matatu_wikipedia] is a type of privately owned bus, which serves (among other places) Nairobi, Kenya. In the 1990s, the government owned public [transit system collapsed][standard_collapse]. The Matatu market is [heavily competitive][transitapp], which has leads to [innovation][transitapp], but also [chaos][citylab_informal] and even [violence][daily_nation]. In 2014, a project called Digital Matatu [set out to map][citylab_map] the Matatu system, and found it to be more orderly than passengers or government were aware of. With a formalized description of the system in place, the software company [Transit][transit_wikipedia], from Montreal, incorporated it into their phone application in hopes of making passengers more aware of travel options.

[TOC]

# Results

## Finance and Management

For all its faults, the Matatu system was shown to survive in the marketplace and meet demand, even in a time [when the government][transitapp] was unable to manage a transit system. The government system [seems to have failed][standard_collapse] in the 1990s due to explosive population growth and change, and perhaps due to a change in management of the public transit department. However over the years, the government has had more of a hand in regulating the matatu system and plans to [build a light rail][citylab_informal] system.

## Reliability

The price of Matatus are [known to be][transitapp] chaotic. Navigation of the Matatu system was also known to be [chaotic, and a hassle][transitapp]. For example, they [don't always stop][citylab_map] at official stops.

However, despite its chaos, a study called Digital Matatus [showed the informal system to have][transitapp] 130 regular routes, congregating around regular stops. Overall it was found to be [more regular][citylab_map] than anyone realized. The Digital Matatus project, [completed in 2014][transitapp], involved traveling around the system with GPS trackers. It was conducted by a combination of public and private universities (more on this below).

Jacqueline Klopp, a member of the project, [stated][citylab_map]:

> Look, these people have planned your system from below!
>
> ...
>
> It is not as chaotic as people think it is. They have routes, they have numbers. There’s very, very regular stops that the city didn’t plan. I think it really helps people to see that there is this system that you can then improve on, that it’s not just a chaotic mess.

Later, [Transit][transitapp], a company from Montreal which makes a transit system navigation app, added the Digital Matatus map to their application, with the aim of making navigation easier for Nairobi residents and visitors. The hope is that if commuters were made aware of how regular the system was, they could make better use of it.

### Government System Comparison

There is of course no modern government run transit system in Nairobi to compare it to. However, in the 1930s through 1970s, the government public transit system was [not known to be chaotic][standard_collapse], and stayed on schedule.

## Amenities

One benefit of the inadvertent privatization of the Nairobi transit system has been various amenities that the Matatu companies have added to their vehicles. Matatus are adorned with [flair and branding][nyt]. Some have [video and sound systems][transitapp], and occasionally [onboard WiFi][transitapp].

## Safety

Matatus have had a reputation to come with a risk of violence against passengers, particularly [female passengers][qz]. The Matatus also have a bad reputation for [road safety][citylab_informal]. [They are known][transitapp] to drive on unpaved streets and disrupt traffic.

The cutthroat competition has also lead to violence [between companies][daily_nation] competing for routes.

## Government Involvement

The [original matatu routes][citylab_map] were based on transit lines established by the old government system, but it has diverged since then.

[By 2015][standard_cleanup], the city had started [enforcing regulations][nyt] to address safety concerns, purportedly with success. There has also been some attempt at self-regulation within the Matatu industry, including the Smart Matatu project. However it was [arguably induced][citylab_informal] by threat of government regulation.

The Digital Matatu project, which mapped out the informal system, was conducted in a [joint effort][transitapp] by MIT, Columbia University, and University of Nairobi, the last of which is a public institution. It may also be [used in the future][citylab_map] by the government to legally formalize the system.

# Sources

## References

* [Quartz][qz]
* [New York Times][nyt]
* [article in][daily_nation] [Daily Nation](https://en.wikipedia.org/wiki/Daily_Nation)
* Articles at [The Standard](https://en.wikipedia.org/wiki/The_Standard_(Kenya)) (major Kenyan news source):
    * About [collapse of government][standard_collapse] system
    * About how the Matatu System is [better in 2015][standard_cleanup] due to gov't intervention.
* Citylab articles:
    * About the [Digital Matatu project][citylab_map]
    * [Overview of][citylab_informal] of informal transit in Nairobi
* [Medium Post][transitapp] made by the Transit software company in Montreal, about incorporating Digital Matatu data into their application.

## Additional Resources

Wikipedia Articles:

* [Matatu][matatu_wikipedia]
* [Transit (app)][transit_wikipedia]
* Smart Matatu: - a driver [safety study](http://blog.smartmatatu.com/) and [app](http://smartmatatu.com/)
* Digital Matatu Project [scientific paper][scientific_paper]
* Digital Matatu Project [official website](http://www.digitalmatatus.com/)

[matatu_wikipedia]: https://en.wikipedia.org/wiki/Matatu
[transit_wikipedia]: https://en.wikipedia.org/wiki/Transit_(app)
[scientific_paper]: https://www.sciencedirect.com/science/article/pii/S0966692315001878
[citylab_informal]: https://www.citylab.com/transportation/2017/08/the-future-of-nairobis-informal-transit/537573/
[citylab_map]: https://www.citylab.com/transportation/2014/02/what-informal-transit-looks-when-you-actually-map-it/8283/
[transitapp]: https://medium.com/transit-app/hello-nairobi-cc27bb5a73b7
[standard_collapse]: https://www.standardmedia.co.ke/article/2001275307/genesis-of-chaos-how-nairobi-s-public-transport-system-collapsed
[qz]: https://qz.com/1163608/kenya-wants-to-make-its-popular-matatu-commuter-buses-safe-for-women/
[nyt]: https://www.nytimes.com/2018/04/14/travel/transportation-turned-performance-art-nairobis-matatu-crews.html
[standard_cleanup]: https://www.standardmedia.co.ke/article/2000181987/matatu-sector-has-greatly-improved
[daily_nation]: https://www.nation.co.ke/oped/1192-272318-ltpshp/index.html