<!-- title: Streetcars in North America -->

<!-- redirect: /w/c/Infrastructure/public_transit/streetcars_in_the_north_america -->


In the mid 1800s to early 1900s, streetcars in urban centers were primarily provided by [private companies][eh]. They started with [horse-drawn omnibuses][eh] and later moved on to [cable cars][brooklyn_rail], and then [electric lines][brooklyn_rail].

The end of the era of competing streetcar companies arguably came when [high traffic density][vox] due to increasingly popular automobile traffic made it more difficult for them to be timely. Meanwhile some streetcar companies contracted with municipal governments for the [monopoly][vox] to operate in the city. Some of these governments then started [stipulating a low passenger fare][vox] that made it difficult to maintain profitability. Over time, larger companies such as [National City Lines][vox] started buying out the smaller, failing companies. Eventually municipal bus lines and trains [took over][eh].

# Government Involvement

## Official Routes

The trolley lines, at least in New York City in the 1850s, were [given government franchises][eh] for specific routes.

It would be good to know whether this was a government granted monopoly over the route, or whether multiple companies were allowed to compete to serve the same route.

## Infrastructure

Though the companies were privately owned, there is the question of paying for and building infrastructure. This includes the rails along the streets, the streets themselves, cables, and electric lines. According to the [Brooklyn Historic Railway Association][brooklyn_rail], in the 1920s:

> Track repairs were often hindered by demands on transit companies to also repair adjoining streets.

Which seems to imply that the companies were in charge of building it themselves, at least in some cities, though it is not clear. Presumably the government was still in the role of determining right-of-way between the companies.

More details on this topic would be good to have.

# Sources

## References

* [Urban Mass Transit In The United States][eh] - Economic History Association
* [The real story behind the demise of America's once-mighty streetcars][vox] - Vox
* [History of the Streetcar][brooklyn_rail] - The Brooklyn Historic Railway Association

## Other Sources

* [Wikipedia article][wikipedia] on the history of streetcars.

## Further Reading

* [The Great American Streetcar Myth][market_urbanism] - Market Urbanism
* [A Desire Named Streetcar][cato] - Article from Cato Institute

[vox]: https://www.vox.com/2015/5/7/8562007/streetcar-history-demise
[eh]: https://eh.net/encyclopedia/urban-mass-transit-in-the-united-states/
[wikipedia]: https://en.wikipedia.org/wiki/Streetcars_in_North_America
[market_urbanism]: https://marketurbanism.com/2010/09/23/the-great-american-streetcar-myth/
[brooklyn_rail]: http://www.brooklynrail.net/info_streetcar.html
[cato]: https://www.cato.org/publications/policy-analysis/desire-named-streetcar-how-federal-subsidies-encourage-wasteful-local-transit-systems
