<!-- title: The Water Box -->
<!-- summary: Jaden Smith and friends respond to the continued need for clean water in Flint Michigan with a mobile water filtration system. -->

![Water Pickup sign](/images/stories/infrastructure/water/sign.jpg)

The [Water Box][nbc25] is an experimental mobile water filtration system donated to the First Trinity Missionary Baptist Church in Flint, Michigan on March 2nd 2019. It is [designed to clear][abc12] contaminants including lead and bacteria from water taken from a compromised municipal source such as Flint's, making it safe for consumption. Its development, including conception and engineering, [was a collaboration][nbc25] between multiple organizations, including [501cTHREE][501cthree], [The Last Kilometer][last_km_water], Rethink H2O, Black Millenials for Flint, and the church. Notably, one of the members of 501cTHREE is celebrity Jaden Smith (actor, musician, actor, son of actor Will Smith), which brought a lot of attention to the event. He also notably runs a water company called [JUST Water][just_water] with Drew FitzGerald, who is also his partner at 501cTHREE. Accounts of the project's development are somewhat unclear, but it appears that Smith and FitzGerald [gathered requirements][mlive] based on the church's needs and The Last Kilometer [did the engineering work][nbc25].

<iframe width="560" height="315" src="https://www.youtube.com/embed/S_J7fgRguBY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Background

The device was designed during the [crisis in Flint][cnn_recap] wherein the state of Michigan changed its source of water from Lake Huron to the Flint River as a cost-saving measure. The river's water is [highly corrosive][cnn_recap] and was not treated with the necessary anti-corrosive agent when used as the city's water supply. This resulted in [lead and iron][cnn_recap] from pipes leaching into water, which is hazardous, especially to children. As a result, various mitigations were put in place to give the residents a safe source of water to work with while the pipes are being replaced. One mitigation is a [program][cnn_michigan_water_end] run by the state of Michigan, giving a ration of bottled water to Flint residents. Another one is an [outpouring of private donations][cnn_donations] of bottled water, in large part by celebrities and major companies. From 2014 to 2019, the First Trinity Missionary Baptist Church distributed [over five million water bottles][abc12] of the donated water.

As of 2019, [many municipal pipes][detroit_news] have been replaced, and lead in the water is now testing below the federal action limit. By some accounts, [Michigan state authorities claim][cnn_michigan_water_end] that it is now safe to drink. By [some other accounts][mlive], there are government agencies (including the federal Environmental Protection Agency) which say that risk of dangerous contamination from water transmission pipes remains. Additionally, some homes [may contain][motherjones] lead pipes that need to be replaced. The end of the state's free water program was also announced, though the [recently elected governor][detroit_news] seems to indicate she may extend it. At any rate, residents are [slow to trust][cnn_michigan_water_end] the government. This means that, as of 2019, residents are [still accepting][mlive] donated water, while less water is being donated than before.

<iframe width="560" height="315" src="https://www.youtube.com/embed/v73sFL7AOlo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Results

## Amount of Water

With The Water Box, the church is [able to produce][abc12] 8-10 gallons of clean water per minute, filtering out lead, bacteria, and other contaminants. This likely will not alone meet the needs of residents, but assuming this pilot program is successful Smith [plans to expand][mlive] the program with more water boxes in Flint and beyond.

## Quality of Water

Part of this project involves the church [conducting regular tests][mlive] on the quality of the water, both locally using handheld devices, and using an independent laboratory (notably, unaffiliated with the city of Flint). They post the test results [on a website][firsttrinitywater_test]. This is important given that trust is such an issue for the residents.

## Funding

It's often difficult to find information about sources of funding for such projects or organizations, and this case is particularly so. There appears to be no explicit statement in news reports or even the organizations' own websites as to where exactly the money is coming from. However, the news stories do seem to imply that Jaden Smith is broadly responsible for this project. And, a couple of explicitly stated claims corroborate the notion that Jaden Smith, Drew FitzGerald, and/or the Smith family are financially responsible. Firstly, 501cTHREE [will be paying][nbc25] for the church's utility and maintenance costs during water distribution, according to FitzGerald and Smith. Secondly, Jaden's mother Jada Pinkett Smith [has already announced][nbc25] that she would donate a second water box in Flint (though, this is still presumably much less than the cost of first box which included research and development).

The project is also [accepting donations][firsttrinitywater_help] from the general public. [Anybody can sponsor][firsttrinitywater_help] another water box for $50,000 or purchase water jugs for residents to fill up with water from the box.

## Government involvement

The Last Kilometer [has done][last_km_govt] some government-funded projects in the past.

The water used as input to the Water Box comes from a municipal source.

As stated above, the state of Michigan [has its own][cnn_michigan_water_end] free bottled water program for Flint residents, funded by (as of 2018) a $450 million state and federal aid package.

Flint mayor Karen Weaver [seems to be friendly][nbc25] with Jada Pinkett Smith in the context of this project.

# Sources

## References

* First Trinity Water - A website for The Water Box project
[firsttrinitywater_test]: https://www.firsttrinitywater.com/test-results (First Trinity Water - Water testing results)
    * [Water testing results][firsttrinitywater_test]
[firsttrinitywater_help]: https://www.firsttrinitywater.com/how-you-can-help (First Trinity Water - How You Can Help)
    * [How You Can Help][firsttrinitywater_help]

* The Last Kilometer website
[last_km_water]: https://www.thelastkm.org/single-post/2019/03/01/Mobile-Water-Treatment-for-Flint-MI (The Last Kilometer - Community-based Water Treatment for Flint, MI)
    * [Community-based Water Treatment for Flint, MI][last_km_water]
[last_km_govt]: https://www.thelastkm.org/single-post/2017/02/27/MiniGrid-for-UN-Peace-Operations (The Last Kilometer - Birendra Peace Operations Training Center)
    * [Birendra Peace Operations Training Center][last_km_govt] (wherein they accepted government funding)

[501cthree]: https://501cthree.org (501cTHREE)
* [501cTHREE Website][501cthree], which includes an explanation of how the Water Box works

[just_water]: https://www.justwater.com/ourwater/ (Just Water - Our Water)
* Just Water website - [Our Water][just_water] - An overview of what the company does.

* CNN
[cnn_recap]: https://www.cnn.com/2016/01/11/health/toxic-tap-water-flint-michigan/ (CNN - How tap water became toxic in Flint, Michigan)
    * [How tap water became toxic in Flint, Michigan][cnn_recap]
[cnn_donations]: https://money.com/money/4194456/flint-water-crisis-donations/ (CNN - Here’s Who Has Donated the Most Bottled Water to Flint)
    * [Here’s Who Has Donated the Most Bottled Water to Flint][cnn_donations]
[cnn_michigan_water_end]: https://www.cnn.com/2018/04/07/us/flint-michigan-water-bottle-program-ends/index.html (CNN - Michigan will end Flint's free bottled water program)
    * [Michigan will end Flint's free bottled water program][cnn_michigan_water_end]
[detroit_news]: https://www.detroitnews.com/story/news/local/michigan/2019/01/21/whitmer-flint-needs-bottled-water-pipes-replaced/2638902002/ (Detroit News - Whitmer: Flint needs bottled water until pipes replaced)
* Detroit News - [Whitmer: Flint needs bottled water until pipes replaced][detroit_news]

[nbc25]: https://nbc25news.com/news/local/jaden-smith-visits-flint-for-launch-of-the-water-box (NBC 25 News - Jaden Smith visits Flint for launch of The Water Box)
* NBC 25 News - [Jaden Smith visits Flint for launch of The Water Box][nbc25]

[abc12]: https://www.abc12.com/content/news/Jaden-Smith-bringing-The-Water-Box-to-Flint-506616531.html (ABC 12 News - Jaden Smith bringing "The Water Box" to Flint)
* ABC 12 News - [Jaden Smith bringing "The Water Box" to Flint][abc12]

[mlive]: https://www.mlive.com/news/2019/03/jaden-smith-visits-flint-church-to-unveil-water-box.html (MLive.com - Jaden Smith visits Flint church to unveil Water Box)
* MLive.com - [Jaden Smith visits Flint church to unveil Water Box][mlive]

[motherjones]: https://www.motherjones.com/environment/2018/04/officials-say-flints-water-is-safe-residents-say-its-not-scientists-say-its-complicated/ (Mother Jones - Officials Say Flint’s Water Is Safe. Residents Say It’s Not. Scientists Say It’s Complicated.)
* Mother Jones - [Officials Say Flint’s Water Is Safe. Residents Say It’s Not. Scientists Say It’s Complicated.][motherjones]

* YouTube
[youtube_501cthree]: https://www.youtube.com/watch?v=S_J7fgRguBY (YouTube - 501cTHREE - The Water Box in Flint)
    * [The Water Box in Flint][youtube_501cthree] - 501cTHREE
[youtube_news]: https://www.youtube.com/watch?v=v73sFL7AOlo (YouTube - WNEM TV5 - Jaden Smith's Water Box makes its Flint public debut)
    * [Jaden Smith's Water Box makes its Flint public debut][youtube_news] - WNEM TV5 News
