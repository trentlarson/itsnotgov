<!-- title: Benson Leung's USB cable reviews -->

<!-- redirect: /w/c/Regulation/ProductSafety/benson_leung_usb_cable_reviews -->

*This article is a stub. You can help out by finding and filling out useful details.  See our [writing guidelines](/help/story-writing-guidelines).*

Benson Leung is a popular reviewer of USB cables [on Amazon.com][amazon_profile].

Others have compiled lists of products that Benson has approved. On droid-life.com there is a list of ["Benson approved" USB-C adapters][benson_approved_adapter_list]. A Reddit user compiled a [spreadsheet][benson_approved_cable_spreadsheet] of "Benson approved" cables. The website [bensonapproved.com][benson_approved_website] is a small browsable catalog of "Benson approved" products with links to Amazon.

In one notable instance in February of 2016, Benson [tested a USB C adapter][usb_c_fried_hardware] which he claims damaged his laptop.

[amazon_profile]: https://www.amazon.com/gp/pdp/profile/A25GROL6KJV3QG
[benson_approved_product]: https://www.amazon.com/Charger-Tronsmart-Charge-Technology-Attached-Approved/dp/B0146FK3G0
[usb_c_fried_hardware]: http://www.androidpolice.com/2016/02/02/google-engineer-benson-leung-finds-a-usb-type-c-cable-that-isnt-just-dangerous-on-paper-it-allegedly-fried-his-hardware/
[benson_approved_adapter_list]: http://www.droid-life.com/2016/01/25/best-usb-type-c-adapters-benson-leung/
[benson_approved_cable_spreadsheet]: https://www.reddit.com/r/Nexus6P/comments/3robzo/google_spreadsheet_for_usbc_cables_with_benson/
[benson_approved_website]: http://bensonapproved.com/