<!-- title: Snell Memorial Foundation - Helmet Testing -->

<!-- redirect: /w/c/Regulation/ProductSafety/SnellStandardVsUsDepartmentOfTransportation -->

[Snell Memorial Foundation][main_website] is a private organization that tests various riding helmets, from bicicle to motorcycle to automobile. It is a standard that works in parallel with the mandatory US Department of Transportation standards.

# Funding 

Unlike [Consumer Reports](/regulation/product-safety/consumer-reports), which takes a hard line stance on funding from any company whose products it tests, Snell [recieves 90% its funding][main_website_qa] from the manufacturers of the helmets it tests, and the rest from donations. Snell addresses this in its [response][nyt_response] to a New York Times article criticizing them on this and other things.

# Potential Government Involvement

Though Snell receives no government funding, it does get funding by selling approval stickers, which likely utilizes government protection of intellectual property to prevent forgery.

# Quality

There has been [some controversy][harry_hurt_speaks_out] about the quality of Snell's helmet testing standards, largely from noted motorcycle safety researcher [Harry Hurt][harry_hurt_wikipedia]. Snell has [responded to him][harry_hurt_response] as well as [to a critical New York Times article][nyt_response]. In particular, Hurt's opinion is that the US Department of Transportation's standards are time tested and sufficiently useful. He believes that, though Snell has a drop test for higher speeds, they do not reflect realistic crash situations.

However, as evidence that Snell's standards are none the less respected, Snell product approval is still [cited in Motorycycle Magazine's 2010 buyer's guide][motorcyclist_magazine_2010_buyers_guide]. There are also many certified helmets [listed on Snell's website][main_website_helmets], indicating that many helmet manufacturers see fit to pay for the certification.

# Sources

# References:

* [Harry Hurt][harry_hurt_wikipedia], motorcycle safety researcher, [gives criticism][harry_hurt_speaks_out] of Snell in Motorcycle Consumer News
* Snell's [response][harry_hurt_response] to Harry Hurt's criticism.
* Snell's [response][nyt_response] to a New York Times article criticizing them.
* [2010 helmet buyer's guide][motorcyclist_magazine_2010_buyers_guide] from Motorcyclist Magazine's website, which references Snell
* [Snell Memorial Foundation website][main_website]
* [Certified helmets][main_website_helmets] from Snell's website

# Other Resources:

* [Wikipedia article][wikipedia]
* [Saftey standards][main_website_standards] published on Snell's website
* [About page][main_website_about] from Snell's website
* [Q/A][main_website_qa] from Snell Memorial Foundation website
* Snell gives a [comparison][snell_compares_self_with_dot] between themselves and the US Department of Transportation

[wikipedia]: http://en.wikipedia.org/wiki/Snell_Memorial_Foundation
[main_website]: http://www.smf.org
[main_website_standards]: http://www.smf.org/stds.html
[main_website_helmets]: http://www.smf.org/cert
[main_website_about]: http://www.smf.org/about
[main_website_qa]: http://www.smf.org/docs/media/mpn
[harry_hurt_speaks_out]: http://www.mcnews.com/mcn/features/200502Hurt.pdf
[harry_hurt_response]: http://www.smf.org/docs/articles/pdf/btlo_tech_response_2.pdf
[nyt_response]: http://www.smf.org/docs/articles/responsenyt
[motorcyclist_magazine_2010_buyers_guide]: http://www.motorcyclistonline.com/gear/2010-helmet-buyers-guide
[snell_compares_self_with_dot]: http://www.smf.org/docs/articles/dot
[harry_hurt_wikipedia]: http://en.wikipedia.org/wiki/Harry_Hurt