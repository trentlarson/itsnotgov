<!-- title: Consumer Lab (Product Safety) -->

<!-- redirect: /w/c/Regulation/ProductSafety/consumer_lab -->

*This article is a stub. You can help out by finding and filling out useful details.  See our [writing guidelines](/help/story-writing-guidelines).*

Consumer Lab is an organization that tests the quality of health and nutrition products. It is [funded by][funding] membership fees for organizations which take its newsletter. Some of these are public organizations, such as public libraries and universities.

In 2016, Consumer Lab was apparently [hired by][bloomberg_aloe_vera] Bloomberg News to investigate aloe vera content in supposed aloe vera products, and discovered that they contained none.

# See Also:

[Consumer Lab (Food Safety)](/w/c/Regulation/FoodSafety/private_fda/)

[funding]: https://www.consumerlab.com/answers/Can+I+trust+ConsumerLab.com+How+are+its+tests+paid+for/trust_consumerlab/
[bloomberg_aloe_vera]: http://www.bloomberg.com/news/articles/2016-11-22/no-evidence-of-aloe-vera-found-in-the-aloe-vera-at-wal-mart-cvs