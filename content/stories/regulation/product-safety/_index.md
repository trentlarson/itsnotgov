<!-- title: Product Safety -->

<!-- redirect: /w/c/Regulation/ProductSafety -->

Product manufacturers want to keep a good reputation, so they have an incentive to keep their products safe. From a consumer's point of view, there is a difficulty with information. Consumers have to do a lot of research if they want to be sure about every product they might buy.

However, there are a lot fewer stores than products, and stores similarly want to maintain their reputation by keeping their products safe. Some stores, such as Target, have safety guidelines for many of their products.

Finally, outside organizations, such as insurance companies, or consumer advocacy groups, offer external opinions. A bad review of a product can put public pressure on stores and producers to improve their standards, if they happen to be lax.