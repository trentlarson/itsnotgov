<!-- title: Product Safety at Target Corporation -->

<!-- redirect: /w/c/Regulation/ProductSafety/TargetProductSafety -->

*This article is a stub*

Target Corporation has [safety standards][Target Safety Overview] for their products that go beyond the government's standards.

# Sources

## References

* [Target Safety Overview][]

## Other Resources

* San Jose Mercury News: [Target increases safety standards of cosmetics](http://www.mercurynews.com/business/ci_24324649/target-unveils-sweeping-changes-product-safety-standards)
* Cosmetic safety advocacy group [praises Target's safety standards](http://safecosmetics.org/article.php?id=1159)

[Target Safety Overview]: https://corporate.target.com/corporate-responsibility/responsible-sourcing/product-safety-quality-assurance/QA-tools-and-processes