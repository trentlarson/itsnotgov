<!-- title:American Dental Association Seal of Acceptance -->
<!-- summary:A voluntary certification of dental products verified with laboratory tests. Since 1931. -->

![Teeth](/images/stories/regulation/product-safety/ada-seal-of-acceptance/teeth.jpg)

The [American Dental Association][site_home] (ADA) is a professional organization formed in 1859. One of the ADA's activities is providing a [Seal of Acceptance][consumer_reports_toothbrush] to dental products that it deems safe and effective per the products claims. The Seal of Acceptance is [strictly voluntary][consumer_reports_toothbrush], and not all dental product have the seal. The first Seal of Acceptance was [given in 1931][site_seal_faq].

# Funding

The cost of product evaluation process is [paid for][site_seal_overview] in part by the companies submitting their products for review. It is not clear where the rest comes from, as financial information about the ADA is difficult to find. However the ADA is also [funded by][site_membership] membership dues which is a likely answer to the question.

# Results

## Credibility

It is difficult to find an objective third party assessment of the effectiveness of the ADA's seal program, as most articles about it are from the dental industry which is of questionable impartiality given the ADA's pervasiveness in the industry. However the American Dental Association appears to be generally respected among government organizations (which is relevant in the context of establishing its credibility as a comparable alternative to government). For instance, it is [referenced by the FDA][fda_data] as a source of data in policy making:

> The Subcommittee wishes to thank the American Dental Association’s (ADA) Council on Scientific Affairs for its assistance in providing data, information, and testimony during the course of the Subcommittee’s deliberations.

The Seal of Acceptance is acknowledged by the New York State Department of Health in a [health practice guideline][ny_gov] document:

> The ADA Seal of Acceptance long has been a valuable and respected guide to consumer and professional products.

Finally, in an [otherwise critical article][consumerist], Consumerist concedes that the ADA Seal of Acceptance program enjoys wide credibility:

> Despite the controversy (if this even _counts_ as controversy in today’s market of privately-funded research), it’s widely accepted that the ADA’s tests are tough and comprehensive...

## Standards

With this credibility in mind, the ADA [claims to perform][site_toothpastes] or require laboratory testing for toothpaste ingredients, and also claim that the FDA does not perform laboratory tests for its requirements for toothpaste. The ADA also claims to perform or require laboratory tests for specific claims that product manufacturers make. For instance, they require evidence that a given toothpaste [contains fluoride][site_toothpastes] or [fighting erosion][site_erosion].

### Controversies

One [controversy][bbc] is that the US Department for Health and Human Services and Agriculture claimed that there was insufficient proof of the importance of flossing, despite the ADA's recommendations. However the ADA [released a statement][site_flossing] defending their position.

[Another controversy][nih_mercury] is the ADA's claims that mercury amalgam fillings are safe. However, the FDA [also claims][fda_mercury] that they are safe.

# Incentives

The organization has a [monopoly on accreditation][site_coda] of dental education programs, which is a benefit to the existing dental professionals who join. They also seem to have a reputation for [lobbying to limit][wapo_lobby] competition in other ways. As such, dental professionals may be paying for ADA membership out of self-interest. This does not necessarily lead to an incentive to create the Seal of Acceptance program, however fees from members who join out of self-interest do contribute to any overhead costs that may be shared between these two programs.

There is also a question of impartiality, given that companies [pay to offset][abc] their portion of the test, and may have other ongoing relationships with the ADA. Also, according to an ABC News story, the details of the tests [are not open][abc] to the public. However, per the same story:

> Still, to be considered for the ADA's seal of acceptance, each gum brand submitted by Wrigley underwent a battery of tough laboratory and clinical tests to show that it was, indeed, safe and effective at preventing cavities.

# Government Involvement

The ADA as an organization has a fairly involved relationship with the government. As mentioned above, the ADA has a [monopoly on accreditation][site_coda] of dental education programs and (per some opinions) [engages in lobbying][wapo_lobby] to limit competition. It [also lobbies][site_lobby] for government policy changes related to dentistry and [makes recommendations][fda_recommendation] to the FDA. And of course, as with most certification organizations, it presumably relies on the state to enforce a trademark to prevent its Seal of Acceptance from fraudulently being placed on products.

# Sources

## References

* American Dental Association
[site_home]: https://www.ada.org (ADA - Home Page)
    * [Home Page][site_home]
[site_seal_overview]: https://www.ada.org/en/science-research/ada-seal-of-acceptance/how-to-earn-the-ada-seal/guidelines-for-participation-in-the-ada-seal-progr (ADA - Seal Program Overview)
    * [Seal Program Overview][site_seal_overview]
[site_membership]: https://www.ada.org/en/member-center/join-or-renew-ada-membership/ada-membership-faq (ADA - Frequently Asked Questions about ADA Membership)
    * [Frequently Asked Questions about ADA Membership][site_membership]
[site_toothpastes]: https://www.ada.org/en/member-center/oral-health-topics/toothpastes (ADA - Toothpastes)
    * [Toothpastes][site_toothpastes]
[site_erosion]: https://www.ada.org/en/publications/ada-news/2017-archive/november/toothpaste-first-to-carry-7-attributes-of-ada-seal (ADA - Toothpaste first to receive ADA Seal for fighting erosion)
    * [Toothpaste first to receive ADA Seal for fighting erosion][site_erosion]
[site_coda]: https://www.ada.org/en/coda/accreditation/about-us (ADA - Commission on Dental Accreditation)
    * [Commission on Dental Accreditation][site_coda]
[site_lobby]: https://www.ada.org/en/advocacy/advocacy-issues (ADA - Advocacy Issues)
    * [Advocacy Issues][site_lobby]
[site_flossing]: https://www.ada.org/en/press-room/news-releases/2016-archive/august/statement-from-the-american-dental-association-about-interdental-cleaners (ADA - Federal Government, ADA Emphasize Importance of Flossing and Interdental Cleaners )
    * [Federal Government, ADA Emphasize Importance of Flossing and Interdental Cleaners ][site_flossing]
[site_seal_faq]: https://www.ada.org/en/science-research/ada-seal-of-acceptance/ada-seal-faq (ADA - Seal of Acceptance FAQ)
    * [Seal of Acceptance FAQ][site_seal_faq]

* Consumer Reports
[consumer_reports_toothbrush]: https://www.consumerreports.org/cro/news/2014/05/hot-to-choose-your-next-toothbrush/index.htm (Consumer Reports - How to choose your next toothbrush)
    * [How to choose your next toothbrush][consumer_reports_toothbrush]

* Consumerist
[consumerist]: https://consumerist.com/2007/09/28/gum-company-paid-ada-for-study-that-earned-it-their-seal-of-approval/ (Consumerist - Gum Company Paid ADA For Study That Earned It Their Seal Of Approval)
    * [Gum Company Paid ADA For Study That Earned It Their Seal Of Approval][consumerist]

* United States Food and Drug Administration
[fda_data]: https://www.fda.gov/downloads/drugs/developmentapprovalprocess/developmentresources/over-the-counterotcdrugs/statusofotcrulemakings/ucm096081.pdf (FDA - Rule Proposal Document)
    * [Rule Proposal Document][fda_data]
[fda_recommendation]: https://www.fda.gov/downloads/Radiation-EmittingProducts/RadiationEmittingProductsandProcedures/MedicalImaging/MedicalX-Rays/ucm116505.pdf (FDA - Guidelines for the Selection of Patients for Dental Rediographic Examinations)
    * [Guidelines for the Selection of Patients for Dental Rediographic Examinations][fda_recommendation]
[fda_mercury]: https://www.fda.gov/medicaldevices/productsandmedicalprocedures/dentalproducts/dentalamalgam/ucm171094.htm (FDA - About Dental Amalgam Fillings)
    * [About Dental Amalgam Fillings][fda_mercury]

* New York State Department of Health
[ny_gov]: https://www.health.ny.gov/publications/0824/pda/windows_mobile/0824_child_health_professionals.pdf (New York State Department of Health - Oral Health Care during Pregnancy and Early Childhood Practice Guidelines)
    * [Oral Health Care during Pregnancy and Early Childhood Practice Guidelines][ny_gov]

* Washington Post
[wapo_lobby]: https://www.washingtonpost.com/politics/the-unexpected-political-power-of-dentists/2017/07/01/ee946d56-54f3-11e7-a204-ad706461fa4f_story.html?utm_term=.5776e8188b31 (Washington Post - The unexpected political power of dentists)
    * [The unexpected political power of dentists][wapo_lobby]

* ABC News
[abc]: https://abcnews.go.com/Health/story?id=3650113&page=1 (ABC - Dentists, Consumer Groups Debate ADA Seal on Gum)
    * [Dentists, Consumer Groups Debate ADA Seal on Gum][abc]

* BBC News
[bbc]: https://www.bbc.com/news/health-36962667 (BBC - Should you floss or not? Study says benefits unproven)
    * [Should you floss or not? Study says benefits unproven][bbc]

[nih_mercury]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3388771/ (The Dental Amalgam Toxicity Fear: A Myth or Actuality)
* Academic article - [The Dental Amalgam Toxicity Fear: A Myth or Actuality][nih_mercury]

## See Also

* Wikipedia
[wikipedia]: https://en.wikipedia.org/wiki/American_Dental_Association (Wikipedia - American Dental Association)
    * [American Dental Association][wikipedia]
* Consumer Reports
[consumer_reports]: https://www.consumerreports.org/dental-oral-care/why-is-triclosan-in-toothpaste/ (Consumer Reports - Why Is Triclosan in Toothpaste?)
    * [Why Is Triclosan in Toothpaste?][consumer_reports]

## Futher Reading

* Reason
[reason_clout]: https://reason.com/blog/2017/07/07/pulling-teeth-in-the-state-house-how-den (Reason - How Dentists Use Their Political Clout to Limit Competition)
    * [How Dentists Use Their Political Clout to Limit Competition][reason_clout]
