<!-- title:Good Housekeeping Research Institute -->
<!-- summary:Product testing laboratories affiliated with the Good Housekeeping publication. -->

![Test Tube](/images/stories/regulation/product-safety/good-housekeeping-research-institute/test-tube.jpg)

Good Housekeeping Research Institute is a [group of product testing laboratories][nyt_blog] associated with the Good Housekeeping magazine. It [began in 1900][wapo] as the Experiment Kitchen, and was renamed to its current name in 1910. It [has uncovered][nyt_blog] numerous useful findings such as formaldehyde in certain hair products, safety hazards in certain baby strollers, and products simply not living up to advertised claims. Additionally, the laboratories are used as needed to [qualify select products][wapo] for the Good Housekeeping Seal. Good Housekeeping [promises to][site_warranty] refund (up to $2,000), repair, or replace any defective product with the Good Housekeeping Seal within 2 years of purchase.

## Results

The magazine [had a circulation][wapo] of 4.6 million as of 2008 (larger than Martha Stewart Living), informing many consumers who have specifically sought out the information.

A stronger argument for their impact would be producers who corrected their product as a result of a finding, which would benefit many more consumers. One possible example is when the Good Housekeeping Research Institute [discovered that][sfgate] fat content in the snack known as "Pirate's Booty" was mislabeled, which lead to a product recall. However, it's hard to say whether the producer's incentive was the threat of legal or regulatory action or merely public and business relations. Indeed, in this case a class action lawsuit [was filed][sfgate].

## Government Involvement

The award of the Good Housekeeping Seal likely relies on some sort of legal intellectual property protection to prevent companies from using it illegitimately.

In 1939, the FTC [filed a complaint][site_seal_history] regarding the specific wording on their seal, as well as a charge of publishing advertisements with grossly exaggerated and false claims. Assuming the FTC was correct, the government served as a correcting force on the magazine and the validity of the seal.

One of the major early figures in the Good Housekeeping Research Institute, Harvey W. Wiley, had [previously worked][fda_wiley] for regulatory agencies in government and was known as the "Father of the Pure Food and Drugs Act". [He became][fda_commissioners] the first commissioner of the Food and Drug Administration. Good Housekeeping [has advocated][allgov] for other regulations in modern times.

Good Housekeeping [claims to have][site_institute_history], through their publication, contributed to the eventual passing the Pure Food and Drug Act in U.S. Congress.

Finally, again for any case where a published lab result lead to a company improving their product, it's possible that they would not have done so without the threat of subsequent lawsuit or regulatory action based on the finding.

# Sources

* Good Housekeeping Website
[site_seal_history]: https://www.goodhousekeeping.com/institute/about-the-institute/a16509/good-housekeeping-seal-history/ (Good Housekeeping - The History of the Good Housekeeping Seal)
    * [The History of the Good Housekeeping Seal][site_seal_history]
[site_warranty]: https://www.goodhousekeeping.com/institute/about-the-institute/a22148/about-good-housekeeping-seal/ (Good Housekeeping - About the GH Limited Warranty Seal)
    * [About the GH Limited Warranty Seal][site_warranty]
[site_institute_history]: https://www.goodhousekeeping.com/institute/about-the-institute/g2148/good-housekeeping-research-institute-history/?slide=5 (Good Housekeeping - The History of the Good Housekeeping Institute)
    * [The History of the Good Housekeeping Institute][site_institute_history]
[nyt_blog]: https://well.blogs.nytimes.com/2015/02/02/good-housekeeping-seal-keeps-going/ (NYT Personal Health Blog - Good Housekeeping Seal Keeps Going)
* [Good Housekeeping Seal Keeps Going][nyt_blog] - New York Times Personal Health Blog
[allgov]: http://www.allgov.com/news/top-stories/unregulated-50-billion-cosmetics-industry-at-center-of-congressional-debate-over-unsafe-products-160816?news=859325 (AllGov - Unregulated $50-Billion Cosmetics Industry at Center of Congressional Debate over Unsafe Products)
* [Unregulated $50-Billion Cosmetics Industry at Center of Congressional Debate over Unsafe Products][allgov] - AllGov
[sfgate]: https://www.sfgate.com/news/article/Mom-blows-whistle-on-booty-snack-Rice-puffs-2820584.php (SF Gate - Mom blows whistle on 'booty' snack / Rice puffs contain three times more fat than shown on label)
* [Mom blows whistle on 'booty' snack / Rice puffs contain three times more fat than shown on label][sfgate] - SF Gate
[wapo]: http://www.washingtonpost.com/wp-dyn/content/article/2008/01/01/AR2008010100642.html (Wapo - Surviving the Test of Time)
* [Surviving the Test of Time][wapo] - Washington Post
* Food and Drug Administration Website
[fda_commissioners]: https://www.fda.gov/AboutFDA/History/FOrgsHistory/Leaders/ucm2006081.htm (FDA - Commissioners)
    * [Commissioners][fda_commissioners]
[fda_wiley]: https://www.fda.gov/AboutFDA/History/FOrgsHistory/Leaders/ucm2016811.htm (FDA - Harvey W. Wiley)
    * [Harvey W. Wiley][fda_wiley]

## Other Sources

[cornell_adjuncts]: http://hearth.library.cornell.edu/cgi/t/text/pageviewer-idx?c=hearth;rgn=full%20text;idno=6417403_1321_001;view=image;seq=23 (Good Housekeeping, 1902 - Injurious Food Adjuncts)
* [Injurious Food Adjuncts][cornell_adjuncts] - A notable article in Good Housekeeping from 1902 (on Cornell University's website)
[wikipedia]: https://en.wikipedia.org/wiki/Good_Housekeeping (Wikipedia - Good Housekeeping)
* [Good Housekeeping][wikipedia] - Wikipedia

## Further Reading

[cato]: https://object.cato.org/sites/cato.org/files/pubs/pdf/pa-303.pdf (Cato - Private Regulation: A Real Alternative for Regulatory Reform)
* [Private Regulation: A Real Alternative for Regulatory Reform][cato] - Cato Institute (Mentions Good Housekeeping Institute and other examples of private regulation)
