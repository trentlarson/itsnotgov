<!-- title: Deed Restrictions -->

Deed restrictions are [clauses in a deed][uslegal] to a piece property which stipulate requirements regarding the use of said property. This is [often used][uslegal] to create a sense of uniformity by the initial housing developers. In Houston, Texas, they are to some extent used in lieu of traditional zoning laws. For instance, it is [possible to restrict][houston_overview] certain businesses, such as a [corner store][rice], from being operated in an area.

# Government Involvement

The enforcement of the deed contract of course generally involves the government. In Houston, Texas, the government has a [Deed Restriction Enforcement Team][houston_enforcement] which takes on a more active role. This is likely because Houston [does not have][rice] traditional zoning laws.

# Results

[One critique][rice] of Houston's model based on deed restrictions (and some top-down regulations, short of traditional zoning laws) is that it leads to ineffective planning and unequal enforcement.

# Sources

## References

[houston_overview]: http://www.houstontx.gov/planning/Neighborhood/deed_restr.html
* [Official information][houston_overview] giving an overview of deed restrictions in Houston, Texas

[houston_enforcement]: https://www.houstontx.gov/legal/deed.html
* [Official information][houston_enforcement] about special enforcement of deed restrictions in Houston, Texas

[rice]: https://kinder.rice.edu/2015/09/08/forget-what-youve-heard-houston-really-does-have-zoning-sort-of/
* [Article][rice] arguing the existence of de facto zoning regulation in Houston, Texas from the Rice Kinder Institute for Urban Research

[uslegal]: https://definitions.uslegal.com/d/deed-restrictions/
* [Definition][uslegal] on USLegal

## Other Sources

[ozarkland]: http://www.ozarkland.com/index.php/deed-restrictions
* [Examples of deed restrictions][ozarkland] associated with a real estate company in the Ozarks

## Further Reading

[walter_block]: http://www.walterblock.com/2017/03/20/deed-restrictions/
* [Libertarian economist Walter Block][walter_block] on deed restrictions in the context of libertarian theory

[market_urbanism]: http://marketurbanism.com/2016/09/19/how-houston-regulates-land-use/
* [How Houston Regulates Land Use][market_urbanism] - Market Urbanism
