<!-- title: Land Use -->

<!-- redirect: /w/c/Regulation/land_use -->

Governments often engage in [Land-use Planning](https://en.wikipedia.org/wiki/Land-use_planning), including [Zoning laws](https://en.wikipedia.org/wiki/Zoning), in order to increase efficiency and reduce conflicts. Is there an alternative?