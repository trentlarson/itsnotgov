<!-- title:Examine.com - Scientific guides to nutritional supplements for consumers -->
<!-- summary:A profitable company that sells digests and summaries of scientific studies about the effects of nutritional supplements. No ads, no partnerships, no investors. -->

![Vitamins](/images/stories/regulation/drug-safety/examine/vitamins.jpg)

[Examine.com][site_home] is an informational website about nutritional supplements. While it [does not review][fastcompany] specific products, it provides [references and summaries][forbes_domain_names] of scientific studies for the common consumer [about specific ingredients][fastcompany].

# Results

The company [was started][mensjournal] by Sol Orwell in 2011. According to a 2016 article by Orwell published in The Globe And Mail, it is profitable and has taken [no outside investment][globe_and_mail]. As of 2014 the site receives [20,000 visits][fastcompany] daily. In 2015, [as reported by][fastcompany] Fastcompany, an internal survey conducted by Examine.com showed that the site affected decisions of over $10,000,000 in supplement sales.

It has (as of 2015) [no ads][fastcompany], claims to have [no partnerships][site_five_reasons], and vows to [never make][site_guardian_response] product recommendations, in order to maintain its neutrality. As of 2014, its [only revenue][mensjournal] was from sales of a $49 reference guide. On a page on their site dated as updated Feb 23, 2019, they list [four publications][site_five_reasons] for sale, again asserting that these are their only source of income.


# Sources

## References

* Examine.com
[site_home]: https://examine.com/ (Examine.com)
    * [Home page][site_home]
[site_five_reasons]: https://examine.com/nutrition/five-reasons-examine-fitness-guide/ (Examine.com: Five reasons why you need the Examine.com Fitness Guide)
    * [Five reasons why you need the Examine.com Fitness Guide ][site_five_reasons]
[site_supplement_guides]: https://examine.com/store/supplement-guides/ (Examine.com - Supplement Guides)
    * [Supplement Guides][site_supplement_guides]

[fastcompany]: https://www.fastcompany.com/3041643/the-worlds-top-10-most-innovative-companies-of-2015-in-fitnes (Fastcompany - The World’s Top 10 Most Innovative Companies Of 2015 In Fitness)
* [The World’s Top 10 Most Innovative Companies Of 2015 In Fitness][fastcompany] - Fastcompany

[globe_and_mail]: https://www.theglobeandmail.com/report-on-business/small-business/startups/why-i-dont-want-funding-for-my-company/article27459833/ (The Globe And Mail - Why I don't want funding for my company)
* [Why I don't want funding for my company][globe_and_mail] - Examine.com Founder Sol Orwell writing in The Globe And Mail

[mensjournal]: https://www.mensjournal.com/health-fitness/game-changers-2014-sol-orwell (Men's Journal - Game Changers 2014: Sol Orwell)
* [Game Changers 2014: Sol Orwell][mensjournal] - Men's Journal

[forbes_seven_figures]: https://www.forbes.com/sites/elainepofeldt/2015/07/30/a-young-entrepreneurs-passion-for-hacking-his-diet-sparks-a-seven-figure-business/ (Forbes - A Young Entrepreneur's Passion For Hacking His Diet Sparks A Seven-Figure Business)
[A Young Entrepreneur's Passion For Hacking His Diet Sparks A Seven-Figure Business][forbes_seven_figures] - Forbes

## Other Sources

[forbes_domain_names]: https://www.forbes.com/sites/kimjay/2017/09/10/this-entrepreneur-spends-thousands-on-domain-names-for-his-startups-heres-why-you-should-too/ (Forbes - This Entrepreneur Spends Thousands On Startup Domain Names -- Here's Why You Should Too)
[This Entrepreneur Spends Thousands On Startup Domain Names -- Here's Why You Should Too][forbes_domain_names] - Forbes

[guardian]: https://www.theguardian.com/science/blog/2014/apr/15/dietary-supplements-vitamins-evidence (The Guardian - Dietary supplements: who needs them?)
[An article][guardian] in The Guardian critical of Examine.com

* Examine.com
[site_guardian_response]: https://examine.com/nutrition/scientific-research-and-our-article-in-the-guardian/ (Examine.com - Scientific Research and our Article in the Guardian )
    * [Response to][site_guardian_response] the above Guardian article
[site_annual_letter]: https://examine.com/nutrition/2017-annual-letter/ (Examine.com - 2017 Annual letter to Examine.com readers)
    * [2017 Annual Letter][site_annual_letter] to users, in lieu of investors

[wikipedia]: https://en.wikipedia.org/wiki/Examine.com (Wikipedia - Examine.com)
* [Wikipedia article][wikipedia]
