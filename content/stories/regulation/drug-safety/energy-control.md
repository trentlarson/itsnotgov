<!-- title: Energy Control -->

<!-- redirect: /w/c/Regulation/DrugSafety/energy_control -->

*This article is in-progress. It requires determining its funding, and finding any available unbiased measures of success of the program.*

[Energy Control][website] is an organization, started in Barcelona, Spain in 1997, which tests the composition of recreational drugs in order to mitigate harm to users.

# Funding

Their [standard fee][website] for drug testing is €50.

However, Energy Control appears to [receive funding][2009_activity_report] from various government organizations. It's not yet determined how much of its funding come from those sources.

# "Recognition" from various governments.

The organization boasts its recognition from various government organizations. It's unclear what sort of support this implies.

# Testing capabilities

The [testing techniques][website] used by Energy Control are [Gas chromatography-mass-spectrometry](https://en.wikipedia.org/wiki/Gas_chromatography-mass_spectrometry) and [Liquid chromatography-mass-spectrometry](https://en.wikipedia.org/wiki/Liquid_chromatography%E2%80%93mass_spectrometry).

## Drugs Tested

According to [their website][website], the offer identification of the following substances:

> * Cocaine and its usual adulterants: levamisole, phenacetine, acetaminophen, caffeine, procaine, tetracaine, lidocaine and benzocaine
> * Heroin, codeine and morphine
> * Amphetamine, methamphetamine and 4-metylamphetamine
> * MDMA, MDA, MDEA and adulterants: dextromethorphan, m-CPP, metoclorpramide, PMA and PMMA.
> * LSD
> * DMT, NMT, harmine, harmaline, THH and gramine.
> * 2C-B
> * Methylone, butylone and mephedrone.
> * Cannabinoids (only THC and CBD).
> * GHB and GBL

# Sources

## References

* [Website][website]
* [Activity Report from 2009][2009_activity_report] which includes some information about funding.

## Further Reading

* [Medium post][backchannel] by Backchannel

[website]: http://energycontrol.org/international.html
[2009_activity_report]: http://energycontrol.org/files/pdfs/MEMORIA+EC+2009+ING.pdf
[backchannel]: https://medium.com/backchannel/inside-the-deep-web-drug-lab-9718cd0fe504
