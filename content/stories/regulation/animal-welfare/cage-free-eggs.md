<!-- title: Cage-Free Eggs -->
<!-- summary: Conscious consumers and public pressure from the Humane Society are leading companies to promise to switch their eggs to cage-free. Some have switched already. -->

![Egg](/images/stories/regulation/animal-welfare/cage-free-eggs/egg.jpg)

Cage-free eggs are eggs produced from hens which are [kept under conditions][humane_society_eggs] that allow them a specified amount of freedom of movement. While it [does not address][humane_society_eggs] all welfare concerns for chickens, it is still a significant improvement over the "battery cage" method. As of 2019, cage-free eggs comprised [10% of all eggs][cnbc] sold according to CNBC, compared to [5% in 2015][cnn] according to the Humane Society of the United States. Over time, because of [consumer preferences][bloomberg_ticking], pressure from the [Humane Society][bloomberg_ticking], as well as changes in a few [state laws][cnbc] (including California), companies have decided to start voluntarily committing to updating their supply chain such that 100% of eggs are cage free. [According to][fortune] a representative from the Humane Society, McDonald’s announcement of their commitment to switch to cage free eggs influenced many other companies to follow suit.

Over all, [hundreds of companies][bloomberg_ticking] are pledging to be cage-free by 2025, though it [will be difficult][cnbc]. Some companies are [already lagging][bloomberg_probably_not] in their progress, but at least one notable company has transitioned [ahead of schedule][unilever_hellmanns]. Many (as of 2019) remaining companies [face pressure][bloomberg_ticking] to meet their promised deadlines, which is in turn [putting pressure][cnbc] on egg farms to update their facilities.

# Results

Some notable companies and brands which have made significant progress (per their own claims) in switching to cage-free include:

* [Whole Foods][whole_foods] - Cage free since 2004
* [Hellmann's mayonnaise and dressing][unilever_hellmanns] - Cage free in the United States since 2017, 3 years ahead of their originally stated schedule
* [Taco Bell][taco_bell] - Breakfast menu cage-free as of 2017. Planned to become 100% cage free by January 1, 2018, unclear whether it happened.
* [Hyatt][hyatt] - As of 90% in the US. Planning for 2022 for US Canada and Europe, 2025 worldwide

Some [notable brands][fortune] that are planning to make the switch by 2025 or sooner are McDonald's, [Walmart][cnbc], Burger King, Subway, Sodexho and Compass Group. But again, it [will be][bloomberg_probably_not] a challenge.

## Funding

[According to Charity Navigator][charity_navigator_humane_society], the Animal Humane Society of the United States receives no government funding.

# Government involvement

The standard for cage-free eggs, and the enforcement of claims of being cage-free, is done by the [United States Department of Agriculture][cnn]. Cage-free is not generally legally mandated in the United States, however [several states][huffpo] have mandated it, though some other states have challenged them legally as it affects interstate commerce. There was also a [failed attempt][cnbc] by the Humane Society to enact a federal law. If more states require it as time goes on, it may become more difficult to determine how much of the push is market driven.

# Sources

## Reference

[cnn]: https://money.cnn.com/2015/12/24/news/companies/cage-free-eggs-companies-2015/index.html
* CNN - [Cage-free eggs cracked these corporate shells][cnn]
[taco_bell]: https://www.tacobell.com/news/2017-new-years-commitments?selectedTag=&selectYear=2017
* Taco Bell - [Taco Bell's 2017 New Year's Commitments][taco_bell]
* Bloomberg
[bloomberg_probably_not]: https://www.bloomberg.com/news/articles/2018-09-18/your-eggs-probably-aren-t-cage-free
    * [Your Eggs Probably Aren’t Cage-Free][bloomberg_probably_not]
[bloomberg_ticking]: https://www.bloomberg.com/news/articles/2019-03-08/clock-is-ticking-as-companies-scramble-to-produce-cage-free-eggs
    * [Companies Are Rushing to Meet Cage-Free Egg Deadline][bloomberg_ticking]
[cnbc]: https://www.cnbc.com/2017/03/22/egg-makers-are-freaked-out-by-the-cage-free-future.html
* CNBC - [Egg makers are freaked out by the cage-free future][cnbc]
[fortune]: http://fortune.com/2016/01/20/target-cage-free-eggs/
* Fortune - [Target Joins Growing List of Food Companies Committing To Cage-Free Eggs][fortune]
[huffpo]: https://www.huffingtonpost.com/entry/cage-free-egg-laws-spur-cage-match-between-states_us_5addea18e4b0cf2c46f28c2b
* Huffington Post - [Cage-Free-Egg Laws Spur Cage Match Between States][huffpo]
[charity_navigator_humane_society]: https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=3848
* Charity Navigator - [The Humane Society of the United States][charity_navigator_humane_society]
[humane_society_eggs]: https://www.humanesociety.org/resources/cage-free-vs-battery-cage-eggs
* The Humane Society of the United States - [Cage-free vs. battery-cage eggs][charity_navigator_humane_society]
[hyatt]: https://about.hyatt.com/en/hyatt-thrive/responsible-sourcing.html
* Hyatt - [Responsible Sourcing][hyatt]
[whole_foods]: https://media.wholefoodsmarket.com/news/whole-foods-market-receives-award-for-commitment-to-cage-free-supply-chain
* Whole Foods - [Whole Foods Market receives award for commitment to cage-free supply chain][whole_foods]
[unilever_hellmanns]: https://www.unileverusa.com/news/press-releases/2017/hellmanns-mayonnaise-and-dressings-now-use-100-percent-cage-free-eggs.html
* Unilever - [Hellmann’s mayonnaise and mayonnaise dressings now use 100% cage-free eggs in the U.S.\*, three years ahead of schedule][unilever_hellmanns]
