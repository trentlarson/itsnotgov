<!-- title: Standardization -->

The government is in the business of developing standards used in industry. For instance [National Institute of Standards and Technology][nist] regulates weights and measures in the United States. Even for some nominally voluntary standards associations such as ANSI, the United States government often [codifies their standards][ansi] into law. This makes it difficult to determine how well standards would work out by sheer market incentives.

Are there examples of organizations coming together to form standards voluntarily adopted by the marketplace?

[nist]: https://www.nist.gov/pml/weights-and-measures
[ansi]: https://www.ansi.org/news_publications/other_documents/safeguarding
