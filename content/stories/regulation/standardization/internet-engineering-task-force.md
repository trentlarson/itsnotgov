<!-- title: Internet Engineering Task Force -->
<!-- summary: Creating and updating standard protocols that underlie the Internet -->

![Internet Engineering Task Force - Logo](/images/stories/regulation/standardization/internet-engineering-task-force/logo.png)

The [Internet Engineering Task Force][ietf_home] is the [standards organization][oreilly] that develops and updates underlying Internet protocols, such as TCP/IP. [Its membership comprises][ripe] researchers and industry professionals, and [is limited][oreilly] to individuals (as opposed to organizations). All of its documents, mailing lists, and meetings are [openly available][oreilly]. A proposal within the Internet Engineering Task Force [becomes an eligible Internet Standard][oreilly] after a deliberation process and demonstrated success in the marketplace. Though it was funded by the government until 1998, it [is now supported][oreilly] by The Internet Society, which appears to be mostly industry money (see below).

# Government Involvement

Until 1998, the Internet Engineering Task Force was [funded by][oreilly] various agencies of the United States government.

In dealing with IP addresses, it [must coordinate with][ripe] Internet Assigned Numbers Authority, which may include some government oversight.

Internet Standards are [not enforced][ietf_meeting] by government mandates, though some government regulations refer to IETF standards.

# Funding

Since 1998, [in addition][oreilly] to meeting fees, the Internet Engineering Task Force has been funded by the Internet Society, which is a non-profit organization. The Internet Society [has received funding][techcrunch] from Google.org. It is not totally clear whether they receive any government funding, but [it appears][oreilly] that they were created in part to secure funding for the Internet Engineering Task Force after its government funding ended. The [2015 financial statement][internet_society_financials_2015] of the Internet Society doesn't seem to mention any government funding. Its [membership appears to consist][internet_society_members] almost entirely of companies, and each level of [membership requires][internet_society_membership_levels] a monetary contribution.

# Sources

## References

[oreilly]: https://www.oreilly.com/openbook/opensources/book/ietf.html (O'Reilly - The Internet Engineering Task Force)
* ["The Internet Engineering Task Force"][oreilly] - chapter from "Open Sources: Voices from the Open Source Revolution" - O'Reilly
[ripe]: https://www.ripe.net/participate/internet-governance/internet-technical-community/ietf (Ripe NCC - Internet Engineering Task Force (IETF))
* [Internet Engineering Task Force (IETF)][ripe] - Ripe Network Coordination Centre

* Internet Society
[internet_society_members]: https://www.internetsociety.org/about-internet-society/organization-members/ (Internet Society - Members)
    * [Members][internet_society_members]
[internet_society_membership_levels]: https://www.internetsociety.org/about-internet-society/organization-members/membership-levels/ (Internet Society - Membership Levels)
    * [Membership Levels][internet_society_membership_levels]
[internet_society_history]: https://www.internetsociety.org/internet/history-of-the-internet/ietf-internet-society/ (Internet Society - History)
    * [History][internet_society_history]
[internet_society_financials_2015]: https://www.internetsociety.org/wp-content/uploads/2017/07/isoc_financials_web.pdf (Internet Society - 2015 Financials)
    * [2015 Financial Statement][internet_society_financials_2015]

[techcrunch]: https://techcrunch.com/2014/02/25/google-org-provides-4-4m-in-grants-to-the-internet-society-and-nsrc-to-improve-internet-access-in-sub-saharan-africa/ (TechCrunch - Google.org Provides $4.4M In Grants To The Internet Society And NSRC To Improve Internet Access In Sub-Saharan Africa)
* [TechCrunch Article][techcrunch] about Google.org donating to the Internet Society

* IETF Website
[ietf_home]: https://www.ietf.org/ (Internet Engineering Task Force - Home)
    * [Home page][ietf_home]
[ietf_meeting]: https://www6.ietf.org/meeting/79/documents/79newcomers.pdf (Internet Engineering Task Force - Meeting Notes)
    * [Meeting notes][ietf_meeting]

## Other Sources

[wikipedia]: https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force (Wikipedia - Internet Engineering Task Force)
* [Wikipedia article][wikipedia]

