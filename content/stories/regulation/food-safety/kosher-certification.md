<!-- title: Kosher Certification -->

<!-- redirect: /w/c/Regulation/FoodSafety/kosher_certification -->

Kosher certification, at least in the United States, is performed by a [large collection][kosherquest_list] of competing private organizations. While they all [compete for quality][food_safety_news], there is a coalition of the five largest certifiers in the US which [set standards][food_safety_news] which the other organizations tend to follow.

Before the 1950s, [there was a lack of][food_safety_news] reliability in the kosher certification industry, along with the broader private food certification industry. However since then the kosher industry has developed a [very high reputation][food_safety_news].

# Incentives

## Voluntary

The incentive to produce the service is fairly simple: it [is a business][food_safety_news], and food producers know that customers are interested in the quality of the food they are purchasing, at least from a kosher perspective. The interesting question is how it maintains quality and reputation. Arguably, competition in food certification services has [lead companies to cut corners][food_safety_news], including kosher certification companies prior to the 1950s. Somehow, since then, the kosher industry has overcome this, and possibly [leveraged competition][food_safety_news] to improve quality instead.

One factor is that finished product certifiers [provide a check][food_safety_news] on ingredient certifiers.

Another factor could be the [religious commitments][food_safety_news] of the rabbis who conduct the certification. (Even though most _customers_ of kosher food, according to the Vice President of Communications and Marketing at the Orthodox Union, are interested in it for health and safety reasons rather than religious commitments)

Finally, there are also [vigilant consumers][food_safety_news] who seek out and report obviously mislabeled food, as well as counterfeit certification logos.

## Government

It should still be noted that the government plays an implicit role in adding incentives on top of market and social forces. There is the threat of lawsuit, which could be leveraged to punish fraud committed by certifiers. This also includes trademark enforcement, which provides punishment for counterfeit certification logos.

# Results

## Food Quality

Kosher certification is not currently a complete replacement for government regulation. For instance, they are not known for expertise in containing [bacterial contamination][food_safety_news]. However for certain areas, such as bug contamination, they [excel][food_safety_news].

## Standardization

There is a coalition of the [largest five][food_safety_news] companies in the industry, which sets standards. With no formal power to enforce, these standards are credible enough to [put pressure on][food_safety_news] smaller agencies to conform.

# Sources

## References

* [Kosher Certification: A Model for Improving Private Food Safety Audits][food_safety_news] (an article on [Food Safety News][food_safety_news_wikipedia])
* [A list of kosher certifications][kosherquest_list] from a Kosher informational website.
    * May be a biased source, so it's only used to show the existence of the many certification organizations.

## Other Resources

* [Wikipedia page][wikipedia] article about kosher certification agencies

[food_safety_news]: http://www.foodsafetynews.com/2013/03/kosher-certification-a-model-for-improving-private-food-safety-audits/
[kosherquest_list]: http://kosherquest.org/kosher-symbols/
[wikipedia]: https://en.wikipedia.org/wiki/Kosher_certification_agency
[food_safety_news_wikipedia]: https://en.wikipedia.org/wiki/Food_Safety_News