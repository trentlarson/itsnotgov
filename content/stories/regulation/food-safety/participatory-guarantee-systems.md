<!-- title: Participatory Guarantee Systems -->

<!-- left-friendly: yes -->

A [Participatory Guarantee System][ifoam_pgs] (PGS) is a quality assurance scheme used by small farmers. It's largely used to assure compliance with organic farming practices. It [involves an organization][ifoam_studies] of farmers who conduct peer review to confirm that they meet the organization's standards, while [also exchanging][ifoam_youtube] knowledge about natural farming practices. They are regional in nature, and as such [vary in the specifics][ifoam_overview] of their operation. As of 2007, [there are dozens][ifoam_overview] of PGSes worldwide, including the [United States][cng], [India][pgs_india], [Brazil][arc2020_brazil], and [Mexico][researchgate_re-imagining].

To a small farmer, the [benefits of a PGS][ifoam_studies] include a reduction in money and time spent compared to government or even private third party certification, which are often seen as very bureaucratic. It [is based on][ifoam_studies] building trust between farmers and consumers, which arguably could only operate on a local scale in the first place. In some cases, [consumers are even involved][ifoam_overview] in the certification process. Though it has limitations, it presents an interesting model for a process of voluntary regulation on a smaller scale within communities. As seen below, it may greatly benefit from the process itself being certified in some way by some outside trusted institution.

# Results

Participatory Guarantee Systems appear to demonstrate a means by which a small community market can voluntarily self-regulate. But how well does it work? It may be difficult to find objective measures, but there are some indicators we can look at.

## Government Recognition

Putting aside for the moment questions of government involvement (see below), PGSes seem to at least be [acknowledged as effective][iaf_paraguay] in Latin America by the [Inter-American Foundation][wikipedia_iaf], an independent agency of the United States government. PGSes have also been formalized by governments of [India][pgs_india] and [Brazil][arc2020_brazil] (more on this below), which arguably required at least some recognition that they were effective beforehand.

## Subjective Opinions

We have some subjective opinions from a [research study][researchgate_re-imagining] on PGSes that included interviews of participants in a market in Oaxaca, Mexico. Overall, it appears that it there is some trust among farmers and consumers, but a fair amount of skepticism. There is also a potential for more trust if the system were stricter and more professional than what they were experiencing at the time. Additionally, many producers agreed that an official government seal would be necessary for PGSes to be trustworthy.

### Quotes

Some producers interviewed expressed no trust in the system, though they were in the minority. For example:

> The way [participatory certification] works now, it is too subjective, so I don't think it's valid, even if you're just talking about selling in [one local] market.

Most producers interviewed had trust the system in theory, but were still skeptical about its current implementation. For example:

> "[t]he way the concept has been explained to me, I have all the confidence in the world in participatory certification, but the way it is actually working in practice right now, well, we're just starting, and it isn't that it doesn't work, but let's just say that because we're just starting I don't have 100 % trust yet"

One consumer said:

> [t]he idea [of PGS] is wonderful, but it's still a bit open, and I think in order to maintain a certain level of recognition as something that is trustworthy, so that the [Network] markets don't get burned, it has to be done in a strict way.

(where "Network" refers to the Mexican Network of Local Organic Markets)

One market coordinator said:

> At first I was against [PGS] because it had no official authorization and didn't offer the possibility to sell outside of my local market, but those concerns have been addressed now, and also, when I started to read more about the Brazilian experience, the idea became more interesting to me ... it will be necessary to have an office, a seal, and all of those things organized. It [PGS] will have to be treated like a business model, not like something romantic.

One other producer said:

> I do trust the participatory certification process ... provided that people from the university, who are trained in how to make a proper determination [regarding certification status] are involved.

# Government Involvement

As with most non-government certification schemes, the state still can be used to enforce trademarks to prevent false claims of certification, and (as seen above) leads to an increased trust in the system.

Certain governments (also as mentioned above) adopt the certification scheme in a formal way, for example in [India][pgs_india] and [Brazil][arc2020_brazil]. This helps to promote PGSes because it offers a way for small farms to achieve a desirable certification label. Also, previous adoption by government of third-party certification schemes for larger farms may have given the incentive to smaller farmers to develop PGSes in the first place in order to compete, even before the government recognized them.

There may also be bureaucratic infrastructures provided by the government that PGSes piggyback on. [In the opinion][ifoam_studies] of IFOAM - Organics International, with regard to Certified Naturally Grown (CNG), a PGS in the United States:

> Today it is very clear that CNG could not exist without USDA Organic. The programs are actually very complimentary. The USDA NOP provides the bureaucracy, hierarchy and overhead necessary to maintain a third party certification program suitable for processors, distributors and wholesalers. Certified Naturally Grown is then free to focus on and promote small farms selling directly to consumers in their own communities. This complimentary relationship has mutual advantages.

Finally, [in the case][ifoam_studies] of India (at least), the government also sets the standards with which the PGSes comply.

# Sources

## References

* IFOAM - Organics International website:
[ifoam_pgs]: https://www.ifoam.bio/en/organic-policy-guarantee/participatory-guarantee-systems-pgs
    * [Introduction page][ifoam_pgs] for Participatory Guarantee Systems
[ifoam_overview]: https://www.ifoam.bio/sites/default/files/page/files/ifoam_pgs_web.pdf
    * [Information pdf][ifoam_overview] about Participatory Guarantee Systems
[ifoam_studies]: https://www.ifoam.bio/sites/default/files/page/files/studies_book_web.pdf
    * A more detailed [information pdf][ifoam_studies] about Participatory Guarantee Systems

[ifoam_youtube]: https://www.youtube.com/watch?v=vb1hfRswKMg
* A [video about Participatory Guarantee Systems][ifoam_youtube] from IFOAM - Organics International

[arc2020_brazil]: http://www.arc2020.eu/participatory-guarantee-systems-for-organic-food-a-brazilian-model/
* [Description of the Brazilian government policy on PGSes][arc2020_brazil] from the Agricultural and Rural Convention 2020 website

[pgs_india]: https://pgsindia-ncof.gov.in/pgs_india.aspx
* [Indian government overview][pgs_india] of PGSes in India
[iaf_paraguay]: https://archive.iaf.gov/Home/Components/CustomBlog/CustomBlog/14%3Futm_medium=email&utm_source=govdelivery.html
* [Description of PGSes][iaf_paraguay] in Latin America from the Inter-American Foundation, an independent US Government agency

[researchgate_re-imagining]: https://www.researchgate.net/publication/316063245_Participatory_guarantee_systems_and_the_re-imagining_of_Mexico's_organic_sector
* A [research paper][researchgate_re-imagining] about the effectiveness PGSes in Mexico

[cng]: https://www.cngfarming.org/what_is_pgs
* Explanation of Participatory Guarantee Systems from [Certified Naturally Grown][cng], a PGS in the United States

## Other Sources

[researchgate_theoretical]: https://www.researchgate.net/publication/321918181_Participatory_Guarantee_Systems_PGS_in_Mexico_a_theoretic_ideal_or_everyday_practice
* A [research paper][researchgate_theoretical] laying out issues with the operations and sustainability of the PGS model, with three case studies in Mexico

* Wikipedia Articles:
[wikipedia_ifoam]: https://en.wikipedia.org/wiki/IFOAM_-_Organics_International
    * [IFOAM - Organics International][wikipedia_ifoam]
[wikipedia_pgs]: https://en.wikipedia.org/wiki/Participatory_Guarantee_Systems
    * [Participatory Guarantee Systems][wikipedia_pgs]
[wikipedia_iaf]: https://en.wikipedia.org/wiki/Inter-American_Foundation
    * [Inter-American Foundation][wikipedia_iaf]
