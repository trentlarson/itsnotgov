<!-- title: Global Food Safety Initiative -->

The [Global Food Safety Initiative][site_home] (GFSI), part of [the Consumer Goods Forum][site_about], is a coalition of food safety experts led by [major food retailers][foodsafety_gfsi_overview] created for the purpose of [normalizing food safety standards][foodsafety_gfsi_overview] across the supply chain. It was created [in 2000][site_about] in response to a climate of [redundant auditing][foodsafety_gfsi_overview] which was in turn a response to a [crisis in food safety][foodsafety_gfsi_overview] in the 1990s. In 2008, Walmart became the [first nationwide retailer][walmart_first] in the United States to require some of its vendors to comply with a GFSI recognized standard.

# Results

GFSI works not by creating certifications directly, but by [certifying other certification organizations][foodsafety_gfsi_overview].

## Voluntary

It works on a [voluntary basis][site_about]. For example, [Walmart requires][walmart_guide] a GFSI approved audit for certain companies, including all large suppliers of leafy greens, and recommends it for all other companies.

## Reception

A [survey-based study][nih_survey] in the Journal of Food Protection showed significant positive opinion among GFSI-approved producers of GFSI's ability to increase food safety. Walmart has [apparently seen][foodsafety_gfsi_overview] a 31% reduction in recalls since introducing GFSI requirements, though this may need more rigorous study.

# Government Interaction

The GFSI is mostly a private organization, but as is the case with many large standards boards, there is some interaction with government.

## Funding

This is uncertain, and merits more research. However it is not prominently mentioned on its website, nor the website of the Consumer Goods Forum.

## Members and Partnerships

GFSI lists [members of government][site_about] among its member experts.

As of 2018, GFSI has been [working toward][foodsafety_public_private] public-private partnerships to further its mission of standardizing auditing requirements. It is particularly focusing on developing nations, and has [formed a relationship][foodsafety_public_private] with the government of Chile as well as the [World Bank][site_world_bank].

### Relationship with FDA

At the 2012 Global Food Safety Conference, Michael Taylor, an FDA official at the time, indicated that the GFSI in some respects [leads the FDA][site_fsma]:

> "As we build our new import system, we want to work closely with GFSI and build on the foundation you have established for effective and credible certification programs."

It should be acknowledged that the feedback could just as well also go in the other direction. However there [does not appear][site_fsma] to be a formal relationship between the GFSI and the FDA.

## Threat of lawsuits

It should be noted that as with any legal consumer industry, the threat of lawsuit is a part, probably a major part, of the incentive of food retailers to choose to implement food safety protocols. As such, the government is at least implicitly involved in the process.

# Sources

## References

* Global Food Safety Initiative Website:
    * [Home][site_home]
    * [About page][site_about]
    * [Article about comparison with FDA][site_fsma]
    * [Article about partnership with World Bank][site_world_bank]

* Food Safety News:
    * [GFSI Overview][foodsafety_gfsi_overview]
    * [GFSI Public Private Partnerships][foodsafety_public_private] in 2018

* Walmart
    * [Food Safety Guide][walmart_guide]
    * [2008 Announcement of adopting GFSI][walmart_first]

* [Survey-based study][nih_survey] on the reception of GFSI, from the Journal of Food Protection

## Other Resources

* Global Food Safety Initiative [article on Wikipedia][wikipedia]
* About GFSI's [benchmarking process][site_benchmarking] for standards.

## See Also

* [Food Safety at Walmart][itsnotgov_walmart]

[site_home]: http://www.mygfsi.com/
[site_about]: http://www.mygfsi.com/about-us/about-gfsi/what-is-gfsi.html
[site_fsma]: https://www.mygfsi.com/news-resources/news/news-blog/642-what-does-it-mean-to-be-gfsi-certified-in-a-fsma-environment.html
[site_world_bank]: https://www.mygfsi.com/news-resources/news/press-releases/1355-global-food-safety-initiative-gfsi-announces-global-partnership-with-international-finance-corporation
[site_benchmarking]: http://www.mygfsi.com/certification/benchmarking/benchmarking-overview.html

[foodsafety_gfsi_overview]: https://www.foodsafetynews.com/2014/02/transitioning-to-gfsi-recognized-standards/
[foodsafety_public_private]: https://www.foodsafetynews.com/2018/03/public-private-partnerships-in-the-spotlight-at-2018-global-food-safety-conference-kick-off/
[walmart_guide]: https://cdn.corporate.walmart.com/3d/b3/f30fc5f44fc58ea06cec84102c26/supplier-food-safety-requirements-2017-v2.pdf
[walmart_first]: https://news.walmart.com/news-archive/2008/02/04/wal-mart-becomes-first-nationwide-us-grocer-to-adopt-global-food-safety-initiative-standards
[nih_survey]: https://www.ncbi.nlm.nih.gov/pubmed/28853629

[wikipedia]: https://en.wikipedia.org/wiki/Global_Food_Safety_Initiative

[itsnotgov_walmart]: /regulation/food-safety/walmart-food-safety/

<!-- TODO
This is an excellent article, if only we could validate the source at all: http://www.qualityassurancemag.com/article/qa0613-walmart-food-safety-standards/
-->
