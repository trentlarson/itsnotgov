<!-- title: Organic certification -->

<!-- redirect: /w/c/Regulation/FoodSafety/organic_certification -->

*This article is a stub*

Organic certification has multiple brands.

# To Do

The following should be verified. It's based on hearsay of this stub's author:

Private organic standards boards preceded government formation of the legally "organic" standard. Apparently there were even more boards before hand. Apparently the USDA watered down the standard, perhaps even affecting other nominally organic certifications. Apparently Mofga.org is not legally not organic, but by some accounts it may actually be better (like organic boards were pre-USDA standard)

# Orgs
* [MOSA Certified Organic](https://mosaorganic.org/)
* [Oregon Tilth](https://tilth.org/)
* Mofga.org Not legally organic.