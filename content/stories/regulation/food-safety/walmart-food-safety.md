<!-- title: Food Safety at Walmart -->

<!-- redirect: /w/c/Regulation/FoodSafety/WalmartFoodSafety -->

*This article is a stub*

Walmart's [food safety audit guidelines][audit_guidelines] require standards beyond what the FDA and other government organizations require. From their [food safety requirements guide][food_safety_guide]:

> "Please note that Local, State, FDA and USDA inspections will NOT be accepted in the place of a food safety assessment"

Walmart also requires a [Global Food Safety Initiative][gfsi_itsnotgov] approved audit for certain companies, including all large suppliers of leafy greens, and recommends it for all others.

Sources

* [Audit Guidelines][audit_guidelines]
* [Food Safety Requirements for Food and Beverage Suppliers][supplier_guide]

[audit_guidelines]: http://corporate.walmart.com/suppliers/food-safety-audits
[supplier_guide]: https://cdn.corporate.walmart.com/3d/b3/f30fc5f44fc58ea06cec84102c26/supplier-food-safety-requirements-2017-v2.pdf
[gfsi_itsnotgov]: /regulation/food-safety/global-food-safety-initiative
