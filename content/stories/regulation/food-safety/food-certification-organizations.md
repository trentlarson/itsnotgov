<!-- title: Food Safety Certification Organizational Patchwork -->

<!-- redirect: /w/c/Regulation/FoodSafety/food_certification_organizations -->

*This article is a stub. You can help out by filling out useful details. See our writing guidelines.*

# To Do

The following should all be investigated and put into article form. This is not yet ready to be taken as credible:

There seems to be a large patchwork of independent, mostly private regulatory and meta-regulatory agencies. They work together to normalize food safety standards. For example, if you look at message boards for vendors who want to look at Costco or Walmart, each store has its own standards, but they also accept food that is certified by larger organizations. This is useful to the vendors because it creates less work for them to certify.

However if you go down the rabbit hole, it appears that the larger organizations have some sort of relationship which coalesces on even larger organizations. This all deserves some investigation to see the scope of it all, where the government may be quietly involved after all, and comparison to the FDA/USDA. (Try to answer: What is left for FDA/USDA to do for food at that point?)

Here's some, there may be more. I put all of them into one article because their story seems to be intertwined. However, it may turn out to be better to have a separate article for each one.

* [International Food Safety & Quality Network](http://www.ifsqn.com/) - An online platform for food safety practitioners to discuss improvements to food safety standards. Covers issues with both government and third party regulators.
* [Global Food Safety Initiative](http://www.mygfsi.com/)
    * Seemingly [cited by](https://webapps.traqtion.com/traqtionportal/costcofoodportal.jsp) [Costco](/regulation/food-safety/costco-food-safety) (as of 2016) as an option for Chinese food suppliers. *NOTE: interestingly, GFSI's website states that they do not perform certifications themselves. This should be clarified*

This should perhaps all be moved to the category level, since GFSI is now its own story.
