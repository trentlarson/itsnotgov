<!-- title: Insurance Institute for Highway Safety: Road Regulation Research -->

<!-- redirect: /w/c/Regulation/GroundTrafficRegulation/InsuranceInstituteForHighwaySafetyRoadRegula -->

Governments set guidelines for road rules and management to improve safety for drivers. In order to continually optimize rules, government agencies such as the [National Highway Traffic Safety Administration](https://en.wikipedia.org/wiki/National_Highway_Traffic_Safety_Administration) in the United States perform research. Would a private organization have the incentive to perform similar research?

The [Insurance Institute for Highway Safety][iihs_home], or IIHS, is a [nonprofit organization][iihs_about] started in 1959. They are funded entirely by a [large number][iihs_funding] of auto insurance companies and associations.  It is primarily known as an organization that [performs crash tests on automobiles][iihs_crash_test_itsnotgov]. However, they have also been involved in researching road hazards, and providing policy recommendations to the government. The Highway Loss Data Institute, or [HLDI][about_hldi], is a related organization.

[TOC]

# Relevance to voluntary regulation

Obviously, road regulation via legislation requires using government force, and the IIHS is ultimately complicit in collaborating with the government to enforce their vision. Thus, this is not a story about free market achievement from start to finish. However, what this does demonstrate is that insurance companies have the incentive to be a large scale coordinating force, by researching traffic rules, whatever the enforcement mechanism may be. And considering their incentives, it can be argued that they are interested in promoting *effective* rules.

# Research and Policy Influence

IIHS conducts research on various topics related to driving safety, such as cell phone use, [animal hazards][hazard_research], [roundabouts][roundabout_research], and traffic lights. Some of this research leads to the IIHS pushing for government policy changes.

Specific [examples][iihs_milestones] of IIHS/HLDI research include:

* Determining that longer yellow lights were safer, which lead to a change in legislation.
* Determining that right turns on red lights were dangerous, leading NYC policymakers to restrict it.
* Determining that daylight savings reduces crash deaths, influencing legislation to lengthen it.
* Research on teen drivers influencing licensing laws in Florida.
* Determining low speed vehicles pose a safety threat.
* Determining that cellphone and texting bans do not reduce crashes.
* Determining red light cameras significantly reduce fatal crashes, and pushed for red light cameras to be implemented.

# Incentives

[Some argue][nyt_red_light_controversy] that, while the IIHS and HLDI are interested in accurately assessing the safety of cars, they are also interested in having tickets issued to drivers, because it increases their premiums in some cases. As such, they may have an ulterior motive in promoting the use of red light cameras.

Perhaps they have such a perverse incentive when it comes to ticketable offenses, but it seems unlikely to apply to increasing yellow light duration, arguing *against* the usefulness of banning cell phones, and influencing daylight saving time.

# What remains

The insurance companies are not, alone, regulating the road. The are very much doing so with the help of the government. The government still enforces the rules, and is responsible for normalizing rules across large regions, to keep them consistent for drivers. And, as it is, IIHS and HLDI probably produce much less of the body of research that goes into traffic safety, than does the government.

However, in a world without government roads, there would be more place for the insurance companies to research these regulations. The examples above point to the fact that insurance companies do have the incentive to do so, *despite* the government already conducting its own research. Without government researching *anything*, insurance companies would have much more incentive to conduct research.

So what about government enforcement? In a more libertarian world, where roads are privately owned and managed, insurance companies could still have an influence. Drivers want to know what roads to take. Insurance companies could give safety ratings for the roads, just as they do for vehicles. They could offer discounts on premiums to drivers for avoiding roads with low ratings. Just as they have an incentive to accurately measure the safety of car features and specific cars, they have an incentive to accurately measure the safety of road features and specific roads. And remember that, though these policy recommendations are currently enforced by the government, it is still possible for them to be enforced as rules of use of a private road.

# Sources

## References

* [IIHS Main Website][iihs_home]
* [IIHS About Page][iihs_about]
* [IIHS Membership and Funding][iihs_funding]
* [IIHS Milestones][iihs_milestones]
* IIHS [Roundabout][roundabout_research] research and information
* IIHS [Roadway and Environment][hazard_research] research and information
* [About HLDI][about_hldi]

## Opinion Statements
* [New York Times blog post][nyt_red_light_controversy] about the red light camera controversy.

## Other Resources

* [Wikipedia article](http://en.wikipedia.org/wiki/Insurance_Institute_for_Highway_Safety)

## See Also

* [IIHS and crash testing][iihs_crash_test_itsnotgov]

[nyt_red_light_controversy]: http://blogs.wsj.com/numbers/seeing-red-1208/
[iihs_home]: http://www.iihs.org/
[iihs_about]: https://www.iihs.org/iihs/about-us
[iihs_milestones]: http://www.iihs.org/iihs/about-us/milestones
[iihs_funding]: https://www.iihs.org/iihs/about-us/member-groups 
[about_hldi]: http://www.iihs.org/iihs/about-us/hldi
[hazard_research]: http://www.iihs.org/iihs/topics/t/Roadway%20and%20environment/bibliography/bytag
[roundabout_research]: http://www.iihs.org/iihs/topics/t/Roundabouts/bibliography/bytag
[iihs_crash_test_itsnotgov]: /regulation/product-safety/iihs-crash-testing-cars
