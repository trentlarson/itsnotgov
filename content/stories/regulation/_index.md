<!-- icon: 📐 -->

<!-- title: Regulation -->

<!-- redirect: /w/c/Regulation -->

Regulation? I thought libertarians were against all regulations!

Well, sortof. We don't believe we're entitled to act in any way we want, without regard for the people around us. We certainly don't want people doing that to us. We want our products to be safe. We want the air to be clean. We want to make wise, informed decisions. Anything that influences people to these ends are a form of regulation. Natural market prices, insurance contracts, opt-in community agreements. All of these can influence people's behavior for the better, without resorting to government coercion.

What's wrong with government coercion? Well, it's very difficult to determine whether a regulation is optimal, considering people's preferences, cost, and outcomes. The government feedback loop tends to be slow, and government regulation is often co-opted by the industries it is purported to regulate. Having competition, or at least the opportunity to opt out, incentivizes good regulation. On an ethical note, regulations, generally speaking, are a means of stopping people from doing certain things that, in and of themselves, are not unethical. They don't have a clear, direct detrimental effect on other people. To a libertarian, it is unethical to use the threat of force to stop an act that is itself not unethical. Thus, we prefer voluntarily derived incentives, on ethical grounds.

# Further Reading:
* [Private Regulation: A Real Alternative for Regulatory Reform](http://www.cato.org/publications/policy-analysis/private-regulation-real-alternative-regulatory-reform) (Cato Institute)
