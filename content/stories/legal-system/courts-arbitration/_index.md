<!-- title: Courts / Arbitration / Dispute Resolution -->

<!-- redirect: /w/c/LegalSystem/CourtsArbitration -->

Outside of government legal systems, there are various [Alternative dispute resolution](https://en.wikipedia.org/wiki/Alternative_dispute_resolution) systems that offers the possibility of dispute resolution which is is voluntary for all parties involved.

# Societies with Decentralized Law

Some societies current and past have evolved with legal systems that are not based on central authorities. There are people who play the role of judges, but they are chosen by parties of the dispute. However, such systems seem to require strong cultural norms.

# Dispute Resolution Organizations

There are [many organizations](https://en.wikipedia.org/wiki/International_arbitration) internationally that arbitrate disputes. They are used to circumvent the need for expensive litigation in government courts, or to deal with situations where parties are not under the jurisdiction of a single legal authority. Despite being independent organizations, they have been known to trend toward a standard body of law.

There are also [Online Dispute Resolution (ODR)](https://en.wikipedia.org/wiki/Online_dispute_resolution) systems. Some are built into e-commerce websites, and some have attempted to spin out into independent businesses.

# Remaining Questions

One interesting question is one of enforcement, and effectiveness of each method. In the case of arbitration, it is either enforced with the power of the state, or not at all (perhaps relying on threat to reputation of parties that do not follow through). In the case of organizations that settle disputes and also control resources (such as credit card companies), they may enforce it by simply removing access to those resources.

Another interesting question is whether such a system would exist, and how it would look, absent the threat of expense of going through the government court system as the alternative.

# Further Reading

* [Mediate.com](https://www.mediate.com), a resource website about the mediation process.