<!-- title: Xeer (Somali Polycentric Law) -->

<!-- redirect: /w/c/LegalSystem/CourtsArbitration/XeerSomaliPolycentricLaw -->

*This article is a stub*

Xeer is an form of [Customary Law](https://en.wikipedia.org/wiki/Custom_%28law%29), practiced in Somalia. Under Xeer, if a conflict arises, elders from either side's ethnic group are [called in][legal_affairs_article] to resolve it. [Precedent](https://en.wikipedia.org/wiki/Precedent) is also [established over time][legal_affairs_article].

# Sources

## References

* [Article][legal_affairs_article] in Legal Affairs magazine

## Additional Resources

* [Wikipedia](http://en.wikipedia.org/wiki/Xeer)

[legal_affairs_article]: http://www.legalaffairs.org/issues/September-October-2005/scene_lombard_sepoct05.msp