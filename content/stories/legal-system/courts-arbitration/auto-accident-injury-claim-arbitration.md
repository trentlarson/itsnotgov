<!-- title: Auto Accident Injury Claim Arbitration -->

<!-- redirect: /w/c/LegalSystem/CourtsArbitration/auto_accident_injury_claim_arbitration -->

In the United States, it is possible to settle auto injury cases via arbitration, which is [less formal][findlaw] than a state court proceeding. This is done to [save time and money][nolo] that would otherwise be spent on a courtroom trial.

# Incentives

While entering an arbitration agreement for auto accident injuries is [usually voluntary][nolo], the main incentive to do so appears to be that the injured party may [threaten the insurance company][nolo] with a lawsuit in the state court system. In [no-fault][iii_nofault] states, the government [may even mandate][findlaw] it.

# Results

## Provision of Service

While the government provides the incentive to enter arbitration, the choice to opt for private arbitration instead of state court proceedings [usually must be made by][nolo] both parties. This indicates that the market is able to provide the service of providing judgment that is reputable enough to satisfy all parties, and be preferable to state court proceedings. There is often the opportunity to [choose among many arbitrators][findlaw], though the choice is [sometimes stipulated][findlaw] by contract or law.

Private arbitration tends to be [faster][nolo] than state courts. The time between filing and award [can reasonably be][findlaw] 85 days. It is also less expensive to the parties, though there is an interesting question about government-related sources of income for arbitration organizations (see Funding below). Finally, the market of arbitrators and rules provides much more flexibility than state courts. For example, a ["high-low" agreement][nolo] may be possible, wherein a minimum and maximum award are decided ahead of proceedings. It is also possible to agree to not allow the case to go to appeals, which [may actually be][nolo] to the advantage of the injured party.

## Funding

Fees for the arbitration process (administrative fee and arbitration fee) are paid by one or both parties of the arbitration. By contrast, state courts are funded by a combination of [taxes][ncsc_court_funding] and [court fees][npr_court_fees].

However, it should be noted that arbitration organizations often have [government customers and contracts][adr_gov].

## Enforcement

Arbitration for auto injury claims tend to be enforced by the government as [legally binding][findlaw].

# Sources

## Reference

* Findlaw article: [Car Accident Arbitration][findlaw]
* Nolo article: [Car Accident Arbitration][nolo]
* American Arbitration Association, [information about government relationships][adr_gov].
* NPR [article on state court fees][npr_court_fees].
* National Center for State Courts [article on taxpayer money][ncsc_court_funding] spent on state courts.

## Other Resources

* Insurance Information Institute: [No-fault auto insurance][iii_nofault]

## Further Reading

* Nolo article: [Auto Accident Injury Claim Mediation][nolo_mediation]. Similar to arbitration, but it is non-binding. It is mainly meant to facilitate an agreement.

[findlaw]:https://injury.findlaw.com/car-accidents/car-accident-arbitration-process-and-timeline.html
[nolo]: https://www.nolo.com/legal-encyclopedia/arbitration-of-car-accident-claims.html
[iii_nofault]: https://www.iii.org/article/background-on-no-fault-auto-insurance
[adr_gov]: https://www.adr.org/government
[ncsc_court_funding]: http://www.ncsc.org/information-and-resources/budget-resource-center/budget_funding.aspx
[npr_court_fees]: https://www.npr.org/2014/05/19/312158516/increasing-court-fees-punish-the-poor
[nolo_mediation]: https://www.nolo.com/legal-encyclopedia/mediation-of-car-accident-claims.html