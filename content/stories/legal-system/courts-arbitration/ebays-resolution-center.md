<!-- title: eBay's Resolution Center -->

<!-- redirect: /w/c/LegalSystem/CourtsArbitration/ebays_resolution_center -->

eBay's Resolution Center is an [Online Dispute Resolution (ODR)][wikipedia_odr] system for buyers and sellers on eBay.

The system [guides the user][bt] through various common scenarios of parties not meeting each other's expectations to offer standard procedures. As of 2016, eBay [resolved 60 million cases][lawsiteblog] a year using its system, 90 percent of which was done automatically with no human intervention.

However, eBay will [refer the case][dummies] to government law enforcement if they deem that a violation of the law has occurred.

# Sources

## References

* [Dummies Guide][dummies] to eBay's Resolution Center
* [Article on][bt] British Telecommunications website
* 2016 [Post][lawsiteblog] about Online Dispute Resolution on a legal blog

## Additional Resources

* [Ebay Resolution Center][resolution_center]
* Wikipedia: [Online Dispute Resolution][wikipedia_odr]

[resolution_center]: https://resolutioncenter.ebay.com/
[wikipedia_odr]: https://en.wikipedia.org/wiki/Online_dispute_resolution
[dummies]: http://www.dummies.com/business/online-business/ebay/ebays-resolution-center/
[bt]: http://home.bt.com/tech-gadgets/internet/what-to-do-when-selling-an-item-on-ebay-goes-wrong-11363974907833
[lawsiteblog]: https://www.lawsitesblog.com/2016/04/future-online-dispute-resolution.html