<!-- title: SpaceShipOne, SpaceShipTwo -->

<!-- summary: An experimental, reusable spacecraft funded by Paul Allen, co-founder of Microsoft, and a newer version funded by Richard Branson of Virgin. -->

SpaceShipOne was a [privately funded spacecraft][space], designed by aerospace engineer [Burt Rutan][burt_wikipedia], that achieved suborbital spaceflight. It [was the first][space] manned spacecraft not funded by the government. It won the $10 million [Ansari X Prize][ansari], which required building a spacecraft that could be reused within two weeks, among other requirements. After one of its flights, the pilot Mike Melvill [stood atop the craft][spacereview] and held a sign handed to him by a spectator which read "SpaceShipOne, Government Zero".

# Funding

Funding for SpaceShipOne [came from][space] Paul Allen, co-founder of Microsoft, [who supplied][wired] $25 million. Richard Branson [licensed the technology][ansari] for Virgin Galactic and [funded its successor][space] SpaceShipTwo five years later. Funding for the Ansari X Prize came [from the Ansari family][ansari_sponsors]. However, despite Charity Navigator claiming [no government grants][charity_navigator] to the X Prize foundation, at least one X Prize award (not Ansari) [was sponsored][green_chip_stocks] by the US Government. (Perhaps awards and the foundation itself are counted separately for funding purposes).

# Sources

## References

[space]: https://www.space.com/16769-spaceshipone-first-private-spacecraft.html
* [Article from Space.com][space]
[ansari]: https://ansari.xprize.org/prizes/ansari
* [Ansari X Prize award][ansari]
[ansari_sponsors]: https://ansari.xprize.org/prizes/ansari/sponsors
* Ansari X Prize [sponsors page][ansari_sponsors]
[wired]: https://www.wired.com/2004/10/spaceshipone-wins-the-x-prize/
* [Wired Article][wired] announcing that SpaceShipOne won the Ansari X Prize
[spacereview]: http://www.thespacereview.com/article/1649/1
* [Article from The Space Review][spacereview]
[green_chip_stocks]: https://www.greenchipstocks.com/articles/department-of-energy-awards-funding-to-x-prize/78624
* [Green Chip Stocks article][green_chip_stocks] about the Department of Energy funding a different X Prize
[charity_navigator]: https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=9561
* [Charity Navigator article][charity_navigator] for the X Prize Foundation

## Other Sources

[burt_wikipedia]: https://en.wikipedia.org/wiki/Burt_Rutan
* [Burt Rutan][burt_wikipedia] - Wikipedia Article
[spaceshipone_wikipedia]: https://en.wikipedia.org/wiki/SpaceShipOne
* [SpaceShipOne][spaceshipone_wikipedia] - Wikipedia Article
[spaceshiptwo_wikipedia]: https://en.wikipedia.org/wiki/SpaceShipTwo
* [SpaceShipTwo][spaceshiptwo_wikipedia] - Wikipedia Article
[virgin_galactic]: https://www.virgingalactic.com/
* [Virgin Galactic][virgin_galactic] - Richard Branson's commercial spaceflight company


