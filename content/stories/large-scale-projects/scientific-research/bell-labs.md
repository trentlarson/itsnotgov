<!-- title: Bell Labs -->

<!-- redirect: /w/c/LargeScaleProjects/ScientificResearch/BellLabs -->

*This article is a stub. Requires looking into how much funding was because of military contracting for WW2. Also should mention patents.*

Bell Labs was a branch of AT&T that was known for performing significant [technological research][encyclopedia_britannica_article].

# Sources

## References

* [Encyclopedia Britannica article][encyclopedia_britannica_article]

## Additional Resources

* [Wikipedia](https://en.wikipedia.org/wiki/Bell_Labs)

[encyclopedia_britannica_article]: http://www.britannica.com/EBchecked/topic/59675/Bell-Laboratories