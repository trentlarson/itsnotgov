<!-- title: Philanthropic Funding of Basic Research in Science -->

[_Basic research_][wikipedia_basic_research] in science refers to research into fundamental knowledge such as the laws of chemistry, as opposed to _applied research_ which refers to research into practical applications of science such as curing diseases. The early 2010s [saw a significant decline][nyt] in United States government funding for scientific research in general. Philanthropists have [seen this as a reason][nyt] to take up some of the slack. Since basic research [may have a tendency][nature_2008] to be less popular than applied research, philanthropic funding for basic research requires a special focus to make sure it is funded.

Around 2008 there [was a concern][nature_2008] (which perhaps continues) in the scientific community that philanthropists were tending toward applied research and away from less popular research including basic research. [The Science Philanthropy Alliance][site_home] is a [coalition of philanthropists][site_who] who fund basic research in science. It was [formed in response][site_mission] to 2012 United States government research budget cuts. [Its purpose][site_what] is to encourage other philanthropists to support basic research.

# Results

The degree to which basic research is funded by philanthropy has been difficult to determine because these figures had [not been formally collected][nyt] as of 2014. To attempt to answer this question, the Science Philanthropy Alliance [conducted surveys][site_2016_survey], once in 2015 with universities, and again in 2016 with the addition of research institutes. In either year, the number they came up with was necessarily a lower bound, as their data [was limited][inside_philanthropy] to whichever institutions participated in the study.

They [came up with a figure][inside_philanthropy] of $1.2 billion among 27 universities in 2015 and a figure of $2.3 billion for 42 universities and research institutes in 2016. Among the 26 universities which participated in both the 2015 and 2016 surveys, the [figure increased][site_2016_survey] from $1.19 billion to $1.56 billion. It should be noted that these studies [indicated some issues][inside_philanthropy] with how the funding was distributed, and that [federal funding was][site_2016_survey], for comparison, $38 billion per year. The 2016 survey was published in February of 2017.

However, somewhat confusingly, in a [December 2018 blog post][site_blog_post], the Science Philanthropy Alliance paints a rather different picture for the role of philanthropy in 2016. According to this article, 8% of funding (amounting to about $4 billion) for basic research came from directed philanthropy (funding designated for specific projects). However as much as 23% of funding came from university's funds and 14% from research institute's funds, most of which are endowments. This presumably comes from philanthropic sources as well, as the article claims a total 44% of basic research is funded by philanthropy. Another 6% came from business, leaving only 49% from the federal government (amounting to $25.2 billion, in apparent contradiction with their own [2016 survey][site_2016_survey]). This blog post cites data from the [National Science Foundation][wikipedia_nsf] (a department of the United States government).

# Sources

## References

* Science Philanthropy Alliance website
[site_home]: https://www.sciencephilanthropyalliance.org/
    * [Home page][site_home]
[site_who]: https://www.sciencephilanthropyalliance.org/who-we-are/
    * [Who We Are][site_who]
[site_mission]: https://www.sciencephilanthropyalliance.org/our-mission/
    * [Our Mission][site_mission]
[site_what]: https://www.sciencephilanthropyalliance.org/what-we-do
    * [What we do][site_what]
[site_blog_post]: https://www.sciencephilanthropyalliance.org/philanthropy-a-critical-player-in-supporting-scientific-research-alliance-blog/
    * [Blog Post][site_blog_post] from December 2018 about philanthropic funding of basic research in 2016
[site_2016_survey]: http://www.sciencephilanthropyalliance.org/wp-content/uploads/2017/02/Survey-of-Private-Funding-for-Basic-Research-Summary-021317.pdf
    * [2016 Survey][site_2016_survey] published February 2017 about philanthropic funding of basic research in 2016

[nyt]: https://www.nytimes.com/2014/03/16/science/billionaires-with-big-ideas-are-privatizing-american-science.html?_r=0
* [New York Times article][nyt] about philanthropy making up for some of the cuts in government funding of scientific research

[nature_2008]: https://www.nature.com/articles/nn1008-1117
* [Nature editorial][nature_2008] warning of the tendency of philanthropy to skew the focus of scientific research

[inside_philanthropy]: https://www.insidephilanthropy.com/home/2017/2/15/science-philanthropy-alliance-data
* [Inside Philanthropy article][inside_philanthropy] going over the Science Philanthropy Alliance's 2015 and 2016 surveys of research institutions

## Other Sources

* Wikipedia articles
[wikipedia_basic_research]: https://en.wikipedia.org/wiki/Basic_research
    * [Basic Research][wikipedia_basic_research]
[wikipedia_nsf]: https://en.wikipedia.org/wiki/National_Science_Foundation
    * [National Science Foundation][wikipedia_nsf]

## Further Viewing

[youtube_promo]: https://www.youtube.com/embed/PyIXh2qm2G4
* Science Philanthropy Alliance [promotional video][youtube_promo]
