<!-- title: Edhi Foundation (Pakistan) - Missing Persons Service -->

<!-- redirect: /w/c/EmergencyServices/missing_persons/edhi_foundation_pakistan_missing_persons_service -->

*This article is a stub. You can help out by finding and filling out useful details.  See our [writing guidelines](/help/story-writing-guidelines).*

It seems that they use some state services to broadcast their signal looking for people. Their service is also tied into their mortuary service, helping family members identify dead bodies.

This subject is a bit hard to get good objective data on. It would be interesting to know exactly how much they depend on the government.

# Sources:

Edhi's page about it: https://edhi.org/missing-persons-service/

Interesting stories from Dawn (Pakistani English-language news source):

* https://www.dawn.com/news/1335578
* https://www.dawn.com/news/974023

# See Also:

## Related services from Edhi Foundation
* [Edhi Foundation - Homes and Orphanage Centers](/w/c/SocialServices/affordable_housing/edhi_foundation_pakistan_homes_and_orphanage_cente/)

## Other services from Edhi Foundation

* [Edhi Foundation Ambulance Service](/w/c/EmergencyServices/EmergencyMedical/edhi_foundation_pakistan_ambulance_service/)