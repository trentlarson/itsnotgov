<!-- title:Saved In America -->
<!-- summary:A group of private investigators in San Diego, comprising former police and members of military, recover missing children. -->

[Saved In America][site_home] is a [nonprofit group][vice] of private investigators, started either [in 2015][vice] or [late 2014][site_history], with the goal of recovering missing children. They are [based in San Diego][vice], but they find missing children across the United States. Parents ask for help from Saved In America [when they find][vice] that the police do not consider their case to be high priority. As of July, 2017, they [had found][vice] 35 children. As of November 27, 2018 they [claim to have][archive_site_home_nov_2018] saved 137 missing children.

There are conflicting reports about the size of the group's membership: 28 as of July 2017 [according to Vice News][youtube_vice]; 18 as of October 2017 [according to NBC San Diego][nbc]. Members include [former members of][nbc] police, Navy SEALs, and British Special Air Service.

<iframe width="560" height="315" src="https://www.youtube.com/embed/V7FiwHl-IoE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Results

## Funding

Funding for Saved In America appears to be entirely by [voluntary donations][youtube_nbc]. According to their website, their [initial donation][site_history] was from the Lynch Foundation for Children, which, as of Oct, 2017 [continues to donate][nbc]. However the members themselves are volunteers who [take no pay][youtube_nbc] for their work. Donations apparently go [toward operational expenses][youtube_nbc].

## Efficiency

The [average cost][vice] to the group per recovered child is about $5000. The [average time][vice] spent is nine days.

## Children Found

[Vice News asserts][vice] that 35 children were found as of July, 2017. Other figures, including those cited in news sources, seem to be based on the organization's own claims, though the news reports don't appear to question them:

* [43 recovered][nbc] in October, 2017 (NBC San Diego)
* [71 recovered][youtube_nbc] out of 71 attempts (NBC News)
* [137 recovered][archive_site_home_nov_2018] as of November 27, 2018 (Saved In America's own website)

<iframe width="560" height="315" src="https://www.youtube.com/embed/wgVm8xW2CDA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

For context, there were [465,676 entries][vice] for missing children in the National Crime Information Center in 2016 according to the FBI. As of around 2017, according to the Summer Stephan, Chief Deputy District Attorney of San Diego County, there were an [estimated 5000][youtube_vice] victims of sex trafficking in San Diego annually, about 100 of which  are prosecuted by local and federal the federal government. [According to][nbc] the Department of Justice, there are an estimated 2,500 missing kids at any given time in San Diego.

## Government Involvement

[According to][nbc] Saved In America, they do not engage kidnappers or apprehend the missing children. They [refer to the police][youtube_vice] once they find where the child is. However, they appear to be prepared to pick up the child themselves in the [Vice News feature][youtube_vice], citing power of attorney.

They are [licensed private investigators][nbc], which requires government approval. [They are][nbc] former police and members of military, thus they received training from the government. There are [members of government][site_advisory_board] in their advisory board.

# Sources

## Reference

* Saved In America Website
    * Home Page
[site_home]: https://www.savedinamerica.org/ (Saved In America - Home Page)
        * [Current][site_home]
[archive_site_home_nov_2018]: https://web.archive.org/web/20181127224716/https://www.savedinamerica.org/ (Saved In America - Home Page - Nov 2018 Archive)
        * [Archived: November 2018][archive_site_home_nov_2018]
[site_advisory_board]: https://www.savedinamerica.org/advisory-board/ (Saved In America - Advisory Board)
    * [Advisory Board][site_advisory_board]
[site_history]: https://www.savedinamerica.org/history/ (Saved In America - History)
    * [History][site_history]

[vice]: https://news.vice.com/en_us/article/evak5w/ex-navy-seals-are-rescuing-children-from-sex-trafficking (Vice News)
* [Vice News][vice] article and video - July, 2017
[nbc]: https://www.nbcsandiego.com/news/local/Saved-in-America-Works-To-Find-Missing-Children--449150033.html (NBC San Diego)
* [NBC San Diego][nbc] article - October, 2017

* YouTube Videos
[youtube_vice]: https://www.youtube.com/watch?v=V7FiwHl-IoE (YouTube - Vice News)
    * [Vice News feature][youtube_vice] (same as in the above article) - Published to YouTube October, 2017
[youtube_nbc]: https://www.youtube.com/watch?v=wgVm8xW2CDA (YouTube - NBC News)
    * [NBC News feature][youtube_nbc] - Published to YouTube April, 2018
