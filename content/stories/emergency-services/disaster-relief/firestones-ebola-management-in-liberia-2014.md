<!-- title: Firestone's Ebola management in Harbel, Liberia in 2014 -->

<!-- redirect: /w/c/EmergencyServices/DisasterRelief/firestones_ebola_management_in_liberia_2014 -->

Firestone [created a response][npr] to the 2014 Ebola outbreak as it affected the town containing its plantation Harbel, Liberia, near the capital of Monrovia. During a major Ebola outbreak in Liberia, they were able to keep all cases in their town under control. It was [praised][npr] by Dr. Flannery, head of the U.S. Centers for Disease Control and Prevention (CDC) team in Liberia, as well as [CDC Director Tom Frieden][cdc].

# Results

## Success

On March 30th, 2014, a Firestone employee's wife contracted Ebola. Firestone's management couldn't find a hospital in Monrovia to accommodate her, so they created their own Ebola ward in their company hospital, [looking up instructions][npr] on the Internet for treating Ebola, and using hazmat suits normally used to handle chemical spills. As the outbreak spread, [so did their operations][npr] in containing it. As of October 2014, the only Ebola cases in the town [were from people][npr] who come in from nearby towns, while the outbreak continued outside the town's borders.

## Issues:

One method that Firestone used to prevent the spread of disease is quarantine. Indeed, this is a standard operation. For the purposes of this article, it's necessary to consider whether these quarantines were voluntary. (There is an interesting philosophical discussion to be had among libertarians surrounding what constitutes voluntary action in this scenario.)

Another issue worth raising is the position of Firestone itself as an organization. Harbel, the town in question, is a [company town][npr]. Does this make Firestone a de facto government? Even if not, does this mean that only a company town would have the incentive to take this sort of preventative action? This would limit this as a viable solution to those comfortable with the concept of company towns. Though, it should be noted that they did apparently [treat some people][npr] from nearby towns.

Another issue is Firestone's arguably close relationship with the Liberian government. For instance, as part of the establishment of its operations in Liberia, Firestone gave a [large loan][pbs] to Liberia to pay off its debts.

Finally, there are various controversies surrounding Firestone as a company in its involvement in Liberia, including its [involvement][propublica] in its civil war, as well as labor conditions [allegations][socialfunds] going as far as child and slave labor.

# Sources

## References

* [Firestone Did What Governments Have Not: Stopped Ebola In Its Tracks][npr] - NPR
* [Innovative Response by Firestone Health Officials May Have Limited Ebola Spread in a Part of Liberia][cdc] - United States Centers for Disease Control and Prevention
* [PBS Article][pbs] about the history of Firestone in Liberia
* [Socialfunds Article][socialfunds] with allegations of child and slave labor practices.
* [Firestone and the Warlord][propublica] - Propublica article about Firestone's involvement in the Liberian civil war

## Other Sources

* [Firestone Wikipedia Article][firestone_wikipedia]

## Further Reading

* [How the Free Market Would Handle Quarantines][murphy_quarantine] by Robert Murphy

[npr]: https://www.npr.org/sections/goatsandsoda/2014/10/06/354054915/firestone-did-what-governments-have-not-stopped-ebola-in-its-tracks?sc=tw (Definitely give the title of the article!)
[cdc]: https://www.cdc.gov/media/releases/2014/p1021-limited-ebola-spread.html
[pbs]: http://www.pbs.org/independentlens/ironladies/economy.html
[firestone_wikipedia]: https://en.wikipedia.org/wiki/Firestone_Natural_Rubber_Company
[propublica]: https://www.propublica.org/article/firestone-and-the-warlord-intro
[socialfunds]: http://www.socialfunds.com/news/article.cgi/1897.html
[murphy_quarantine]: https://mises.org/library/how-free-market-would-handle-quarantines