<!-- title: Waffle House Hurricane Response -->

The Waffle House is a restaurant [based in Georgia][today] which has been [recognized by][fema] the Federal Emergency Management Agency ([FEMA][fema_home]) for its exceptional preparedness during hurricanes, ready to serve customers and [first responders][usatoday]. Their preparedness is effective and reliable enough that they are ([along with a few other companies][snopes]) used by FEMA as an informal metric called the [Waffle House Index][usatoday] to determine how badly a storm has affected a given area.

# Results

## Providing Resources During Recovery

Because of its level of preparedness, Waffle House is able to [provide a place][npr] for residents to charge their phones and provide food to [first responders][usatoday] in the aftermath of a storm.

Waffle House supports its locations by monitoring storms at the [Waffle House Storm Center][storm_center]. Waffle House locations are able to operate [on gas alone][yahoo], though they sometimes bring in [electric generators][yahoo]. The company also mobilizes ["jump teams"][yahoo]: people who come from other parts of the United States to cover for employees who are unable to report to work because of the storm. Finally, they prepare to reduce to a [limited menu][npr] under circumstances in which some of their supplies are difficult to obtain.

## Wider Disaster Coordination

The Waffle House Index, [devised in 2004][today] by former FEMA Director Craig Fugate, [uses a color code][fema] to indicate the status of each restaurant. Green indicates that a restaurant is fully operational. Yellow indicates limited menu, gas power, or no power. Red indicates that it is closed. A closed location [indicates a very severe situation][today]. FEMA [uses this][fema] index to determine how quickly businesses will rebound as well as how the wider community is doing.

# Government involvement

The Waffle House Storm Center [coordinates with government agencies][today] for supplies and information. (It would be useful to have more details on this) It may rely on government weather data sources such as [NOAA][noaa].

The Waffle House Index is of course used by FEMA, which is a government agency. Thus, it provides only one link in the disaster recovery system.

<!-- TODO It would be good to have quotes from employees for an #Incentives section, as for pride, caring about people, etc.
     The comments on the FEMA blog post showed some really good sentiment, but they're anonymous.
     [today] and [yahoo] have some useful tweets with quotes -->

# Sources

## Reference

News:

* [USA Today article][usatoday]
* [The Waffle House Storm Center][storm_center] - Image from Twitter
* [Today article][today] about Hurricane Florence
* [FEMA blog post][fema] about how they use the Waffle House to help gauge disasters
* [Snopes article][snopes] clarifying how FEMA uses the Waffle House
* [Yahoo Finance article][yahoo] about Waffle House disaster preparedness
* [NPR article][npr] about limited menus at Waffle House during disasters

Agency Home Pages:

* [FEMA home page][fema_home]
* [NOAA home page][noaa]

[usatoday]: https://www.usatoday.com/story/money/nation-now/2018/09/13/hurricane-florence-waffle-house-index-fema/1287881002/
[storm_center]: https://twitter.com/WaffleHouseNews/status/1039606662234075137
[today]: https://www.today.com/food/waffle-houses-carolinas-close-amid-hurricane-florence-t137462
[fema]: https://www.fema.gov/blog/2011-07-07/news-day-what-do-waffle-houses-have-do-risk-management
[snopes]: https://www.snopes.com/fact-check/fema-waffle-house-index/
[yahoo]: https://finance.yahoo.com/news/waffle-houses-hurricane-response-team-prepares-disaster-184844452.html
[npr]: https://www.npr.org/sections/thetwo-way/2017/08/29/547079242/keep-it-simple-and-stay-open-the-waffle-house-storm-menu
[fema_home]: https://www.fema.gov/
[noaa]: https://www.noaa.gov/
