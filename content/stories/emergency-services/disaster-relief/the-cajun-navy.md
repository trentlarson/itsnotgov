<!-- title: The Cajun Navy -->

<!-- redirect: /w/c/EmergencyServices/DisasterRelief/the_cajun_navy -->

*This article is a stub. You can help out by filling out useful details. See our writing guidelines.*

The Cajun Navy is a 100% volunteer flood rescue organization in Louisiana.

* http://www.huffingtonpost.com/entry/cajun-navy-louisiana_us_57bb1bf3e4b00d9c3a18c9d9
* http://www.nola.com/news/baton-rouge/index.ssf/2016/08/louisiana_flooding_cajun_navy.html
* http://www.wsj.com/articles/the-great-cajun-navy-1472420730

Cajun Navy mentioned as coming to Texas for Hurricane Harvey.

* http://www.wbur.org/hereandnow/2017/09/01/texas-navy-houston-harvey
* http://abc13.com/cajun-navy-still-in-texas-helping-coordinate-relief-work/2393619/

Includes some communication with a congressman:

* https://www.gq.com/story/cajun-navy-and-the-future-of-vigilante-disaster-relief

Apparently they're going to get government help soon:

* http://www.theadvocate.com/baton_rouge/news/politics/legislature/article_17e6a262-5531-11e8-8982-4f17c307d35f.html