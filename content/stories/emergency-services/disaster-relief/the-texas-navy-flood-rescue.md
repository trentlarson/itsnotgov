<!-- title: The Texas Navy (flood rescue) -->

<!-- redirect: /w/c/EmergencyServices/DisasterRelief/the_texas_navy_flood_rescue -->

The Texas Navy (not to be confused with the [Texas Navy Association](https://texasnavy.org/)) was a volunteer effort to rescue people in Houston, Texas during [Hurricane Harvey](https://en.wikipedia.org/wiki/Hurricane_Harvey). It comprised people [bringing their own equipment][npr], such as fishing boats and pickup trucks. [It was named][wbur] after [The Cajun Navy](/w/c/EmergencyServices/DisasterRelief/the_cajun_navy/), a similar rescue organization based in Louisiana, who also came to Texas to aid in the rescue efforts.

# Results

One Texas Navy volunteer, Andrew White, [rescued over 100 people][wbur]. He and his team [focused initially][wbur] on the most vulnerable, such as the sick and elderly, and needed to forgo rescuing many other people for lack of time.

The Texas Navy [made use of][marketwatch] an phone application called Zello to communicate in absence of reliable cell service.

## Government assistance.

The Texas Navy [coordinated with][local_cbs] Harrison County Fire and Rescue and local game wardens. It's unclear the degree to which they were assisted by such local government organizations.

# Sources

## References

* [NPR Article][npr]
* [WBUR Interview][wbur]
* [Marketwatch article][marketwatch]
* [Local CBS article][local_cbs]

## See Also
* [The Cajun Navy](/w/c/EmergencyServices/DisasterRelief/the_cajun_navy/) - A similar rescue effort based in Louisiana

[npr]: https://www.npr.org/2017/08/29/546834292/flood-of-texas-navy-private-citizens-help-in-houston-rescue-efforts
[wbur]: http://www.wbur.org/hereandnow/2017/09/01/texas-navy-houston-harvey
[marketwatch]: https://www.marketwatch.com/story/houston-residents-and-civilians-turn-to-zello-app-to-coordinate-rescue-efforts-2017-08-29
[local_cbs]: https://www.cbs19.tv/article/news/local/east-texans-form-texas-navy-and-head-off-to-houston/468904920