<!-- title: San Francisco Patrol Special Police -->

<!-- redirect: /w/c/Security/LocalDefenseSecurity/san_francisco_patrol_special_police -->

The San Francisco Patrol Special Police is a [private security organization][website] in San Francisco. They have been in existence since [1847][website_history], since [before it was added][hoodline] to the city's charter. Over the years, their scope as an independent patrolling security organization [has been diminished][mission_local] by the city administraton. While their close relationship with the city (at least in more recent years) makes them a bad example of for a fully free market in security, they present an example of coordination among neighbors to voluntarily fund security.

The Patrols are paid for by [residents and business owners][mission_local]. They save money compared to a posted security guard by [pooling money][mission_local] with their neighbors. In the Castro district, for instance, they are funded by [an organization called the Castro Neighborhood Association][cbd].

The Patrol Special Police, however, coordinates with the city of San Francisco. The Patrols have special status among security companies, in that [they are the only][website] security company allowed to patrol an area [rather than stay][mission_local] in front of a specific address they are guarding. Their patrols [are not directed][mission_local] day-to-day by the city's police department. However, they do [check in][mission_local] at a city police station in the morning, and have access to [police radio][mission_local]. Their [uniforms][mission_local] look very similar as well. They have the [power to apprehend][nbc_shutting_down] people, and until 1994 have even had the [power to make arrests][mission_local]. They are also allowed to [carry firearms][mission_local]. The city police department vets each member of the Patrols with a [background check][mission_local], and [provides firearms training][mission_local]. As of 2010, [$300,000 was spent][mission_local] annually for the 13 remaining active Patrols.

The Patrol Special Police's numbers are going down over time, and may soon [disappear][nbc_shutting_down], as the San Francisco police department has started to offering some of the same services for pay as part of their 10-B program. In 2010, a [study was presented][mission_local] to the Police Commission recommending ending the Patrol's special status among security companies, stating among other things that the Patrols were a liability to the city and a source of confusion to the citizens. (It should be noted that the director of the organization producing the study is a [former police officer][mission_local]).

A remaining question is how things played out in the early days during the gold rush, before they were formally chartered into the city. How little oversight did the Patrols have from the city? How was their performance? How well did they keep people safe? Were there competing agencies, and was there a peaceful resolutions to issues with them? Etc.

# Sources

## References

* [Hoodline Article][hoodline] about the death of a former Patrol officer
* Official Website of the San Francisco Patrol Special Police
    * [Home Page][website]
    * [History][website_history]
* Castro/Upper Market Community Benefit District (CBD) [note about Patrol Special Police][cbd]
* NBC Bay Area article about the Patrol Special Police [likely shutting down][nbc_shutting_down] due to the police department offering similar services.
* Mission Local article about a [report recommending ending the relationship][mission_local] between San Francisco and the Patrol Special Police

## Other Sources

* Wikipedia article about [Patrol Special Police][wikipedia]
* San Francisco Examiner article about [two resignations][sf_examiner] from the Patrol Special Police

## Further Reading

* Reason.com article about [Patrol Special Police][reason]

[hoodline]: https://hoodline.com/2017/10/castro-mourns-loss-of-patrol-special-police-officer-john-fitzinger
[website]: http://sfpatrolspecpolice.com/
[website_history]: http://sfpatrolspecpolice.com/history.html
[cbd]: http://castrocbd.org/s-f-patrol-special-police/
[nbc_shutting_down]: https://www.nbcbayarea.com/news/local/Patrol-Specials-Program-Nearing-End-in-San-Francisco-442695463.html
[mission_local]: https://missionlocal.org/2010/10/patrol-specials-under-scrutiny/
[sf_examiner]: https://archives.sfexaminer.com/sanfrancisco/two-resignations-further-reduce-a-more-than-150-year-security-body-from-the-city/Content?oid=2787383
[wikipedia]: https://en.wikipedia.org/wiki/San_Francisco_Patrol_Special_Police
[reason]: https://reason.com/archives/2015/07/21/san-franciscos-private-police