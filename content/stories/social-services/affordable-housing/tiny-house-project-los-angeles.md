<!-- title: My Tiny House Project LA -->

<!-- redirect: /w/c/SocialServices/affordable_housing/tiny_house_project_los_angeles -->

My Tiny House Project LA is an organization that builds very small homes for homeless people in the Los Angeles area. It was started by a musician named Elvis Summers. Summers says that [he was once homeless][homes_seized_latimes] himself. 

According to the National Alliance to End Homelessness, there are about [46,000 homeless people][uproxx_feature] in Los Angeles. As of February 2016, the city of LA [had a two billion dollar plan][homes_seized_latimes] to end homelessness over 10 years, but had not yet found a way to budget it.

Summers initially [started putting][homes_seized_latimes] the houses among the existing homeless encampments in [December of 2015][inside_edition_houses_returned]. Then in early 2016, the city of Los Angeles [seized 3 of the homes][homes_seized_latimes] that were located on public property. Elvis was able to remove 7 ([or 8][people_dotcom_feb_2016], depending on news source) others before the city was able subsequently come for them. Due to public outcry and protests, the city government [agreed to return the seized structures][homes_agreed_returned_latimes] a couple months later. However, [according to Elvis][curbed_article], the city removed the solar panels from the houses before returning them.

As of September 2016, [Elvis was working][curbed_article] on more houses, and had started on a mobile shower unit.

# Government support

The project was, at least initially, not on good terms with the LA city government, so (as of late 2016) it's not likely that they actively supported him. However, the project has at times [used public land][homes_seized_latimes] (namely public sidewalks), albeit not to the city government's liking, to keep some of his houses. Since land is part of the expenses required to house people, this arguably acts as a subsidy. However, Elvis appears to be primarily focused on finding private land, since the city is not very accommodating.

Elvis [did claim][inside_edition_houses_returned], however, that the city was open to leasing out unused lots, but not open to supporting his homeless village program. (The meaning of this mixed message is a bit unclear, so the details of this should be investigated further).

# Results

The My Tiny House Project LA has raised money and built a number of houses for the homeless with [volunteer support][people_dotcom_feb_2016]. It has also received some criticism from local residents and members of the city's government.

## Money Raised

In April of 2016, Elvis initially raised $98,015 on a [Gofundme campaign][gofundme_campaign_april_2015_initial] with a $100,000 goal. [It was helped][curbed_article] by some exposure from an article on people.com. This fundraiser was primarily created to obtain materials for building houses.

In December of 2015, Elvis created [another Gofundme campaign][gofundme_campaign_dec_2015_winter] with a call to attention to the needs of homeless in the winter. One purpose of the fundraiser was to obtain land (not stated whether via rent or purchase) for a homeless community, with a garden for the residents to work on, bathrooms, showers, and a laundry facility. A second plan for the money was to buy a dump trailer to employ the homeless to pick up trash on LA's streets (the intended employer is unspecified, so perhaps he wanted to work with the city on this one). Elvis also wanted to set up a base of operations for the organization. Finally, he was hoping to be able to fund a how-to video for building tiny houses to post on Youtube, so that people in other areas could start up similar projects. In addition to asking for funding, he also asked for donations of building materials for houses, and personal care supplies for the homeless. As of December of 2016, this campaign has reached just over $26,000 of its $250k goal, but it is still open to more donations.

The project also accepts standard donations [via its website](http://www.mythpla.org/donate.html). Elvis [also claimed][curbed_article] in September 2016 that he received an offer from an investor to buy more land for his project.

## Houses Built

As of December 2015, Summers and his volunteers had [built dozens of homes][cbs_local_dec_2015]. In February 2016 the number was [more specifically reported][homes_seized_latimes] as 37. In September 2016, [Elvis said][curbed_article] that he had stopped for a while after 38 houses, but was now up to "41 or 42".

Each house [costs about $1200][npr_march_2016] to build. They are equipped with:

* [Solar-powered lights][homes_seized_latimes]
* [Wheels on the bottom][npr_march_2016] so they can be more easily moved
* [Camping toilet][npr_march_2016]
* [Small American flags][homes_seized_latimes]

[According to Elvis][curbed_article], the houses have a footprint of roughly 6 by 8 feet, and are 7 feet tall, and that they are also equipped with:

* A steel reinforced door
* Two windows, with alarms
* An address
* Smoke detectors
* A cell phone charger, also powered by the solar panels
* A new carpet

## Criticisms from city government and local residents

City Councilman Curren Price claims that [residents complained][homes_seized_latimes] that they needed to walk in the street to avoid the houses. [He said][inside_edition_houses_returned] that it was a risk to public safety, citing reports of firearms and drug activity. He also claims that tiny house occupants [rejected existing options][homes_seized_latimes], such as shelters, and that the tiny houses were [not sustainable][inside_edition_houses_returned], citing lack of heating and plumbing. Mayor's spokeswoman Connie Llanos claims that the houses are [hazardous to occupants][npr_march_2016] (citing concerns over materials used, coupled with lack of insulation) and to [others][homes_seized_latimes]. Summers [acknowledges that][inside_edition_houses_returned] the houses are not permanent solutions, but that they were never meant to be.

Unspecified city workers have found a gun as well as drug paraphernalia in one or more of the houses and tents in the encampment. (It's not clear from the news sources found thus far which of these were found in the houses specifically, nor whether the tents involved were also handed out by Summers, nor whether it matters, since tents had already been used. This would be good to clear up.). One [local resident also claimed][homes_seized_latimes] that the houses were used primarily for prostitution and drug use.

# Sources

## References

* LA Times:
    * [Feb 25, 2016 article][homes_seized_latimes] about the homes being seized
    * [April 21, 2016 Article][homes_agreed_returned_latimes] about homes being returned
* [CBS Local story][cbs_local_dec_2015] about the project
* [NPR article][npr_march_2016]
* [People.com article][people_dotcom_feb_2016]
* [Article on Curbed Los Angeles][curbed_article]
* [Feature on Uproxx][uproxx_feature]
    * [Video of Uproxx feature][uproxx_feature_video], including a look at in-progress house and mobile shower construction.
* [Article on insideedition.com][inside_edition_houses_returned]
* Gofundme Campaigns
    * [Initial April 2015 campaign][gofundme_campaign_april_2015_initial]
    * [December 2016 campaign][gofundme_campaign_dec_2015_winter]

## Further Reading/Viewing

* [Tiny House Project, L.A.][main_website] website
* The project's [Twitter account][project_twitter_account]
* The project's [Instagram account][project_instagram_account]
    * [A photo of progress][instagram_shower_unit_progress] on the mobile shower unit from August 2016
* [Reason.com video feature][reason_feature] and interview with Elvis
* Elvis Summers and libertarian activist Adam Kokesh [speak at a protest][protest_summers_kokesh]
* [Time lapse video][time_lapse_building_house] of building a house

## See Also

* A story of a [similar group in San Diego][san_diego_inspired] who were inspired by Elvis Summers' work.
* [Huts For Hope][huts_for_hope], a similar group in the United States Pacific Northwest area 
* A [third gofundme campaign][gofundme_campaign_nov_2016_smokie] that Elvis set up to fund the funeral of Irene "Smokie" McGee, the woman who inspired him to start the project, and the first recipient of one of his tiny houses.

[homes_seized_latimes]: http://www.latimes.com/local/lanow/la-me-ln-tiny-houses-seized-20160224-story.html
[homes_agreed_returned_latimes]: http://www.latimes.com/local/lanow/la-me-tiny-houses-return-20160421-story.html
[cbs_local_dec_2015]: http://losangeles.cbslocal.com/2015/12/18/south-la-man-builds-tiny-houses-for-homeless-people-across-la/
[people_dotcom_feb_2016]: http://people.com/celebrity/l-a-orders-removal-of-tiny-houses-built-for-the-homeless/
[npr_march_2016]: http://www.npr.org/2016/03/03/469054634/la-officials-bring-the-hammer-down-on-tiny-houses-for-homeless
[curbed_article]: http://la.curbed.com/2016/9/23/13026572/tiny-houses-homeless-los-angeles-elvis-summers-interview
[inside_edition_houses_returned]: http://www.insideedition.com/headlines/15999-big-victory-for-tiny-houses-city-returns-donated-homes-it-confiscated-from-the-homeless
[uproxx_feature]: http://uproxx.com/life/tiny-houses-homeless/
[uproxx_feature_video]: https://www.youtube.com/watch?v=13RFjxPJOMU

[main_website]: http://www.mythpla.org/
[project_twitter_account]: https://twitter.com/tinyhousela
[project_instagram_account]: https://www.instagram.com/mythpla/
[instagram_shower_unit_progress]: https://www.instagram.com/p/BIxtomBgHB2/

[gofundme_campaign_april_2015_initial]: https://www.gofundme.com/mythpla
[gofundme_campaign_dec_2015_winter]: https://www.gofundme.com/mythplawinter
[gofundme_campaign_nov_2016_smokie]: https://www.gofundme.com/ripSmokie

[time_lapse_building_house]: https://www.youtube.com/watch?v=L8nTcqwqdU4
[reason_feature]: https://www.youtube.com/watch?v=n6h7fL22WCE
[protest_summers_kokesh]: https://www.youtube.com/watch?v=x0vBoAZ55uI

[san_diego_inspired]: http://www.sandiegouniontribune.com/lifestyle/people/sdut-tiny-house-homeless-village-2016aug17-story.html
[huts_for_hope]: http://www.hutsforhope.com/