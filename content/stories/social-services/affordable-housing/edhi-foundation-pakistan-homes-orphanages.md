<!-- title: Edhi Foundation (Pakistan) - Homes and Orphanage Centers -->

<!-- redirect: /w/c/SocialServices/affordable_housing/edhi_foundation_pakistan_homes_and_orphanage_cente -->

The [Edhi foundation][edhi_home] is a volunteer run and funded charity organization in Pakistan. It was started by [Abdul Sattar Edhi][npr_article], a famous religious humanitarian. The Edhi Foundation provides many services for the poor. [Among these is homes][edhi_homes_orphanages] for orphans, the elderly, the disabled, and others.

During his life, Edhi received [criticism][guardian_article] from religious conservatives, and competition from extremists who have tried to set up charities to promote their violent jihadist organizations. He died at in 2016 at the age of 88, but his organization lives on.

# Incentives

The Edhi Foundation takes a [principled stance][edhi_major_features] against accepting assistance from any government:

> Edhi Foundation has set many examples, which can’t be traced by other NGOs and trusts actively serving in Pakistan that Edhi believes in the principle of self help, thus he is running all his social welfare driven activities without getting funding from any government or donor agency. It is a fact that Edhi isn’t entertaining even he refuses the concept of support from others. The only donations are entertained from the individuals and few leading businessmen.

[comment]: <> (TODO: Anything interesting about his motivations from [Vice][vice_interview] or [NPR][npr_article] (or maybe put this in opening paragraph))

# Results

Exact numbers on the amount of people of any given sector of society served by the Edhi Foundation is a bit difficult to interpret. This is in part due to language barriers, as the most relevant sources, including the foundation's website, are from Pakistan. We will gather what we can here, and of course give sources. It would be great to gather more specific numbers as a basis for comparison with similar government services in Pakistan.

As of 2004, [almost 6,000 people][dawn_bury_truth] were lodged in Edhi houses across Pakistan. At least a thousand were mentally disabled. See the end of [this article][dawn_bury_truth] for more details on those numbers. They are a bit difficult to interpret and square with each other.

As of 2018, the Edhi Foundation appears to claim to house a total of [8500 people in 18 homes][edhi_homes_orphanages] across Pakistan. (Though the wording is a bit confusing; it may instead be claiming 8500 orphans alone).

## Orphanages

According to the Edhi Foundation, they manage [13 orphanages][edhi_destitute_homes] across Pakistan (presumably among their 18 total homes). One Edhi Homes orphanage in Karachi, [seen in this video][ap_video_orphanage] (Associated Press, 2015), spans 65 acres. It takes care of children up to age 18. They all work, including building up a better center, teaching them life skills.

According to [Abdul Sattar Edhi][ap_video_orphanage]:

> They will also be taught how to read and write. We will not award degrees and diplomas. They must have basic literacy, different skills like driving, the work of a mason, electrician and plumbing work. Even at present they are doing these jobs at the Edhi home.

## Compare to government counterparts

Pakistan has a program called [Sweet Home](http://www.sweethomes.com.pk/) which takes care of orphans. As of 2012, [2,800 orphans][dawn_sweet_home] were under under their care. Interestingly, it seems that even a portion of these orphans (750) had their expenses [covered by philanthropists][dawn_philanthropists_sweet_home].

## Government police protection

Against his wishes, Abdul Sattar Edhi received [round-the-clock protection from police][vice_interview] because of threats from the Taliban.

## Responses

### Praise

Aown Kaz, an environmentalist and activist from Lahore, Pakistan, [writes][pakistans_edhi_effect]:

> One particular area in which the Edhi Foundation has excelled is addressing the poor healthcare system in Pakistan. Over the years, the Edhi Foundation has built numerous health clinics, orphanages, old peoples’ homes, free laboratories, animal shelters, soup kitchens and rehabilitation shelters for children and women
>
> ...
>
> In view of all this, it’s not surprising that the people of Pakistan hold the Edhi Foundation in very high esteem and affection as they recognize that Edhi’s social welfare system is much more effective than that provided by the government.

### Criticism

This video entitled ["Edhi homes or Edhi Jails?"][edhi_homes_jails] has a critique by a woman (not in English; Urdu/Hindi speakers to confirm this or give details would be appreciated!). Judging by comments, it appears to point out that girls who find their way to these shelters are not allowed to leave. Comments (almost entirely in opposition to her) respond saying something to the effect of this being the state of affairs for girls is par for the course in Pakistani culture.

# Sources

## Reference:

### Edhi Foundation pages:

* [Home page][edhi_home]
* [General informational page][edhi_major_features]
* [Description of Homes and Orphanages program][edhi_homes_orphanages]
* [Description of "destitute homes"][edhi_destitute_homes] (for orphans, mentally ill, etc)

### News sources:

* [NPR article][npr_article]
* [Guardian article][guardian_article]
* [Associated Press video][ap_video_orphanage] of an Edhi Homes orphanage
* [Vice Interview][vice_interview]

Articles from [Dawn](https://en.wikipedia.org/wiki/Dawn_(newspaper)) (English-language Pakistani news source):

* [A story][dawn_bury_truth] about people leaving their family members at Edhi Homes under false pretenses
* [A story][dawn_sweet_home] about the growing number of orphans under the care of government services
* [A story][dawn_philanthropists_sweet_home] about philanthropists footing the bill for a portion of orphans under the care of government services

### Opinion Statements:

* [Edhi homes or Edhi jails?][edhi_homes_jails] - A critique, not in English
* [Pakistan's Edhi Effect][pakistans_edhi_effect] - Praise from an activist in Pakistan

## Other Resources:

* [Wikipedia Article][wikipedia_article]
* [List of facilities, including offices and shelters][edhi_networks] on the Edhi Foundation's website.
* A [photo gallery][dawn_sanctuary] of an Edhi Foundation orphanage in Karachi.

# See Also:

## Related services from Edhi Foundation
* [Edhi Foundation - Missing Persons Service](/w/c/EmergencyServices/missing_persons/edhi_foundation_pakistan_missing_persons_service/)

## Other services from Edhi Foundation

* [Edhi Foundation Ambulance Service](/w/c/EmergencyServices/EmergencyMedical/edhi_foundation_pakistan_ambulance_service/)

[edhi_home]: https://edhi.org/
[edhi_major_features]: https://edhi.org/major-features-of-edhi-foundation/
[edhi_homes_orphanages]: https://edhi.org/edhi-homes-orphanage-centres/
[edhi_destitute_homes]: https://edhi.org/destitute-homes/
[edhi_networks]: https://edhi.org/networks/#1503492302451-568493cb-4931

[npr_article]: http://www.npr.org/sections/parallels/2016/07/08/485279862/abdul-sattar-edhi-known-as-pakistans-mother-teresa-dies-at-88
[guardian_article]: https://www.theguardian.com/world/2015/apr/01/pakistan-charity-abdul-sattar-edhi-foundation-karachi?CMP=Share_iOSApp_Other
[dawn_bury_truth]: https://www.dawn.com/news/398065
[ap_video_orphanage]: https://www.youtube.com/watch?v=SgZdNMUSU00
[dawn_sweet_home]: https://www.dawn.com/news/703727
[dawn_philanthropists_sweet_home]: https://www.dawn.com/news/619717
[dawn_sanctuary]: https://www.dawn.com/news/685236

[wikipedia_article]: https://en.wikipedia.org/wiki/Edhi_Foundation
[vice_interview]: https://www.youtube.com/watch?v=XiMv95Z-bow
[edhi_homes_jails]: https://www.youtube.com/watch?v=DgT5jWyShto
[pakistans_edhi_effect]: https://futurechallenges.org/local/pakistans-edhi-effect/