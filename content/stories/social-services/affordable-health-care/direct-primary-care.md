<!-- title: Direct Primary Care -->

<!-- redirect: /w/c/SocialServices/AffordableHealthCare/DirectPrimaryCare -->

*This article is a stub. It requires sources, more specific examples, and stories of success/failure*

Direct Primary Care is medical care provided to the consumer based on a monthly fee or a low annual fee and where insurance is never filed or asked for.  The doctors do not accept any form of insurance including Medicaid and Medicare.  The consumer pays the fee every month whether they see the doctor or not and in return they have greater access to the doctor via email or cellphones, lower fees for common things, or even free shots, annual panels, and more.  It is very much like keeping a doctor on retainer.  

# Sources

## Other Resources

* [Wikipedia article](http://en.wikipedia.org/wiki/Direct_primary_care)