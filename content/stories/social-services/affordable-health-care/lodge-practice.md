<!-- title: Lodge Practice -->

<!-- redirect: /w/c/SocialServices/AffordableHealthCare/LodgePractice -->

*This article is a stub. Needs to discuss why Lodge Practice ended. Partially due to desire to remove competition, partially due to changing medical standards.*

Lodge practice was a means of acquiring inexpensive medical care, used by people with low incomes in the late 19th and early 20th centuries. They were [often organized](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1653615/?page=2) as part of a [Benefit Society](https://en.wikipedia.org/wiki/Benefit_society). Member patients would pay a low annual rate, and would be able to visit a facility as needed, where a doctor would regularly perform rounds. Essentially, it was purchasing medicine at a "bulk rate". The rates could be as low as [$1 per year per person][nyt_physician_condemns], which is [roughly $24][inflation_calc] per year in 2014 dollars, or $3 per year for a family. This [did not include](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1653615/?page=2) surgery or obstetrics.

The contracted physicians were [often young](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1653615/?page=2) and early in their careers. Lodge practice provided them with a means of assured, if relatively low, income, as well as a means of building up reputation. There was [high competition](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1653615/?page=2) among such physicians to acquire a lodge position, despite the relatively low compensation they would recieve. However, after gaining experience, many would move on to standard practice.

Lodge practice received pressure from some doctors, who [condemned it][nyt_physician_condemns] for being too inexpensive, degrading their profession and lowering their wages.

# Sources:

## References

* [New York Times article][nyt_physician_condemns] from Dec 6, 1910, wherein a physician voices his strong opposition
* [Inflation calculation by CPI][inflation_calc]
* ["Contract or lodge practice and its influence on medical attitudes to health insurance."][influence_on_medical_attitudes] (Available on NIH website)

[nyt_physician_condemns]: http://query.nytimes.com/mem/archive-free/pdf?res=9507E2D71F39E333A25755C0A9679D946196D6CF
[inflation_calc]:  http://data.bls.gov/cgi-bin/cpicalc.pl
[influence_on_medical_attitudes]: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1653615/