<!-- title: The Dublin Juvenile Library -->

<!-- redirect: /w/c/SocialServices/libraries/the_dublin_juvenile_library -->

The Dublin Juvenile Library was a [privately funded library][library_history], and the first free public library in the United States. It [was established][library_history] in 1822. In 1884 the town voted to establish a tax-funded library. In 1890 the Dublin Juvenile Library was merged into it, ending its status as a privately owned library.

# Sources

## References
* [History page][library_history] from the Dublin Public Library website

## Other Sources
* A [historical document][archive_history] about the down of Dublin, NH on archive.org

[library_history]: https://www.dublinnhpubliclibrary.org/about.html
[archive_history]: https://archive.org/details/historyofdublinn00dubl