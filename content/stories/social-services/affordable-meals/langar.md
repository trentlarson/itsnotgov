<!-- title: Langar -->

Langar is a traditional vegetarian free meal service held by Sikh communities, [inviting people][herald] from all religions, castes, genders, and economic backgrounds to sit together. The [tradition was started][vice] along with the Sikh religion around the year 1500 by the first Sikh Guru,  Guru Nanak. The [largest langar][vice] is held at the Golden Temple in the state of Punjab in India. Accounts vary, but (as of 2018) [by some accounts][herald] 100,000 meals are served there daily, including between 30,000 and 35,000 locals who eat there every day. On weekends and special days, there [may be][aljazeera] even more.

# Funding

Funding for the langar at the Golden Temple [comes from][aljazeera] donations around the world. Interestingly, they also have [an annual budget][aljazeera] of hundreds of millions of dollars. This seems to imply a cost per meal of about $10, which is perhaps not very efficient. However they [also deliver food][herald] to local hospitals, and perhaps other places, so this may not be a very complete account. The service is run largely by volunteers, though it's unclear if some are paid, since some are [referred to][herald] as "volunteer employees".

# Government involvement

The Sikh temples in Punjab are [managed by an organization][sgpc] called Shiromani Gurdwara Parbandhak Committee via an [act of the Punjabi and British governments][britannica] in 1925. This was [due to a controversy][britannica] caused by some of the guardians of the Sikh temples diverting funds for private use. So, though funding for the meals is done voluntarily, the acquisiton of the temples involved some government force.

# Sources:

## References:

[herald]: https://www.nationalheraldindia.com/cafe/golden-temple-amritsar-punjab-sri-guru-ram-das-jee-langar-hall-is-worlds-biggest-community-kitchen
* [National Herald article][herald] about Langar at the Golden Temple
[aljazeera]: https://www.aljazeera.com/indepth/inpictures/2013/11/pictures-kitchen-feeds-100000-daily-20131117124238293396.html
* [Al Jazeera article][aljazeera] about Langar at the Golden Temple
[vice]: https://munchies.vice.com/en_us/article/ypx47x/inside-the-worlds-largest-free-kitchen
* [Vice News article][vice] about Langar at the Golden Temple
[britannica]: https://www.britannica.com/topic/Sikh-Gurdwara-Act
* [Encyclopædia Britannica article][britannica] about the 1925 Sikh Gurdwara Act
[sgpc]: http://sgpc.net/about-sgpc/
* About page on the [Shiromani Gurdwara Parbandhak Committee][sgpc] (SGPC) official website

## Other Sources:

[tribune]: https://www.tribuneindia.com/mobi/news/punjab/tihar-s-bakery-items-for-golden-temple-langar/658867.html
* [Tribune India article][tribune] about a donation from a prison rehab program of baked goods to the Golden Temple.

## Further Reading:

[quora]: https://www.quora.com/From-where-does-the-Darbar-Sahib-Harmandir-Sahib-also-known-as-Golden-Temple-get-all-that-money-for-free-food-to-give-to-thousands-daily
* [Quora discussion][quora] about how the Langar at the Golden Temple is funded
