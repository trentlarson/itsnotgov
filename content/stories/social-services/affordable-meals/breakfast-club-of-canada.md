<!-- title: Breakfast Club of Canada -->

<!-- redirect: /w/c/SocialServices/affordable_meals/breakfast_club_of_canada -->

The [Breakfast Club of Canada][website] is a charity [founded in the 1990s][ctv_news] that feeds children from needy families in Canadian public schools. As of 2018, it [claims to receive][website_finance] only 3.6% of its funding from government grants.

In 2017, 33 million [meals were fed][charity_intelligence] to 203,852 children (an average of 165 meals per student). The same year, [according to Lisa Clowery][ctv_news], a representative from the organization, over one million students were in need of this program. (For reference, in 2014, there were about [five million students][cbc] in the Canadian elementary and secondary school system.) According to an [analysis][charity_intelligence] (conducted internally, cited by Charity Intelligence), schools with new breakfast clubs saw improved attendance and performance of students.

# Sources

## Reference

* Breakfast Club of Canada website
    * [Home Page][website]
    * [Financial information][website_finance]
* [Charity Intelligence report][charity_intelligence] about the Breakfast Club of Canada
* [Canadian Broadcasting Corporation][cbc] story about numbers of school children in Canadian system
* [CVT News][ctv_news] story about the Breakfast Club of Canada

[website]: http://www.breakfastclubcanada.org/
[website_finance]: http://www.breakfastclubcanada.org/the-club/finance/
[charity_intelligence]: https://www.charityintelligence.ca/charity-details/115-breakfast-club-of-canada
[cbc]: https://www.cbc.ca/news/canada/back-to-school-7-million-students-440-000-educators-prepare-for-the-new-year-1.2750519
[ctv_news]: https://www.ctvnews.ca/canada/one-in-five-kids-go-to-school-on-empty-stomach-breakfast-club-of-canada-1.3576668