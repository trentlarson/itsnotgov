<!-- title: Feeding America -->

Feeding America is a private [food relief][wsj] organization. In 2011 it was the [largest such organization][wsj] in the United States. In 2010 [it worked with][wsj] over 200 food banks. In 2017 it was [serving 46 million people][npr] every year.

Feeding America receives [no government funding][wsj]. All of its funding comes from private [donations][wsj]. However, as of 2011, about a [quarter of its donated food][wsj] comes from the United States Department of Agriculture. Other sources [include companies][wsj] which manufacture food.

In 2011, [according to Vicki B. Escarra][wsj], president of Feeding America, the government still plays a much greater role in feeding the needy than private charities.

# Sources

## Reference

* [NPR article][npr]
* [Wall Street Journal article][wsj]

## See also

* [Charity Navigator][charity_navigator] page
* [Article about SNAP][snap], a United States government food assistance program, from the [Center on Budget and Policy Priorities][cbpp_wikipedia]
* [Feeding America Wikipedia Page][feeding_america_wikipedia]
* [Feeding America website][website]

[wsj]: https://web.archive.org/web/20150514063852/https://www.wsj.com/articles/SB10001424052970204394804577011793451898270
[npr]: https://www.npr.org/sections/thesalt/2017/09/18/551796954/one-of-americas-biggest-food-banks-just-cut-junk-food-by-84-percent-in-a-year
[charity_navigator]: https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=5271
[snap]: https://www.cbpp.org/research/policy-basics-the-supplemental-nutrition-assistance-program-snap
[cbpp_wikipedia]: https://en.wikipedia.org/wiki/Center_on_Budget_and_Policy_Priorities
[feeding_america_wikipedia]: https://en.wikipedia.org/wiki/Feeding_America
[website]: http://www.feedingamerica.org/
