<!-- title: Tangelo Park Program -->

[In 1994][orlando_sentinel], hotel businessman Harris Rosen created the [Tangelo Park Program][site_about_program] to improve outcomes for the residents of the Tangelo Park neighborhood. [It includes][orlando_sentinel] free preschool education, parenting programs, mentorships, and college scholarships.

According to the program's website, Rosen is the [main source of funding][site_faq]. However funding [has also come from][site_about_program] The Orlando Magic and the Edyth Bush Charitable Foundation. There is also some fundraising activity.

# Results

[As of 2018][orlando_sentinel], the crime rate in the neighborhood has dropped 50%, and the graduation rate among college attendees from the neighborhood is 77%. County Sheriff Jerry Demings [said of the program][orlando_sentinel]:

> What Harris Rosen has done … has positively impacted the quality of life in the Tangelo Park neighborhood … I think it has contributed to raising better human beings.

[In the opinion][orlando_sentinel] of  Rick Hess, of the [American Enterprise Institute](https://en.wikipedia.org/wiki/American_Enterprise_Institute), the program has longevity, but perhaps expressed doubt that it could so easily be repeated:

> It builds a sense of credibility and trust that is really hard to replicate

# Government Involvement

According to the program's website, the program [started as][site_about_program] a sort of grass roots awareness program that attracted the attention of Rosen. It also lists local government among those who got attention. It is unclear what role local government played. However, there was at [last some coordination][site_about_program] with local public schools when Rosen was initially discussing scholarship programs.

There appears to be no mention of government funding on the program's website.

# Sources

## References

* [Article][orlando_sentinel] on the [Orlando Sentinel](https://en.wikipedia.org/wiki/Orlando_Sentinel)
Tangelo Park Program official website
    * [About page][site_about_program]
    * [F.A.Q. page][site_faq]

## Other Sources

* [Wikipedia article][wikipedia] about Harris Rosen
* A [discussion on Reddit][reddit]
* [About Harris Rosen][site_about_harris] on the Tangelo Park Program official website. It includes his approach to funding and involvement in the program.

[wikipedia]: https://en.wikipedia.org/wiki/Harris_Rosen
[reddit]: https://www.reddit.com/r/todayilearned/comments/1uj13b/til_that_selfmade_millionaire_harris_rosen/ceink5s/
[site_about_harris]: https://www.tangeloparkprogram.com/about/harris-rosen/
[site_about_program]: https://www.tangeloparkprogram.com/about/tangelo-park-program/
[site_faq]: https://www.tangeloparkprogram.com/about/faqs/
[orlando_sentinel]: https://www.orlandosentinel.com/features/education/os-tangelo-park-program-graduates-20141108-story.html

[comment]: <> (TODO: Stuff about the community getting involved in helping themselves)
[comment]: <> (TODO: Stuff in site_about_harris link about his way of not having red tape, but still being judicious about where money is spent)
