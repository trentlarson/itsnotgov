<!-- title: Affordable Private Schools for the Poor (India) -->

<!-- redirect: /w/c/SocialServices/AffordableChildhoodEducation/affordable_private_schools_for_the_poor_india -->

There is a trend in India of [inexpensive private schools][nyt_article], which some disadvantaged families opt to use in spite of available public education, at least in some areas in India.

# Results

Nationally, these private schools [do not dominate][nyt_article], however a notable amount of parents opt for them, citing poor quality of public education. One example is the [Holy Town][nyt_article] school in Hyderabad, serving primarily children of low income Muslim families, sometimes for as low as $2 a month (as of 2011).

[Some argue][guardian_article] that the evidence of the effectiveness of the private schools vs public schools is inconclusive. Additionally, the low fees are [still too high][guardian_article] for certain migrant workers. Some of them are able to [negotiate][guardian_article] a lower price, but the rest resort back to the government schools. Access to the private schools [appears to be affected][guardian_article] by various demographic and geographic factors. Finally, there is some question of [exploitation][guardian_article] of women employed to teach at these schools. [See here][guardian_article] for more details and related critiques.

## Government Involvement

The [Right to Education Act](https://en.wikipedia.org/wiki/Right_of_Children_to_Free_and_Compulsory_Education_Act,_2009) provided [something akin to school vouchers][oneworld_article], which likely provided some assistance to the private schools after its passage in 2009.

It [also required][oneworld_article] that 25% of all private school students be from disadvantaged families, and receive free education. This should be noted to avoid seeing it as evidence that these private schools provided education to students without government involvement.

It should also be noted, however, that the act [also increased][nyt_article] standards placed on schools that were serving poor families, which some private school administrators saw as an undue burden that may hurt their viability. Mohammed Anwar, who runs a chain of private schools in Hyderabad, [said][nyt_article]:

> If you follow the Right to Education, nobody can run a school.

# Sources

## References

* A 2011 [New York Times article][nyt_article] about the relationship between inexpensive private schools and government schools in India
* A 2015 [Guardian Article][guardian_article] expressing skepticism about the value of inexpensive private schools in India, and its value to the nation's poor

## See Also:

* An [Academic study][academic_study] written by the same author as the Guardian article.
* A 2015 [opinion piece][economist_article] in The Economist, claiming the inexpensive private schools are successful.

## Further Viewing:

* A [TEDx talk][tedx_talk] about private schools for the poor in India, among other countries.

[nyt_article]: https://www.nytimes.com/2011/12/31/world/asia/for-indias-poor-private-schools-help-fill-a-growing-demand.html
[guardian_article]: https://www.theguardian.com/global-development-professionals-network/2015/aug/12/low-fee-private-schools-poverty-development-economist
[academic_study]: https://www.tandfonline.com/doi/abs/10.1080/02671520701809783?journalCode=rred20
[economist_article]: https://www.economist.com/news/leaders/21660113-private-schools-are-booming-poor-countries-governments-should-either-help-them-or-get-out
[oneworld_article]: http://southasia.oneworld.net/news/india-to-notify-right-to-education-act
[tedx_talk]: https://www.youtube.com/watch?v=gzv4nBoXoZc