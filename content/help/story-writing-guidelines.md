<!-- title: Story Writing Guidelines -->

<!-- redirect: /w/s/StoryWritingGuidelines -->

*Work in progress*

*Note: This is not a legal document. For legal issues, see our [Terms of Service](/terms-of-service).*

There are guidelines on *how* to write articles. For information on *what* articles to create, see [Story Submission Criteria](/help/story-submission-criteria)

[TOC]

# Values

Our goal for the site is to be a source of information that is useful to libertarians who want to find real life examples, as well as convincing to curious or skeptical non-libertarians. To meet these goals, we hold the following values in our articles, in order of importance:

## Accurate and Intellectually Honest

In order for this site to attain and maintain a serious reputation, these must be top priority. It is natural for people from other ideologies to come here and be skeptical about what they read. For this reason, we must be *accurate*, by diligently citing sources for any claim we make that supports our point of view. If there is no good source, don't make the claim. (see **Types of Sources** below).

We must also be *intellectually honest* and not overstate our case. We should describe where the particular voluntary endeavor failed to meet its goal, or took some help from the government. (Remember that this helps *us* too; we need to know what we have left to figure out how to do without government.)

## Quality Writing Style

*TBD*

## Thorough

Citing that somebody built a road is one thing, but skeptical readers can fill in details with their own imagination. If however (hypothetically) the road builder created their own traffic rules which reduced accidents, cleaned up hazards faster than government roads tended to be cleaned, were more cost effective than government roads, etc, it paints a much bigger picture. Again, for the sake of intellectual honesty, this should be coupled with a similar list of shortcomings. This is where this site's telling of the story can shine above whatever sources we directly pull from.

## Interesting or Entertaining

We want to give non-libertarians a reason to give us the time of day, but people generally don't like to look at sources of intellectual rivals. But many of these stories have the potential of having an entertainment value, which could in itself get initial attention from people. However, this should always be subordinate to the other values: That is to say, it should never compromise accuracy, intellectual honesty, quality of writing, etc. This is similar to the news media. However *unlike* the news media, we should *not* give way to sensationalism; make this a subtle factor, and let the interesting or entertaining facts speak for themselves.

A good example would be citing a few extra facts about the eccentric character of the subject of the article, which don't particularly hurt or help the libertarian case being made. For instance, consider the story of the man who created the small private road between Bristol and Bath England. It may be worthwhile to describe the fact that he arranged the deal to buy the land from a farmer during a discussion at a pub, and that he and his wife used their home as collateral to build it.

Another example might be putting extra priority on certain facts that do help our case, purely because they happen to be amusing. For instance, in the story about drug safety on the Silk Road, there were apparently many different groups testing different drugs. Naming them all would already be good for the sake of being thorough, but the fact that it's an amusing concept should make it a slightly higher priority.

## Facts vs Theory

It's best to stick to facts for the story, since we are here to describe what has already happened. However, in some cases it is useful to cite theory.

It could be useful as an overall teaching point. However, as much as reasonably possible should remain in the category page sections of the site, and out of story articles. For example: https://itsnotgov.org/w/c/Regulation/

It could also help to clarify _how_ a given story proves our point. For instance, clarifying how an insurance company acts as a regulator for an individual's or company's behavior. We should try to link back to the appropriate category page. This has the benefits of:

* Brevity in the articles
* Not repeating it in each relevant story article, and having to maintain each version as improvements come along (programmers know this as the D.R.Y. [Don't Repeat Yourself] principle)
* Having something ready for contributors to reference, to save them time.

# Article Structure

The question of how exactly an article should be structured is a work in progress, however there are some important or useful things to have in an article:

## Background

A general background of the story. Make it interesting or entertaining (see above for more details). This is where you may want to give basic details on how the project is funded. If that question is complicated, perhaps make it its own section, or put it in the Results section (below).

## Incentives

What are the incentives on the people in the story performing the traditional government function? If they are given some sort of government-based incentives, such as a tax break, it's important to note it for intellectual honesty. If the incentive is substantial, consider omitting the article entirely, or make it clear at the beginning that this article's purpose is to clarify that the story is *not* an example of a non-government entity performing a traditional government function.

However even if the incentive is not government based, it's important to include it because it helps paint the picture of how this sort of thing could sustain itself on a larger scale. This is a bit of analysis, which, as always, we have to be very careful and honest as we add. Every argument made here should be obvious, and convincing to a wide audience. If it's potentially questionable, put the potential doubts on the table as well.

## Results

Exactly how much did this entity accomplish? The larger the scale, the more impressive it is. Some of the stories on this site will be about very small scale projects, and it will be a very easy (and fair) criticism to say that this is easy as a one-off situation. Thus, if a story has a large scope, it is much more compelling.

On the other hand, what damage did this cause? Objectively reported facts should go here, as well as subjective opinions, such as criticism by the government. These criticisms may of course be biased, but the source of the criticism should of course be given, for the reader to determine its validity. We can be a lot less careful with this sort of thing than biased information in *favor* of our opinion, to be intellectually honest. (This point may need clarification or further thinking)

## Funding

TBD: This may or may not want to be part of "Results".

## What Remains

Be careful about adding analysis to the site. However for the sake of completeness and intellectual honesty, some analysis should go here about where (if anywhere) the non-government sector's contribution ended and the government's contribution picked up. For instance, if a charity is building houses and the government is donating the land, note the latter. If an agency has private incentives to develop good regulations, but the government ultimately enforces them, even if via basic contract law, better to admit it here.

## Sources

Sources should go at the bottom of the article. See "Types of Sources" below.

# Copying content

For ideological reasons, we are as of now sticking with a [CC0](https://creativecommons.org/publicdomain/zero/1.0/) license. This means that everything you enter is as close to the public domain as Creative Commons was able to get us, for any given legal jurisdiction. In theory, this appears to mean that you can add anything copied from someplace else, as long as it's also **CC0** licensed, under some other compatible license, or otherwise in the public domain. However, we are still working out the finer points of this, so please stick to your own original writing for the time being. Please see our [Terms of Service](/terms-of-service) for the legal version.

We recognize that there is value in being able to copy material from Wikipedia, and will entertain the possibility of switching to a compatible license in the future. However, that is a lot easier than switching in the other direction, so for now we're using **CC0**.

In any case, copying directly from Wikipedia will likely not be extremely useful because Wikipedia a has different focus. Feel free to link to Wikipedia as an *Other Resource* (see below), however, and use it for your own research.

# Types of Sources

Sources used in articles on this site can be put into five categories: *References*, *Opinion Statements*, *Other Resources*, *Further Reading*, and *See Also*. They each have a different standard for objectivity, and should be used in slightly different ways in articles.

## Reference

In our parlance, a *Reference* is an authoritative source which provides evidence for **facts** claimed in the article. This should be a source that the vast majority of people, regardless of bias, would be likely to accept.

Academic papers written by people who happen to be libertarian walk a fine line. If they're evedince based, and peer reviewed, it could be okay. If it's an economic theory paper, for instance, it would be a bad idea to cite it as evidence for anything. Economic theory papers by an adversarial academic might be acceptable.

*References* should be used as links attached to text within the article, connected to the related claim that is being made. They should additionally be listed in a separate "References" section at the end of the article.

## Opinion Statements

In some cases, it may be valuable to cite opinions, when clear facts are harder to come by, as weaker evidence for a claim. In particular, the effectiveness, or desirability, of a certain service which has been provided.

We should be very careful about this. If and when we do this, it must be presented in terms of the *fact* that the person *had the opinion*. The opinion itself **must not** be presented as fact. We should also take care to similarly present contrary points of view, if they are available, to give the full picture.

One major exception is that we should never cite the opinions of people whose bias predisposes them to be in favor of the product, service, or function being described in the story. For instance, consider a story about Bitcoin. A technically forward thinking individual, even if not libertarian, who claims that Bitcoin is really useful, is not interesting, and it would be about as convincing as an advertisement to even cite, *unless* that person had expressed strong skepticism in the past. An average person, especially a technophobe or somebody generally averse to libertarianism, who was surprised to find Bitcoin to be useful, would on the other hand be interesting. Thus, it's good to show the *Opinion Statement* writer's negative bias wherever possible.

*Opinion Statements* should be used as links attached to text within the article. They can be followed by a short block quotation. They should additionally be listed in a separate "Opinion Statements" section at the end of the article.

## Other Resources

More information, ideally objective but not necessarily authoritative, that tells more about the story. If it is a biased source, such as an op-ed, it should not be biased in a libertarian way. *Other Resources* can come in two forms:

* They can be used as general informative links attached to text within the article, in a way that doesn't support any particular point of view relevant to the article's purpose. For instance, if there is an uncommon term used in an article, a Wikipedia link explaining what the term means would be an appropriate example. If the link is about a relevant event, accuracy is important, so you should use a *Reference* instead.

* They can be put in their own "Other Resources" section at the end of the article. In such a case, it is acceptable that they provide evidence for a point of view relevant to the article, with the understanding that, unlike with *References*, the reader should exercise more caution in trusting it. However, if these happen to support any specific claims written in the article, those claims should already have a linked *Reference* first. An example is a Wikipedia article about an event. If that event supports a claim in the article, the text of the claim should first be linked to a *Reference* such as a newspaper article.

If an article contains a link listed in the "Other Resource" section at the end, and an edit is made that uses it as evidence for a claim (assuming it's from an acceptable source), it is now a *Reference*, and should be moved to the "References" section and should otherwise be treated accordingly (see above).

## Further Reading

Here is where we can try to sell readers on something biased. This can be further reading into a book, libertarian leaning op-eds, or links to Reason.com articles and the like. These should generally only be put in their own "Further Reading" section at the end of the article.

## See Also

Wiki pages on this site can link to each other, to point readers to related topics. This can be attached to text in the article, or in its own "See Also" setion at the end of the article.

# Sepecific Sources

## Wikipedia

It is encouraged to link to Wikipedia as an *Other Resource*, particularly attached to text within the article, as a quick way to inform readers about a topic they may be unfamiliar with. The exception to this is if it is possible to link to the same topic within this site instead. The linked article, ideally, would contain a Wikipedia link. Adding a Wikipedia article in the *Further Reading* section is preferable if the topic is, for instance, a libertarian author or economist with an analysis, rather than something related to the story. Wikipedia articles should never be used as *References*. Wikipedia articles do themselves, however, cite many references, so it could be a good place to go for research.

## Economic History Association

They may have some good data on their site, http://eh.net/. They are used as a source on many Wikipedia articles, so it should be okay to use as a *Reference*.