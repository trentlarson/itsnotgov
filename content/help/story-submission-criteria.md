<!-- title: Story Submission Criteria -->

<!-- redirect: /w/s/StorySubmissionCriteria -->

*This is an early, [in-progress](/help/story-submission-criteria-in-progress) draft of submission criteria for stories on this site. Rules and guidelines are subject to change.*

*Note: This is not a legal document. For legal issues, see our [Terms of Service](/terms-of-service).*

This is a guide for whether a **Story** is proper for this site. For guidelines of content of a **Story**, see [Story Writing Guidelines](/help/story-writing-guidelines).

# Overview

The goal of this website is to become a repository of stories of voluntary provision of desired products, services, or functions, which are often believed to be only producible with coercive government influence. It should be useful as a reference to bolster the arguments of the liberty-minded, and as content to cater to the liberty-curious. Submitted Stories should provide a compelling case to skeptics that voluntary efforts can replace most or all functions of government. Stories should be appropriate and well written. The latter is outlined in the [Story Writing Guidelines](/help/story-writing-guidelines); the former is outlined here.

# Definitions:

* **Presumed Government Responsibility** - A type of product, service, or function that is "often believed" to not be producible without coercive government.
* **NotGov Replacement**: A product, service or function that is performs a **Presumed Government Responsibility**, which is provided by an entity other than a coercive government, based on incentives that go beyond influence, if any, by coercive government.
* **Category** - A high level division of **Presumed Government Responsibilities**, which contains a listing of **Sub-Categories**, and an explanatory article.
* **Sub-Category** - A low level division of **Presumed Government Responsibilities**, which contains a listing of relevant **Stories**, and an explanatory article.
* **Story** - An article about a real-life example of a **NotGov Replacement**.

# Criteria

## General

* Stories have to be *real life* examples, not just theoretical. Theoretical analysis can be included as *part* of an article about a real life story. See [Story Writing Guidelines](/help/story-writing-guidelines) for more details. 
* Stories can be current or historical, but they must have accomplished something. If they have yet to prove themselves, hold off on writing about them.
* Some Presumed Government Responsibilities are produced with partial government influence, where a case can be made that the producer would have still had a non-government incentive to produce it. This can still count as NotGov Replacement, but there must be a strong case for them having responded to an incentive *beyond* any government influence. The more government influence involved, the weaker the Story.
    * **Good example**: Grocery stores are required by gov't to test their food, however some stores test beyond this requirement, presumably from market incentives.
    * **Bad example**: Companies perform some sort of charity function for PR, but they also receive a significant tax break.
* Intellectual Honesty - If the status of the Story's subject as a Notgov Replacement is open to interpretation, include all the caveats (see [Story Writing Guidelines](/help/story-writing-guidelines)). If there are so many caveats that it's not impressive, don't bother with the Story.

## By Category

The definition of a Presumed Government Responsibility is based on what is "often believed" cannot be done without government. However, what is "often believed" is rather subjective. We aim to develop a standard that is generally accepted, and with the right scope. Too broad, and the site loses focus. Too narrow, and we lose opportunity to impress doubtful people.

Anything outside of these guidelines has probably not yet been considered and is subject to editorial review, after which a new guideline ought to be established. In other words, feel free to submit based on your common sense, but be aware that we may not accept it (though we always appreciate effort put into contributions).

### Regulation

It is pretty widely believed that regulation is a government function. Most areas where organizations compel each other to "fall in line" in some way or other, purely due to profit incentive, or due to a community project with sufficient coordination, is interesting enough to be included within this category. 

* Businesses "regulating themselves" - not very interesting.
* Regulation of how much to produce via prices - not very interesting.

### Infrastructure

* Private Busses:
    * **Yes**: An intra-city/city-suburb bus service
    * **Yes** (borderline): Company charter bus for employees (Google Bus)
        * Not very common, though not too surprising given a company's incentives
    * **No**: A private long distance bus service (Greyhound, MegaBus)
        * Too well known and commonplace
* Roads
    * **Yes**: Multi-mile Private toll road
    * **Yes** (borderline): Small private toll road
        * Unusual, though relatively simple to implement
    * **Yes** (borderline): Private road that services access to a natural resource
        * Somewhat impressive, though it's similar to charging for a tourist attraction
    * **No** Private road that simply gives customers access to a far off business's facilities
        * Not much more interesting than a parking lot.
